package com.jacto.unext.repository;

import com.jacto.unext.domain.TipoMaquinario;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TipoMaquinario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoMaquinarioRepository extends JpaRepository<TipoMaquinario, Long> {}
