import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MaquinarioDetailComponent } from './maquinario-detail.component';

describe('Maquinario Management Detail Component', () => {
  let comp: MaquinarioDetailComponent;
  let fixture: ComponentFixture<MaquinarioDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MaquinarioDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ maquinario: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MaquinarioDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MaquinarioDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load maquinario on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.maquinario).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
