import { IUser } from 'app/entities/user/user.model';
import { IHistoricoLocacao } from 'app/entities/historico-locacao/historico-locacao.model';
import { ICarteira } from 'app/entities/carteira/carteira.model';

export interface IParceiro {
  id?: number;
  nomeFantasia?: string;
  razaoSocial?: string;
  cnpj?: string;
  tefelone?: number;
  email?: string | null;
  rua?: string | null;
  numero?: number | null;
  bairro?: string | null;
  cep?: string | null;
  cidade?: string | null;
  estado?: string | null;
  user?: IUser | null;
  historicoLocacaos?: IHistoricoLocacao[] | null;
  carteiras?: ICarteira[] | null;
}

export class Parceiro implements IParceiro {
  constructor(
    public id?: number,
    public nomeFantasia?: string,
    public razaoSocial?: string,
    public cnpj?: string,
    public tefelone?: number,
    public email?: string | null,
    public rua?: string | null,
    public numero?: number | null,
    public bairro?: string | null,
    public cep?: string | null,
    public cidade?: string | null,
    public estado?: string | null,
    public user?: IUser | null,
    public historicoLocacaos?: IHistoricoLocacao[] | null,
    public carteiras?: ICarteira[] | null
  ) {}
}

export function getParceiroIdentifier(parceiro: IParceiro): number | undefined {
  return parceiro.id;
}
