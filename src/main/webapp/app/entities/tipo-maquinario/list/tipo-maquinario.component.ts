import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITipoMaquinario } from '../tipo-maquinario.model';
import { TipoMaquinarioService } from '../service/tipo-maquinario.service';
import { TipoMaquinarioDeleteDialogComponent } from '../delete/tipo-maquinario-delete-dialog.component';

@Component({
  selector: 'jhi-tipo-maquinario',
  templateUrl: './tipo-maquinario.component.html',
})
export class TipoMaquinarioComponent implements OnInit {
  tipoMaquinarios?: ITipoMaquinario[];
  isLoading = false;

  constructor(protected tipoMaquinarioService: TipoMaquinarioService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.tipoMaquinarioService.query().subscribe(
      (res: HttpResponse<ITipoMaquinario[]>) => {
        this.isLoading = false;
        this.tipoMaquinarios = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ITipoMaquinario): number {
    return item.id!;
  }

  delete(tipoMaquinario: ITipoMaquinario): void {
    const modalRef = this.modalService.open(TipoMaquinarioDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tipoMaquinario = tipoMaquinario;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
