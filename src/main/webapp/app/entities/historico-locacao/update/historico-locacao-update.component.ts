import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IHistoricoLocacao, HistoricoLocacao } from '../historico-locacao.model';
import { HistoricoLocacaoService } from '../service/historico-locacao.service';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';
import { ParceiroService } from 'app/entities/parceiro/service/parceiro.service';

@Component({
  selector: 'jhi-historico-locacao-update',
  templateUrl: './historico-locacao-update.component.html',
})
export class HistoricoLocacaoUpdateComponent implements OnInit {
  isSaving = false;

  clientesSharedCollection: ICliente[] = [];
  parceirosSharedCollection: IParceiro[] = [];

  editForm = this.fb.group({
    id: [],
    data: [null, [Validators.required]],
    tempoLocacao: [null, [Validators.required]],
    valorTotal: [null, [Validators.required]],
    fornecedor: [null, [Validators.required]],
    cliente: [],
    parceiro: [],
  });

  constructor(
    protected historicoLocacaoService: HistoricoLocacaoService,
    protected clienteService: ClienteService,
    protected parceiroService: ParceiroService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ historicoLocacao }) => {
      this.updateForm(historicoLocacao);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const historicoLocacao = this.createFromForm();
    if (historicoLocacao.id !== undefined) {
      this.subscribeToSaveResponse(this.historicoLocacaoService.update(historicoLocacao));
    } else {
      this.subscribeToSaveResponse(this.historicoLocacaoService.create(historicoLocacao));
    }
  }

  trackClienteById(index: number, item: ICliente): number {
    return item.id!;
  }

  trackParceiroById(index: number, item: IParceiro): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistoricoLocacao>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(historicoLocacao: IHistoricoLocacao): void {
    this.editForm.patchValue({
      id: historicoLocacao.id,
      data: historicoLocacao.data,
      tempoLocacao: historicoLocacao.tempoLocacao,
      valorTotal: historicoLocacao.valorTotal,
      fornecedor: historicoLocacao.fornecedor,
      cliente: historicoLocacao.cliente,
      parceiro: historicoLocacao.parceiro,
    });

    this.clientesSharedCollection = this.clienteService.addClienteToCollectionIfMissing(
      this.clientesSharedCollection,
      historicoLocacao.cliente
    );
    this.parceirosSharedCollection = this.parceiroService.addParceiroToCollectionIfMissing(
      this.parceirosSharedCollection,
      historicoLocacao.parceiro
    );
  }

  protected loadRelationshipsOptions(): void {
    this.clienteService
      .query()
      .pipe(map((res: HttpResponse<ICliente[]>) => res.body ?? []))
      .pipe(
        map((clientes: ICliente[]) => this.clienteService.addClienteToCollectionIfMissing(clientes, this.editForm.get('cliente')!.value))
      )
      .subscribe((clientes: ICliente[]) => (this.clientesSharedCollection = clientes));

    this.parceiroService
      .query()
      .pipe(map((res: HttpResponse<IParceiro[]>) => res.body ?? []))
      .pipe(
        map((parceiros: IParceiro[]) =>
          this.parceiroService.addParceiroToCollectionIfMissing(parceiros, this.editForm.get('parceiro')!.value)
        )
      )
      .subscribe((parceiros: IParceiro[]) => (this.parceirosSharedCollection = parceiros));
  }

  protected createFromForm(): IHistoricoLocacao {
    return {
      ...new HistoricoLocacao(),
      id: this.editForm.get(['id'])!.value,
      data: this.editForm.get(['data'])!.value,
      tempoLocacao: this.editForm.get(['tempoLocacao'])!.value,
      valorTotal: this.editForm.get(['valorTotal'])!.value,
      fornecedor: this.editForm.get(['fornecedor'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
      parceiro: this.editForm.get(['parceiro'])!.value,
    };
  }
}
