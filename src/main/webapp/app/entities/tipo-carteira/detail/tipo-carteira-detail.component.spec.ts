import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TipoCarteiraDetailComponent } from './tipo-carteira-detail.component';

describe('TipoCarteira Management Detail Component', () => {
  let comp: TipoCarteiraDetailComponent;
  let fixture: ComponentFixture<TipoCarteiraDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TipoCarteiraDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ tipoCarteira: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TipoCarteiraDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TipoCarteiraDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load tipoCarteira on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.tipoCarteira).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
