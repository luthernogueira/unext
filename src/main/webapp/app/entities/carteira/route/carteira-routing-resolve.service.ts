import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICarteira, Carteira } from '../carteira.model';
import { CarteiraService } from '../service/carteira.service';

@Injectable({ providedIn: 'root' })
export class CarteiraRoutingResolveService implements Resolve<ICarteira> {
  constructor(protected service: CarteiraService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICarteira> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((carteira: HttpResponse<Carteira>) => {
          if (carteira.body) {
            return of(carteira.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Carteira());
  }
}
