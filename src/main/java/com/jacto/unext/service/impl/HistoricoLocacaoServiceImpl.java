package com.jacto.unext.service.impl;

import com.jacto.unext.domain.HistoricoLocacao;
import com.jacto.unext.repository.HistoricoLocacaoRepository;
import com.jacto.unext.service.HistoricoLocacaoService;
import com.jacto.unext.service.dto.HistoricoLocacaoDTO;
import com.jacto.unext.service.mapper.HistoricoLocacaoMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link HistoricoLocacao}.
 */
@Service
@Transactional
public class HistoricoLocacaoServiceImpl implements HistoricoLocacaoService {

    private final Logger log = LoggerFactory.getLogger(HistoricoLocacaoServiceImpl.class);

    private final HistoricoLocacaoRepository historicoLocacaoRepository;

    private final HistoricoLocacaoMapper historicoLocacaoMapper;

    public HistoricoLocacaoServiceImpl(
        HistoricoLocacaoRepository historicoLocacaoRepository,
        HistoricoLocacaoMapper historicoLocacaoMapper
    ) {
        this.historicoLocacaoRepository = historicoLocacaoRepository;
        this.historicoLocacaoMapper = historicoLocacaoMapper;
    }

    @Override
    public HistoricoLocacaoDTO save(HistoricoLocacaoDTO historicoLocacaoDTO) {
        log.debug("Request to save HistoricoLocacao : {}", historicoLocacaoDTO);
        HistoricoLocacao historicoLocacao = historicoLocacaoMapper.toEntity(historicoLocacaoDTO);
        historicoLocacao = historicoLocacaoRepository.save(historicoLocacao);
        return historicoLocacaoMapper.toDto(historicoLocacao);
    }

    @Override
    public Optional<HistoricoLocacaoDTO> partialUpdate(HistoricoLocacaoDTO historicoLocacaoDTO) {
        log.debug("Request to partially update HistoricoLocacao : {}", historicoLocacaoDTO);

        return historicoLocacaoRepository
            .findById(historicoLocacaoDTO.getId())
            .map(existingHistoricoLocacao -> {
                historicoLocacaoMapper.partialUpdate(existingHistoricoLocacao, historicoLocacaoDTO);

                return existingHistoricoLocacao;
            })
            .map(historicoLocacaoRepository::save)
            .map(historicoLocacaoMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HistoricoLocacaoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HistoricoLocacaos");
        return historicoLocacaoRepository.findAll(pageable).map(historicoLocacaoMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HistoricoLocacaoDTO> findOne(Long id) {
        log.debug("Request to get HistoricoLocacao : {}", id);
        return historicoLocacaoRepository.findById(id).map(historicoLocacaoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete HistoricoLocacao : {}", id);
        historicoLocacaoRepository.deleteById(id);
    }
}
