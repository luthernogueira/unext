package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.TipoCarteira;
import com.jacto.unext.repository.TipoCarteiraRepository;
import com.jacto.unext.service.dto.TipoCarteiraDTO;
import com.jacto.unext.service.mapper.TipoCarteiraMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TipoCarteiraResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TipoCarteiraResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String DEFAULT_BANDEIRA = "AAAAAAAAAA";
    private static final String UPDATED_BANDEIRA = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/tipo-carteiras";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TipoCarteiraRepository tipoCarteiraRepository;

    @Autowired
    private TipoCarteiraMapper tipoCarteiraMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTipoCarteiraMockMvc;

    private TipoCarteira tipoCarteira;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoCarteira createEntity(EntityManager em) {
        TipoCarteira tipoCarteira = new TipoCarteira().descricao(DEFAULT_DESCRICAO).bandeira(DEFAULT_BANDEIRA);
        return tipoCarteira;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoCarteira createUpdatedEntity(EntityManager em) {
        TipoCarteira tipoCarteira = new TipoCarteira().descricao(UPDATED_DESCRICAO).bandeira(UPDATED_BANDEIRA);
        return tipoCarteira;
    }

    @BeforeEach
    public void initTest() {
        tipoCarteira = createEntity(em);
    }

    @Test
    @Transactional
    void createTipoCarteira() throws Exception {
        int databaseSizeBeforeCreate = tipoCarteiraRepository.findAll().size();
        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);
        restTipoCarteiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isCreated());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeCreate + 1);
        TipoCarteira testTipoCarteira = tipoCarteiraList.get(tipoCarteiraList.size() - 1);
        assertThat(testTipoCarteira.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testTipoCarteira.getBandeira()).isEqualTo(DEFAULT_BANDEIRA);
    }

    @Test
    @Transactional
    void createTipoCarteiraWithExistingId() throws Exception {
        // Create the TipoCarteira with an existing ID
        tipoCarteira.setId(1L);
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        int databaseSizeBeforeCreate = tipoCarteiraRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoCarteiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDescricaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoCarteiraRepository.findAll().size();
        // set the field null
        tipoCarteira.setDescricao(null);

        // Create the TipoCarteira, which fails.
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        restTipoCarteiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBandeiraIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoCarteiraRepository.findAll().size();
        // set the field null
        tipoCarteira.setBandeira(null);

        // Create the TipoCarteira, which fails.
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        restTipoCarteiraMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTipoCarteiras() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        // Get all the tipoCarteiraList
        restTipoCarteiraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoCarteira.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].bandeira").value(hasItem(DEFAULT_BANDEIRA)));
    }

    @Test
    @Transactional
    void getTipoCarteira() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        // Get the tipoCarteira
        restTipoCarteiraMockMvc
            .perform(get(ENTITY_API_URL_ID, tipoCarteira.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tipoCarteira.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO))
            .andExpect(jsonPath("$.bandeira").value(DEFAULT_BANDEIRA));
    }

    @Test
    @Transactional
    void getNonExistingTipoCarteira() throws Exception {
        // Get the tipoCarteira
        restTipoCarteiraMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewTipoCarteira() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();

        // Update the tipoCarteira
        TipoCarteira updatedTipoCarteira = tipoCarteiraRepository.findById(tipoCarteira.getId()).get();
        // Disconnect from session so that the updates on updatedTipoCarteira are not directly saved in db
        em.detach(updatedTipoCarteira);
        updatedTipoCarteira.descricao(UPDATED_DESCRICAO).bandeira(UPDATED_BANDEIRA);
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(updatedTipoCarteira);

        restTipoCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tipoCarteiraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isOk());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
        TipoCarteira testTipoCarteira = tipoCarteiraList.get(tipoCarteiraList.size() - 1);
        assertThat(testTipoCarteira.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testTipoCarteira.getBandeira()).isEqualTo(UPDATED_BANDEIRA);
    }

    @Test
    @Transactional
    void putNonExistingTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tipoCarteiraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTipoCarteiraWithPatch() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();

        // Update the tipoCarteira using partial update
        TipoCarteira partialUpdatedTipoCarteira = new TipoCarteira();
        partialUpdatedTipoCarteira.setId(tipoCarteira.getId());

        partialUpdatedTipoCarteira.bandeira(UPDATED_BANDEIRA);

        restTipoCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTipoCarteira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTipoCarteira))
            )
            .andExpect(status().isOk());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
        TipoCarteira testTipoCarteira = tipoCarteiraList.get(tipoCarteiraList.size() - 1);
        assertThat(testTipoCarteira.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testTipoCarteira.getBandeira()).isEqualTo(UPDATED_BANDEIRA);
    }

    @Test
    @Transactional
    void fullUpdateTipoCarteiraWithPatch() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();

        // Update the tipoCarteira using partial update
        TipoCarteira partialUpdatedTipoCarteira = new TipoCarteira();
        partialUpdatedTipoCarteira.setId(tipoCarteira.getId());

        partialUpdatedTipoCarteira.descricao(UPDATED_DESCRICAO).bandeira(UPDATED_BANDEIRA);

        restTipoCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTipoCarteira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTipoCarteira))
            )
            .andExpect(status().isOk());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
        TipoCarteira testTipoCarteira = tipoCarteiraList.get(tipoCarteiraList.size() - 1);
        assertThat(testTipoCarteira.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testTipoCarteira.getBandeira()).isEqualTo(UPDATED_BANDEIRA);
    }

    @Test
    @Transactional
    void patchNonExistingTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, tipoCarteiraDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTipoCarteira() throws Exception {
        int databaseSizeBeforeUpdate = tipoCarteiraRepository.findAll().size();
        tipoCarteira.setId(count.incrementAndGet());

        // Create the TipoCarteira
        TipoCarteiraDTO tipoCarteiraDTO = tipoCarteiraMapper.toDto(tipoCarteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoCarteiraDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TipoCarteira in the database
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTipoCarteira() throws Exception {
        // Initialize the database
        tipoCarteiraRepository.saveAndFlush(tipoCarteira);

        int databaseSizeBeforeDelete = tipoCarteiraRepository.findAll().size();

        // Delete the tipoCarteira
        restTipoCarteiraMockMvc
            .perform(delete(ENTITY_API_URL_ID, tipoCarteira.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoCarteira> tipoCarteiraList = tipoCarteiraRepository.findAll();
        assertThat(tipoCarteiraList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
