package com.jacto.unext.web.rest;

import com.jacto.unext.repository.TipoMaquinarioRepository;
import com.jacto.unext.service.TipoMaquinarioService;
import com.jacto.unext.service.dto.TipoMaquinarioDTO;
import com.jacto.unext.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jacto.unext.domain.TipoMaquinario}.
 */
@RestController
@RequestMapping("/api")
public class TipoMaquinarioResource {

    private final Logger log = LoggerFactory.getLogger(TipoMaquinarioResource.class);

    private static final String ENTITY_NAME = "tipoMaquinario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoMaquinarioService tipoMaquinarioService;

    private final TipoMaquinarioRepository tipoMaquinarioRepository;

    public TipoMaquinarioResource(TipoMaquinarioService tipoMaquinarioService, TipoMaquinarioRepository tipoMaquinarioRepository) {
        this.tipoMaquinarioService = tipoMaquinarioService;
        this.tipoMaquinarioRepository = tipoMaquinarioRepository;
    }

    /**
     * {@code POST  /tipo-maquinarios} : Create a new tipoMaquinario.
     *
     * @param tipoMaquinarioDTO the tipoMaquinarioDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoMaquinarioDTO, or with status {@code 400 (Bad Request)} if the tipoMaquinario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-maquinarios")
    public ResponseEntity<TipoMaquinarioDTO> createTipoMaquinario(@Valid @RequestBody TipoMaquinarioDTO tipoMaquinarioDTO)
        throws URISyntaxException {
        log.debug("REST request to save TipoMaquinario : {}", tipoMaquinarioDTO);
        if (tipoMaquinarioDTO.getId() != null) {
            throw new BadRequestAlertException("A new tipoMaquinario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoMaquinarioDTO result = tipoMaquinarioService.save(tipoMaquinarioDTO);
        return ResponseEntity
            .created(new URI("/api/tipo-maquinarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-maquinarios/:id} : Updates an existing tipoMaquinario.
     *
     * @param id the id of the tipoMaquinarioDTO to save.
     * @param tipoMaquinarioDTO the tipoMaquinarioDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoMaquinarioDTO,
     * or with status {@code 400 (Bad Request)} if the tipoMaquinarioDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoMaquinarioDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-maquinarios/{id}")
    public ResponseEntity<TipoMaquinarioDTO> updateTipoMaquinario(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TipoMaquinarioDTO tipoMaquinarioDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TipoMaquinario : {}, {}", id, tipoMaquinarioDTO);
        if (tipoMaquinarioDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tipoMaquinarioDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tipoMaquinarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TipoMaquinarioDTO result = tipoMaquinarioService.save(tipoMaquinarioDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tipoMaquinarioDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /tipo-maquinarios/:id} : Partial updates given fields of an existing tipoMaquinario, field will ignore if it is null
     *
     * @param id the id of the tipoMaquinarioDTO to save.
     * @param tipoMaquinarioDTO the tipoMaquinarioDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoMaquinarioDTO,
     * or with status {@code 400 (Bad Request)} if the tipoMaquinarioDTO is not valid,
     * or with status {@code 404 (Not Found)} if the tipoMaquinarioDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the tipoMaquinarioDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tipo-maquinarios/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TipoMaquinarioDTO> partialUpdateTipoMaquinario(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TipoMaquinarioDTO tipoMaquinarioDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TipoMaquinario partially : {}, {}", id, tipoMaquinarioDTO);
        if (tipoMaquinarioDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tipoMaquinarioDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tipoMaquinarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TipoMaquinarioDTO> result = tipoMaquinarioService.partialUpdate(tipoMaquinarioDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tipoMaquinarioDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /tipo-maquinarios} : get all the tipoMaquinarios.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoMaquinarios in body.
     */
    @GetMapping("/tipo-maquinarios")
    public List<TipoMaquinarioDTO> getAllTipoMaquinarios() {
        log.debug("REST request to get all TipoMaquinarios");
        return tipoMaquinarioService.findAll();
    }

    /**
     * {@code GET  /tipo-maquinarios/:id} : get the "id" tipoMaquinario.
     *
     * @param id the id of the tipoMaquinarioDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoMaquinarioDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-maquinarios/{id}")
    public ResponseEntity<TipoMaquinarioDTO> getTipoMaquinario(@PathVariable Long id) {
        log.debug("REST request to get TipoMaquinario : {}", id);
        Optional<TipoMaquinarioDTO> tipoMaquinarioDTO = tipoMaquinarioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoMaquinarioDTO);
    }

    /**
     * {@code DELETE  /tipo-maquinarios/:id} : delete the "id" tipoMaquinario.
     *
     * @param id the id of the tipoMaquinarioDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-maquinarios/{id}")
    public ResponseEntity<Void> deleteTipoMaquinario(@PathVariable Long id) {
        log.debug("REST request to delete TipoMaquinario : {}", id);
        tipoMaquinarioService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
