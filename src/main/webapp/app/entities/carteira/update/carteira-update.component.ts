import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICarteira, Carteira } from '../carteira.model';
import { CarteiraService } from '../service/carteira.service';
import { ITipoCarteira } from 'app/entities/tipo-carteira/tipo-carteira.model';
import { TipoCarteiraService } from 'app/entities/tipo-carteira/service/tipo-carteira.service';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';
import { ParceiroService } from 'app/entities/parceiro/service/parceiro.service';

@Component({
  selector: 'jhi-carteira-update',
  templateUrl: './carteira-update.component.html',
})
export class CarteiraUpdateComponent implements OnInit {
  isSaving = false;

  tipoCarteirasSharedCollection: ITipoCarteira[] = [];
  clientesSharedCollection: ICliente[] = [];
  parceirosSharedCollection: IParceiro[] = [];

  editForm = this.fb.group({
    id: [],
    titular: [null, [Validators.required]],
    numeroConta: [null, [Validators.required]],
    tipoCarteira: [],
    cliente: [],
    parceiro: [],
  });

  constructor(
    protected carteiraService: CarteiraService,
    protected tipoCarteiraService: TipoCarteiraService,
    protected clienteService: ClienteService,
    protected parceiroService: ParceiroService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ carteira }) => {
      this.updateForm(carteira);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const carteira = this.createFromForm();
    if (carteira.id !== undefined) {
      this.subscribeToSaveResponse(this.carteiraService.update(carteira));
    } else {
      this.subscribeToSaveResponse(this.carteiraService.create(carteira));
    }
  }

  trackTipoCarteiraById(index: number, item: ITipoCarteira): number {
    return item.id!;
  }

  trackClienteById(index: number, item: ICliente): number {
    return item.id!;
  }

  trackParceiroById(index: number, item: IParceiro): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICarteira>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(carteira: ICarteira): void {
    this.editForm.patchValue({
      id: carteira.id,
      titular: carteira.titular,
      numeroConta: carteira.numeroConta,
      tipoCarteira: carteira.tipoCarteira,
      cliente: carteira.cliente,
      parceiro: carteira.parceiro,
    });

    this.tipoCarteirasSharedCollection = this.tipoCarteiraService.addTipoCarteiraToCollectionIfMissing(
      this.tipoCarteirasSharedCollection,
      carteira.tipoCarteira
    );
    this.clientesSharedCollection = this.clienteService.addClienteToCollectionIfMissing(this.clientesSharedCollection, carteira.cliente);
    this.parceirosSharedCollection = this.parceiroService.addParceiroToCollectionIfMissing(
      this.parceirosSharedCollection,
      carteira.parceiro
    );
  }

  protected loadRelationshipsOptions(): void {
    this.tipoCarteiraService
      .query()
      .pipe(map((res: HttpResponse<ITipoCarteira[]>) => res.body ?? []))
      .pipe(
        map((tipoCarteiras: ITipoCarteira[]) =>
          this.tipoCarteiraService.addTipoCarteiraToCollectionIfMissing(tipoCarteiras, this.editForm.get('tipoCarteira')!.value)
        )
      )
      .subscribe((tipoCarteiras: ITipoCarteira[]) => (this.tipoCarteirasSharedCollection = tipoCarteiras));

    this.clienteService
      .query()
      .pipe(map((res: HttpResponse<ICliente[]>) => res.body ?? []))
      .pipe(
        map((clientes: ICliente[]) => this.clienteService.addClienteToCollectionIfMissing(clientes, this.editForm.get('cliente')!.value))
      )
      .subscribe((clientes: ICliente[]) => (this.clientesSharedCollection = clientes));

    this.parceiroService
      .query()
      .pipe(map((res: HttpResponse<IParceiro[]>) => res.body ?? []))
      .pipe(
        map((parceiros: IParceiro[]) =>
          this.parceiroService.addParceiroToCollectionIfMissing(parceiros, this.editForm.get('parceiro')!.value)
        )
      )
      .subscribe((parceiros: IParceiro[]) => (this.parceirosSharedCollection = parceiros));
  }

  protected createFromForm(): ICarteira {
    return {
      ...new Carteira(),
      id: this.editForm.get(['id'])!.value,
      titular: this.editForm.get(['titular'])!.value,
      numeroConta: this.editForm.get(['numeroConta'])!.value,
      tipoCarteira: this.editForm.get(['tipoCarteira'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
      parceiro: this.editForm.get(['parceiro'])!.value,
    };
  }
}
