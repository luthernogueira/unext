package com.jacto.unext.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CarteiraTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Carteira.class);
        Carteira carteira1 = new Carteira();
        carteira1.setId(1L);
        Carteira carteira2 = new Carteira();
        carteira2.setId(carteira1.getId());
        assertThat(carteira1).isEqualTo(carteira2);
        carteira2.setId(2L);
        assertThat(carteira1).isNotEqualTo(carteira2);
        carteira1.setId(null);
        assertThat(carteira1).isNotEqualTo(carteira2);
    }
}
