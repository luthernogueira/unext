package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Parceiro.
 */
@Entity
@Table(name = "parceiro")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Parceiro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome_fantasia", nullable = false)
    private String nomeFantasia;

    @NotNull
    @Column(name = "razao_social", nullable = false)
    private String razaoSocial;

    @NotNull
    @Column(name = "cnpj", nullable = false, unique = true)
    private String cnpj;

    @NotNull
    @Column(name = "tefelone", nullable = false)
    private Integer tefelone;

    @Column(name = "email")
    private String email;

    @Column(name = "rua")
    private String rua;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "cep")
    private String cep;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "estado")
    private String estado;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "parceiro")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "maquinarios", "cliente", "parceiro" }, allowSetters = true)
    private Set<HistoricoLocacao> historicoLocacaos = new HashSet<>();

    @OneToMany(mappedBy = "parceiro")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tipoCarteira", "cliente", "parceiro" }, allowSetters = true)
    private Set<Carteira> carteiras = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Parceiro id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeFantasia() {
        return this.nomeFantasia;
    }

    public Parceiro nomeFantasia(String nomeFantasia) {
        this.setNomeFantasia(nomeFantasia);
        return this;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRazaoSocial() {
        return this.razaoSocial;
    }

    public Parceiro razaoSocial(String razaoSocial) {
        this.setRazaoSocial(razaoSocial);
        return this;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return this.cnpj;
    }

    public Parceiro cnpj(String cnpj) {
        this.setCnpj(cnpj);
        return this;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Integer getTefelone() {
        return this.tefelone;
    }

    public Parceiro tefelone(Integer tefelone) {
        this.setTefelone(tefelone);
        return this;
    }

    public void setTefelone(Integer tefelone) {
        this.tefelone = tefelone;
    }

    public String getEmail() {
        return this.email;
    }

    public Parceiro email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRua() {
        return this.rua;
    }

    public Parceiro rua(String rua) {
        this.setRua(rua);
        return this;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Parceiro numero(Integer numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return this.bairro;
    }

    public Parceiro bairro(String bairro) {
        this.setBairro(bairro);
        return this;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return this.cep;
    }

    public Parceiro cep(String cep) {
        this.setCep(cep);
        return this;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return this.cidade;
    }

    public Parceiro cidade(String cidade) {
        this.setCidade(cidade);
        return this;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return this.estado;
    }

    public Parceiro estado(String estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Parceiro user(User user) {
        this.setUser(user);
        return this;
    }

    public Set<HistoricoLocacao> getHistoricoLocacaos() {
        return this.historicoLocacaos;
    }

    public void setHistoricoLocacaos(Set<HistoricoLocacao> historicoLocacaos) {
        if (this.historicoLocacaos != null) {
            this.historicoLocacaos.forEach(i -> i.setParceiro(null));
        }
        if (historicoLocacaos != null) {
            historicoLocacaos.forEach(i -> i.setParceiro(this));
        }
        this.historicoLocacaos = historicoLocacaos;
    }

    public Parceiro historicoLocacaos(Set<HistoricoLocacao> historicoLocacaos) {
        this.setHistoricoLocacaos(historicoLocacaos);
        return this;
    }

    public Parceiro addHistoricoLocacao(HistoricoLocacao historicoLocacao) {
        this.historicoLocacaos.add(historicoLocacao);
        historicoLocacao.setParceiro(this);
        return this;
    }

    public Parceiro removeHistoricoLocacao(HistoricoLocacao historicoLocacao) {
        this.historicoLocacaos.remove(historicoLocacao);
        historicoLocacao.setParceiro(null);
        return this;
    }

    public Set<Carteira> getCarteiras() {
        return this.carteiras;
    }

    public void setCarteiras(Set<Carteira> carteiras) {
        if (this.carteiras != null) {
            this.carteiras.forEach(i -> i.setParceiro(null));
        }
        if (carteiras != null) {
            carteiras.forEach(i -> i.setParceiro(this));
        }
        this.carteiras = carteiras;
    }

    public Parceiro carteiras(Set<Carteira> carteiras) {
        this.setCarteiras(carteiras);
        return this;
    }

    public Parceiro addCarteira(Carteira carteira) {
        this.carteiras.add(carteira);
        carteira.setParceiro(this);
        return this;
    }

    public Parceiro removeCarteira(Carteira carteira) {
        this.carteiras.remove(carteira);
        carteira.setParceiro(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Parceiro)) {
            return false;
        }
        return id != null && id.equals(((Parceiro) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Parceiro{" +
            "id=" + getId() +
            ", nomeFantasia='" + getNomeFantasia() + "'" +
            ", razaoSocial='" + getRazaoSocial() + "'" +
            ", cnpj='" + getCnpj() + "'" +
            ", tefelone=" + getTefelone() +
            ", email='" + getEmail() + "'" +
            ", rua='" + getRua() + "'" +
            ", numero=" + getNumero() +
            ", bairro='" + getBairro() + "'" +
            ", cep='" + getCep() + "'" +
            ", cidade='" + getCidade() + "'" +
            ", estado='" + getEstado() + "'" +
            "}";
    }
}
