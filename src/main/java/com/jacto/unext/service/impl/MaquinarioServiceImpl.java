package com.jacto.unext.service.impl;

import com.jacto.unext.domain.Maquinario;
import com.jacto.unext.repository.MaquinarioRepository;
import com.jacto.unext.service.MaquinarioService;
import com.jacto.unext.service.dto.MaquinarioDTO;
import com.jacto.unext.service.mapper.MaquinarioMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Maquinario}.
 */
@Service
@Transactional
public class MaquinarioServiceImpl implements MaquinarioService {

    private final Logger log = LoggerFactory.getLogger(MaquinarioServiceImpl.class);

    private final MaquinarioRepository maquinarioRepository;

    private final MaquinarioMapper maquinarioMapper;

    public MaquinarioServiceImpl(MaquinarioRepository maquinarioRepository, MaquinarioMapper maquinarioMapper) {
        this.maquinarioRepository = maquinarioRepository;
        this.maquinarioMapper = maquinarioMapper;
    }

    @Override
    public MaquinarioDTO save(MaquinarioDTO maquinarioDTO) {
        log.debug("Request to save Maquinario : {}", maquinarioDTO);
        Maquinario maquinario = maquinarioMapper.toEntity(maquinarioDTO);
        maquinario = maquinarioRepository.save(maquinario);
        return maquinarioMapper.toDto(maquinario);
    }

    @Override
    public Optional<MaquinarioDTO> partialUpdate(MaquinarioDTO maquinarioDTO) {
        log.debug("Request to partially update Maquinario : {}", maquinarioDTO);

        return maquinarioRepository
            .findById(maquinarioDTO.getId())
            .map(existingMaquinario -> {
                maquinarioMapper.partialUpdate(existingMaquinario, maquinarioDTO);

                return existingMaquinario;
            })
            .map(maquinarioRepository::save)
            .map(maquinarioMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MaquinarioDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Maquinarios");
        return maquinarioRepository.findAll(pageable).map(maquinarioMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MaquinarioDTO> findOne(Long id) {
        log.debug("Request to get Maquinario : {}", id);
        return maquinarioRepository.findById(id).map(maquinarioMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Maquinario : {}", id);
        maquinarioRepository.deleteById(id);
    }
}
