package com.jacto.unext.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HistoricoLocacaoDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HistoricoLocacaoDTO.class);
        HistoricoLocacaoDTO historicoLocacaoDTO1 = new HistoricoLocacaoDTO();
        historicoLocacaoDTO1.setId(1L);
        HistoricoLocacaoDTO historicoLocacaoDTO2 = new HistoricoLocacaoDTO();
        assertThat(historicoLocacaoDTO1).isNotEqualTo(historicoLocacaoDTO2);
        historicoLocacaoDTO2.setId(historicoLocacaoDTO1.getId());
        assertThat(historicoLocacaoDTO1).isEqualTo(historicoLocacaoDTO2);
        historicoLocacaoDTO2.setId(2L);
        assertThat(historicoLocacaoDTO1).isNotEqualTo(historicoLocacaoDTO2);
        historicoLocacaoDTO1.setId(null);
        assertThat(historicoLocacaoDTO1).isNotEqualTo(historicoLocacaoDTO2);
    }
}
