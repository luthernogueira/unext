package com.jacto.unext.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jacto.unext.domain.TipoCarteira} entity.
 */
public class TipoCarteiraDTO implements Serializable {

    private Long id;

    @NotNull
    private String descricao;

    @NotNull
    private String bandeira;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoCarteiraDTO)) {
            return false;
        }

        TipoCarteiraDTO tipoCarteiraDTO = (TipoCarteiraDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, tipoCarteiraDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TipoCarteiraDTO{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            ", bandeira='" + getBandeira() + "'" +
            "}";
    }
}
