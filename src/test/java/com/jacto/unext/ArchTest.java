package com.jacto.unext;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.jacto.unext");

        noClasses()
            .that()
            .resideInAnyPackage("com.jacto.unext.service..")
            .or()
            .resideInAnyPackage("com.jacto.unext.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.jacto.unext.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
