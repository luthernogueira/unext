package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A TipoCarteira.
 */
@Entity
@Table(name = "tipo_carteira")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TipoCarteira implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @NotNull
    @Column(name = "bandeira", nullable = false)
    private String bandeira;

    @OneToMany(mappedBy = "tipoCarteira")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tipoCarteira", "cliente", "parceiro" }, allowSetters = true)
    private Set<Carteira> carteiras = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TipoCarteira id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public TipoCarteira descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getBandeira() {
        return this.bandeira;
    }

    public TipoCarteira bandeira(String bandeira) {
        this.setBandeira(bandeira);
        return this;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public Set<Carteira> getCarteiras() {
        return this.carteiras;
    }

    public void setCarteiras(Set<Carteira> carteiras) {
        if (this.carteiras != null) {
            this.carteiras.forEach(i -> i.setTipoCarteira(null));
        }
        if (carteiras != null) {
            carteiras.forEach(i -> i.setTipoCarteira(this));
        }
        this.carteiras = carteiras;
    }

    public TipoCarteira carteiras(Set<Carteira> carteiras) {
        this.setCarteiras(carteiras);
        return this;
    }

    public TipoCarteira addCarteira(Carteira carteira) {
        this.carteiras.add(carteira);
        carteira.setTipoCarteira(this);
        return this;
    }

    public TipoCarteira removeCarteira(Carteira carteira) {
        this.carteiras.remove(carteira);
        carteira.setTipoCarteira(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoCarteira)) {
            return false;
        }
        return id != null && id.equals(((TipoCarteira) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TipoCarteira{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            ", bandeira='" + getBandeira() + "'" +
            "}";
    }
}
