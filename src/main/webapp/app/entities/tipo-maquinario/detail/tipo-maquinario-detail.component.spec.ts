import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TipoMaquinarioDetailComponent } from './tipo-maquinario-detail.component';

describe('TipoMaquinario Management Detail Component', () => {
  let comp: TipoMaquinarioDetailComponent;
  let fixture: ComponentFixture<TipoMaquinarioDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TipoMaquinarioDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ tipoMaquinario: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TipoMaquinarioDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TipoMaquinarioDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load tipoMaquinario on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.tipoMaquinario).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
