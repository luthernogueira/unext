import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { HistoricoLocacaoComponent } from '../list/historico-locacao.component';
import { HistoricoLocacaoDetailComponent } from '../detail/historico-locacao-detail.component';
import { HistoricoLocacaoUpdateComponent } from '../update/historico-locacao-update.component';
import { HistoricoLocacaoRoutingResolveService } from './historico-locacao-routing-resolve.service';

const historicoLocacaoRoute: Routes = [
  {
    path: '',
    component: HistoricoLocacaoComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HistoricoLocacaoDetailComponent,
    resolve: {
      historicoLocacao: HistoricoLocacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HistoricoLocacaoUpdateComponent,
    resolve: {
      historicoLocacao: HistoricoLocacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HistoricoLocacaoUpdateComponent,
    resolve: {
      historicoLocacao: HistoricoLocacaoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(historicoLocacaoRoute)],
  exports: [RouterModule],
})
export class HistoricoLocacaoRoutingModule {}
