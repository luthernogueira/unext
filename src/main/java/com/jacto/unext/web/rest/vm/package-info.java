/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jacto.unext.web.rest.vm;
