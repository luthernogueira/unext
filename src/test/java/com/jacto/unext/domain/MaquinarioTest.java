package com.jacto.unext.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MaquinarioTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Maquinario.class);
        Maquinario maquinario1 = new Maquinario();
        maquinario1.setId(1L);
        Maquinario maquinario2 = new Maquinario();
        maquinario2.setId(maquinario1.getId());
        assertThat(maquinario1).isEqualTo(maquinario2);
        maquinario2.setId(2L);
        assertThat(maquinario1).isNotEqualTo(maquinario2);
        maquinario1.setId(null);
        assertThat(maquinario1).isNotEqualTo(maquinario2);
    }
}
