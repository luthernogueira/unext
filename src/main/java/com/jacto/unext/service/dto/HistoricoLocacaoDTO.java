package com.jacto.unext.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jacto.unext.domain.HistoricoLocacao} entity.
 */
public class HistoricoLocacaoDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate data;

    @NotNull
    private Float tempoLocacao;

    @NotNull
    private Float valorTotal;

    @NotNull
    private String fornecedor;

    private ClienteDTO cliente;

    private ParceiroDTO parceiro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Float getTempoLocacao() {
        return tempoLocacao;
    }

    public void setTempoLocacao(Float tempoLocacao) {
        this.tempoLocacao = tempoLocacao;
    }

    public Float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public ParceiroDTO getParceiro() {
        return parceiro;
    }

    public void setParceiro(ParceiroDTO parceiro) {
        this.parceiro = parceiro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HistoricoLocacaoDTO)) {
            return false;
        }

        HistoricoLocacaoDTO historicoLocacaoDTO = (HistoricoLocacaoDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, historicoLocacaoDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HistoricoLocacaoDTO{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", tempoLocacao=" + getTempoLocacao() +
            ", valorTotal=" + getValorTotal() +
            ", fornecedor='" + getFornecedor() + "'" +
            ", cliente=" + getCliente() +
            ", parceiro=" + getParceiro() +
            "}";
    }
}
