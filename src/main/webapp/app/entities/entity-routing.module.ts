import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'parceiro',
        data: { pageTitle: 'uNextApp.parceiro.home.title' },
        loadChildren: () => import('./parceiro/parceiro.module').then(m => m.ParceiroModule),
      },
      {
        path: 'cliente',
        data: { pageTitle: 'uNextApp.cliente.home.title' },
        loadChildren: () => import('./cliente/cliente.module').then(m => m.ClienteModule),
      },
      {
        path: 'tipo-maquinario',
        data: { pageTitle: 'uNextApp.tipoMaquinario.home.title' },
        loadChildren: () => import('./tipo-maquinario/tipo-maquinario.module').then(m => m.TipoMaquinarioModule),
      },
      {
        path: 'maquinario',
        data: { pageTitle: 'uNextApp.maquinario.home.title' },
        loadChildren: () => import('./maquinario/maquinario.module').then(m => m.MaquinarioModule),
      },
      {
        path: 'historico-locacao',
        data: { pageTitle: 'uNextApp.historicoLocacao.home.title' },
        loadChildren: () => import('./historico-locacao/historico-locacao.module').then(m => m.HistoricoLocacaoModule),
      },
      {
        path: 'tipo-carteira',
        data: { pageTitle: 'uNextApp.tipoCarteira.home.title' },
        loadChildren: () => import('./tipo-carteira/tipo-carteira.module').then(m => m.TipoCarteiraModule),
      },
      {
        path: 'carteira',
        data: { pageTitle: 'uNextApp.carteira.home.title' },
        loadChildren: () => import('./carteira/carteira.module').then(m => m.CarteiraModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
