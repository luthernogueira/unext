jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CarteiraService } from '../service/carteira.service';
import { ICarteira, Carteira } from '../carteira.model';
import { ITipoCarteira } from 'app/entities/tipo-carteira/tipo-carteira.model';
import { TipoCarteiraService } from 'app/entities/tipo-carteira/service/tipo-carteira.service';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';
import { ParceiroService } from 'app/entities/parceiro/service/parceiro.service';

import { CarteiraUpdateComponent } from './carteira-update.component';

describe('Carteira Management Update Component', () => {
  let comp: CarteiraUpdateComponent;
  let fixture: ComponentFixture<CarteiraUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let carteiraService: CarteiraService;
  let tipoCarteiraService: TipoCarteiraService;
  let clienteService: ClienteService;
  let parceiroService: ParceiroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CarteiraUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(CarteiraUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CarteiraUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    carteiraService = TestBed.inject(CarteiraService);
    tipoCarteiraService = TestBed.inject(TipoCarteiraService);
    clienteService = TestBed.inject(ClienteService);
    parceiroService = TestBed.inject(ParceiroService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call TipoCarteira query and add missing value', () => {
      const carteira: ICarteira = { id: 456 };
      const tipoCarteira: ITipoCarteira = { id: 60896 };
      carteira.tipoCarteira = tipoCarteira;

      const tipoCarteiraCollection: ITipoCarteira[] = [{ id: 38870 }];
      jest.spyOn(tipoCarteiraService, 'query').mockReturnValue(of(new HttpResponse({ body: tipoCarteiraCollection })));
      const additionalTipoCarteiras = [tipoCarteira];
      const expectedCollection: ITipoCarteira[] = [...additionalTipoCarteiras, ...tipoCarteiraCollection];
      jest.spyOn(tipoCarteiraService, 'addTipoCarteiraToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      expect(tipoCarteiraService.query).toHaveBeenCalled();
      expect(tipoCarteiraService.addTipoCarteiraToCollectionIfMissing).toHaveBeenCalledWith(
        tipoCarteiraCollection,
        ...additionalTipoCarteiras
      );
      expect(comp.tipoCarteirasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Cliente query and add missing value', () => {
      const carteira: ICarteira = { id: 456 };
      const cliente: ICliente = { id: 36137 };
      carteira.cliente = cliente;

      const clienteCollection: ICliente[] = [{ id: 6871 }];
      jest.spyOn(clienteService, 'query').mockReturnValue(of(new HttpResponse({ body: clienteCollection })));
      const additionalClientes = [cliente];
      const expectedCollection: ICliente[] = [...additionalClientes, ...clienteCollection];
      jest.spyOn(clienteService, 'addClienteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      expect(clienteService.query).toHaveBeenCalled();
      expect(clienteService.addClienteToCollectionIfMissing).toHaveBeenCalledWith(clienteCollection, ...additionalClientes);
      expect(comp.clientesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Parceiro query and add missing value', () => {
      const carteira: ICarteira = { id: 456 };
      const parceiro: IParceiro = { id: 95750 };
      carteira.parceiro = parceiro;

      const parceiroCollection: IParceiro[] = [{ id: 37999 }];
      jest.spyOn(parceiroService, 'query').mockReturnValue(of(new HttpResponse({ body: parceiroCollection })));
      const additionalParceiros = [parceiro];
      const expectedCollection: IParceiro[] = [...additionalParceiros, ...parceiroCollection];
      jest.spyOn(parceiroService, 'addParceiroToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      expect(parceiroService.query).toHaveBeenCalled();
      expect(parceiroService.addParceiroToCollectionIfMissing).toHaveBeenCalledWith(parceiroCollection, ...additionalParceiros);
      expect(comp.parceirosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const carteira: ICarteira = { id: 456 };
      const tipoCarteira: ITipoCarteira = { id: 56587 };
      carteira.tipoCarteira = tipoCarteira;
      const cliente: ICliente = { id: 74352 };
      carteira.cliente = cliente;
      const parceiro: IParceiro = { id: 45188 };
      carteira.parceiro = parceiro;

      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(carteira));
      expect(comp.tipoCarteirasSharedCollection).toContain(tipoCarteira);
      expect(comp.clientesSharedCollection).toContain(cliente);
      expect(comp.parceirosSharedCollection).toContain(parceiro);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Carteira>>();
      const carteira = { id: 123 };
      jest.spyOn(carteiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: carteira }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(carteiraService.update).toHaveBeenCalledWith(carteira);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Carteira>>();
      const carteira = new Carteira();
      jest.spyOn(carteiraService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: carteira }));
      saveSubject.complete();

      // THEN
      expect(carteiraService.create).toHaveBeenCalledWith(carteira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Carteira>>();
      const carteira = { id: 123 };
      jest.spyOn(carteiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ carteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(carteiraService.update).toHaveBeenCalledWith(carteira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackTipoCarteiraById', () => {
      it('Should return tracked TipoCarteira primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTipoCarteiraById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackClienteById', () => {
      it('Should return tracked Cliente primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackClienteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackParceiroById', () => {
      it('Should return tracked Parceiro primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackParceiroById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
