package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A TipoMaquinario.
 */
@Entity
@Table(name = "tipo_maquinario")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TipoMaquinario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @OneToMany(mappedBy = "tipoMaquinario")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "historicoLocacao", "tipoMaquinario" }, allowSetters = true)
    private Set<Maquinario> maquinarios = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TipoMaquinario id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public TipoMaquinario descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set<Maquinario> getMaquinarios() {
        return this.maquinarios;
    }

    public void setMaquinarios(Set<Maquinario> maquinarios) {
        if (this.maquinarios != null) {
            this.maquinarios.forEach(i -> i.setTipoMaquinario(null));
        }
        if (maquinarios != null) {
            maquinarios.forEach(i -> i.setTipoMaquinario(this));
        }
        this.maquinarios = maquinarios;
    }

    public TipoMaquinario maquinarios(Set<Maquinario> maquinarios) {
        this.setMaquinarios(maquinarios);
        return this;
    }

    public TipoMaquinario addMaquinario(Maquinario maquinario) {
        this.maquinarios.add(maquinario);
        maquinario.setTipoMaquinario(this);
        return this;
    }

    public TipoMaquinario removeMaquinario(Maquinario maquinario) {
        this.maquinarios.remove(maquinario);
        maquinario.setTipoMaquinario(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoMaquinario)) {
            return false;
        }
        return id != null && id.equals(((TipoMaquinario) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TipoMaquinario{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
