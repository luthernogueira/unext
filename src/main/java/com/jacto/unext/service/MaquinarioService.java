package com.jacto.unext.service;

import com.jacto.unext.service.dto.MaquinarioDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.Maquinario}.
 */
public interface MaquinarioService {
    /**
     * Save a maquinario.
     *
     * @param maquinarioDTO the entity to save.
     * @return the persisted entity.
     */
    MaquinarioDTO save(MaquinarioDTO maquinarioDTO);

    /**
     * Partially updates a maquinario.
     *
     * @param maquinarioDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MaquinarioDTO> partialUpdate(MaquinarioDTO maquinarioDTO);

    /**
     * Get all the maquinarios.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaquinarioDTO> findAll(Pageable pageable);

    /**
     * Get the "id" maquinario.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaquinarioDTO> findOne(Long id);

    /**
     * Delete the "id" maquinario.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
