import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IParceiro, Parceiro } from '../parceiro.model';
import { ParceiroService } from '../service/parceiro.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-parceiro-update',
  templateUrl: './parceiro-update.component.html',
})
export class ParceiroUpdateComponent implements OnInit {
  isSaving = false;

  usersSharedCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    nomeFantasia: [null, [Validators.required]],
    razaoSocial: [null, [Validators.required]],
    cnpj: [null, [Validators.required]],
    tefelone: [null, [Validators.required]],
    email: [],
    rua: [],
    numero: [],
    bairro: [],
    cep: [],
    cidade: [],
    estado: [],
    user: [],
  });

  constructor(
    protected parceiroService: ParceiroService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ parceiro }) => {
      this.updateForm(parceiro);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const parceiro = this.createFromForm();
    if (parceiro.id !== undefined) {
      this.subscribeToSaveResponse(this.parceiroService.update(parceiro));
    } else {
      this.subscribeToSaveResponse(this.parceiroService.create(parceiro));
    }
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParceiro>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(parceiro: IParceiro): void {
    this.editForm.patchValue({
      id: parceiro.id,
      nomeFantasia: parceiro.nomeFantasia,
      razaoSocial: parceiro.razaoSocial,
      cnpj: parceiro.cnpj,
      tefelone: parceiro.tefelone,
      email: parceiro.email,
      rua: parceiro.rua,
      numero: parceiro.numero,
      bairro: parceiro.bairro,
      cep: parceiro.cep,
      cidade: parceiro.cidade,
      estado: parceiro.estado,
      user: parceiro.user,
    });

    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, parceiro.user);
  }

  protected loadRelationshipsOptions(): void {
    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): IParceiro {
    return {
      ...new Parceiro(),
      id: this.editForm.get(['id'])!.value,
      nomeFantasia: this.editForm.get(['nomeFantasia'])!.value,
      razaoSocial: this.editForm.get(['razaoSocial'])!.value,
      cnpj: this.editForm.get(['cnpj'])!.value,
      tefelone: this.editForm.get(['tefelone'])!.value,
      email: this.editForm.get(['email'])!.value,
      rua: this.editForm.get(['rua'])!.value,
      numero: this.editForm.get(['numero'])!.value,
      bairro: this.editForm.get(['bairro'])!.value,
      cep: this.editForm.get(['cep'])!.value,
      cidade: this.editForm.get(['cidade'])!.value,
      estado: this.editForm.get(['estado'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
