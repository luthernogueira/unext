package com.jacto.unext.service.impl;

import com.jacto.unext.domain.Parceiro;
import com.jacto.unext.repository.ParceiroRepository;
import com.jacto.unext.service.ParceiroService;
import com.jacto.unext.service.dto.ParceiroDTO;
import com.jacto.unext.service.mapper.ParceiroMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Parceiro}.
 */
@Service
@Transactional
public class ParceiroServiceImpl implements ParceiroService {

    private final Logger log = LoggerFactory.getLogger(ParceiroServiceImpl.class);

    private final ParceiroRepository parceiroRepository;

    private final ParceiroMapper parceiroMapper;

    public ParceiroServiceImpl(ParceiroRepository parceiroRepository, ParceiroMapper parceiroMapper) {
        this.parceiroRepository = parceiroRepository;
        this.parceiroMapper = parceiroMapper;
    }

    @Override
    public ParceiroDTO save(ParceiroDTO parceiroDTO) {
        log.debug("Request to save Parceiro : {}", parceiroDTO);
        Parceiro parceiro = parceiroMapper.toEntity(parceiroDTO);
        parceiro = parceiroRepository.save(parceiro);
        return parceiroMapper.toDto(parceiro);
    }

    @Override
    public Optional<ParceiroDTO> partialUpdate(ParceiroDTO parceiroDTO) {
        log.debug("Request to partially update Parceiro : {}", parceiroDTO);

        return parceiroRepository
            .findById(parceiroDTO.getId())
            .map(existingParceiro -> {
                parceiroMapper.partialUpdate(existingParceiro, parceiroDTO);

                return existingParceiro;
            })
            .map(parceiroRepository::save)
            .map(parceiroMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParceiroDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Parceiros");
        return parceiroRepository.findAll(pageable).map(parceiroMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ParceiroDTO> findOne(Long id) {
        log.debug("Request to get Parceiro : {}", id);
        return parceiroRepository.findById(id).map(parceiroMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Parceiro : {}", id);
        parceiroRepository.deleteById(id);
    }
}
