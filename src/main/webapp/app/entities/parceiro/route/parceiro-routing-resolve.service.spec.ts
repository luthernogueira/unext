jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IParceiro, Parceiro } from '../parceiro.model';
import { ParceiroService } from '../service/parceiro.service';

import { ParceiroRoutingResolveService } from './parceiro-routing-resolve.service';

describe('Parceiro routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ParceiroRoutingResolveService;
  let service: ParceiroService;
  let resultParceiro: IParceiro | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(ParceiroRoutingResolveService);
    service = TestBed.inject(ParceiroService);
    resultParceiro = undefined;
  });

  describe('resolve', () => {
    it('should return IParceiro returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultParceiro = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultParceiro).toEqual({ id: 123 });
    });

    it('should return new IParceiro if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultParceiro = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultParceiro).toEqual(new Parceiro());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Parceiro })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultParceiro = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultParceiro).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
