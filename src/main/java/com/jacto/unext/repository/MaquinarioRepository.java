package com.jacto.unext.repository;

import com.jacto.unext.domain.Maquinario;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Maquinario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaquinarioRepository extends JpaRepository<Maquinario, Long> {}
