import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ITipoMaquinario, TipoMaquinario } from '../tipo-maquinario.model';
import { TipoMaquinarioService } from '../service/tipo-maquinario.service';

@Component({
  selector: 'jhi-tipo-maquinario-update',
  templateUrl: './tipo-maquinario-update.component.html',
})
export class TipoMaquinarioUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    descricao: [null, [Validators.required]],
  });

  constructor(
    protected tipoMaquinarioService: TipoMaquinarioService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tipoMaquinario }) => {
      this.updateForm(tipoMaquinario);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tipoMaquinario = this.createFromForm();
    if (tipoMaquinario.id !== undefined) {
      this.subscribeToSaveResponse(this.tipoMaquinarioService.update(tipoMaquinario));
    } else {
      this.subscribeToSaveResponse(this.tipoMaquinarioService.create(tipoMaquinario));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoMaquinario>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(tipoMaquinario: ITipoMaquinario): void {
    this.editForm.patchValue({
      id: tipoMaquinario.id,
      descricao: tipoMaquinario.descricao,
    });
  }

  protected createFromForm(): ITipoMaquinario {
    return {
      ...new TipoMaquinario(),
      id: this.editForm.get(['id'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
    };
  }
}
