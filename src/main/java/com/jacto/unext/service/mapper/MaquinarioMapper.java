package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.Maquinario;
import com.jacto.unext.service.dto.MaquinarioDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Maquinario} and its DTO {@link MaquinarioDTO}.
 */
@Mapper(componentModel = "spring", uses = { HistoricoLocacaoMapper.class, TipoMaquinarioMapper.class })
public interface MaquinarioMapper extends EntityMapper<MaquinarioDTO, Maquinario> {
    @Mapping(target = "historicoLocacao", source = "historicoLocacao", qualifiedByName = "id")
    @Mapping(target = "tipoMaquinario", source = "tipoMaquinario", qualifiedByName = "descricao")
    MaquinarioDTO toDto(Maquinario s);
}
