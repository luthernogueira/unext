package com.jacto.unext.web.rest;

import com.jacto.unext.repository.MaquinarioRepository;
import com.jacto.unext.service.MaquinarioService;
import com.jacto.unext.service.dto.MaquinarioDTO;
import com.jacto.unext.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jacto.unext.domain.Maquinario}.
 */
@RestController
@RequestMapping("/api")
public class MaquinarioResource {

    private final Logger log = LoggerFactory.getLogger(MaquinarioResource.class);

    private static final String ENTITY_NAME = "maquinario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaquinarioService maquinarioService;

    private final MaquinarioRepository maquinarioRepository;

    public MaquinarioResource(MaquinarioService maquinarioService, MaquinarioRepository maquinarioRepository) {
        this.maquinarioService = maquinarioService;
        this.maquinarioRepository = maquinarioRepository;
    }

    /**
     * {@code POST  /maquinarios} : Create a new maquinario.
     *
     * @param maquinarioDTO the maquinarioDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new maquinarioDTO, or with status {@code 400 (Bad Request)} if the maquinario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/maquinarios")
    public ResponseEntity<MaquinarioDTO> createMaquinario(@Valid @RequestBody MaquinarioDTO maquinarioDTO) throws URISyntaxException {
        log.debug("REST request to save Maquinario : {}", maquinarioDTO);
        if (maquinarioDTO.getId() != null) {
            throw new BadRequestAlertException("A new maquinario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaquinarioDTO result = maquinarioService.save(maquinarioDTO);
        return ResponseEntity
            .created(new URI("/api/maquinarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /maquinarios/:id} : Updates an existing maquinario.
     *
     * @param id the id of the maquinarioDTO to save.
     * @param maquinarioDTO the maquinarioDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated maquinarioDTO,
     * or with status {@code 400 (Bad Request)} if the maquinarioDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the maquinarioDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/maquinarios/{id}")
    public ResponseEntity<MaquinarioDTO> updateMaquinario(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MaquinarioDTO maquinarioDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Maquinario : {}, {}", id, maquinarioDTO);
        if (maquinarioDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, maquinarioDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!maquinarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MaquinarioDTO result = maquinarioService.save(maquinarioDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, maquinarioDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /maquinarios/:id} : Partial updates given fields of an existing maquinario, field will ignore if it is null
     *
     * @param id the id of the maquinarioDTO to save.
     * @param maquinarioDTO the maquinarioDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated maquinarioDTO,
     * or with status {@code 400 (Bad Request)} if the maquinarioDTO is not valid,
     * or with status {@code 404 (Not Found)} if the maquinarioDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the maquinarioDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/maquinarios/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MaquinarioDTO> partialUpdateMaquinario(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MaquinarioDTO maquinarioDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Maquinario partially : {}, {}", id, maquinarioDTO);
        if (maquinarioDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, maquinarioDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!maquinarioRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MaquinarioDTO> result = maquinarioService.partialUpdate(maquinarioDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, maquinarioDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /maquinarios} : get all the maquinarios.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of maquinarios in body.
     */
    @GetMapping("/maquinarios")
    public ResponseEntity<List<MaquinarioDTO>> getAllMaquinarios(Pageable pageable) {
        log.debug("REST request to get a page of Maquinarios");
        Page<MaquinarioDTO> page = maquinarioService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /maquinarios/:id} : get the "id" maquinario.
     *
     * @param id the id of the maquinarioDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the maquinarioDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/maquinarios/{id}")
    public ResponseEntity<MaquinarioDTO> getMaquinario(@PathVariable Long id) {
        log.debug("REST request to get Maquinario : {}", id);
        Optional<MaquinarioDTO> maquinarioDTO = maquinarioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(maquinarioDTO);
    }

    /**
     * {@code DELETE  /maquinarios/:id} : delete the "id" maquinario.
     *
     * @param id the id of the maquinarioDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/maquinarios/{id}")
    public ResponseEntity<Void> deleteMaquinario(@PathVariable Long id) {
        log.debug("REST request to delete Maquinario : {}", id);
        maquinarioService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
