import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TipoMaquinarioComponent } from '../list/tipo-maquinario.component';
import { TipoMaquinarioDetailComponent } from '../detail/tipo-maquinario-detail.component';
import { TipoMaquinarioUpdateComponent } from '../update/tipo-maquinario-update.component';
import { TipoMaquinarioRoutingResolveService } from './tipo-maquinario-routing-resolve.service';

const tipoMaquinarioRoute: Routes = [
  {
    path: '',
    component: TipoMaquinarioComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TipoMaquinarioDetailComponent,
    resolve: {
      tipoMaquinario: TipoMaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TipoMaquinarioUpdateComponent,
    resolve: {
      tipoMaquinario: TipoMaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TipoMaquinarioUpdateComponent,
    resolve: {
      tipoMaquinario: TipoMaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(tipoMaquinarioRoute)],
  exports: [RouterModule],
})
export class TipoMaquinarioRoutingModule {}
