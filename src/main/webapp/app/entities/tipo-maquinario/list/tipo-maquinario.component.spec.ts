import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { TipoMaquinarioService } from '../service/tipo-maquinario.service';

import { TipoMaquinarioComponent } from './tipo-maquinario.component';

describe('TipoMaquinario Management Component', () => {
  let comp: TipoMaquinarioComponent;
  let fixture: ComponentFixture<TipoMaquinarioComponent>;
  let service: TipoMaquinarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TipoMaquinarioComponent],
    })
      .overrideTemplate(TipoMaquinarioComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TipoMaquinarioComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(TipoMaquinarioService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.tipoMaquinarios?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
