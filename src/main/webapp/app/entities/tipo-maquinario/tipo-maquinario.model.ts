import { IMaquinario } from 'app/entities/maquinario/maquinario.model';

export interface ITipoMaquinario {
  id?: number;
  descricao?: string;
  maquinarios?: IMaquinario[] | null;
}

export class TipoMaquinario implements ITipoMaquinario {
  constructor(public id?: number, public descricao?: string, public maquinarios?: IMaquinario[] | null) {}
}

export function getTipoMaquinarioIdentifier(tipoMaquinario: ITipoMaquinario): number | undefined {
  return tipoMaquinario.id;
}
