import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITipoMaquinario, getTipoMaquinarioIdentifier } from '../tipo-maquinario.model';

export type EntityResponseType = HttpResponse<ITipoMaquinario>;
export type EntityArrayResponseType = HttpResponse<ITipoMaquinario[]>;

@Injectable({ providedIn: 'root' })
export class TipoMaquinarioService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/tipo-maquinarios');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(tipoMaquinario: ITipoMaquinario): Observable<EntityResponseType> {
    return this.http.post<ITipoMaquinario>(this.resourceUrl, tipoMaquinario, { observe: 'response' });
  }

  update(tipoMaquinario: ITipoMaquinario): Observable<EntityResponseType> {
    return this.http.put<ITipoMaquinario>(`${this.resourceUrl}/${getTipoMaquinarioIdentifier(tipoMaquinario) as number}`, tipoMaquinario, {
      observe: 'response',
    });
  }

  partialUpdate(tipoMaquinario: ITipoMaquinario): Observable<EntityResponseType> {
    return this.http.patch<ITipoMaquinario>(
      `${this.resourceUrl}/${getTipoMaquinarioIdentifier(tipoMaquinario) as number}`,
      tipoMaquinario,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITipoMaquinario>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITipoMaquinario[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTipoMaquinarioToCollectionIfMissing(
    tipoMaquinarioCollection: ITipoMaquinario[],
    ...tipoMaquinariosToCheck: (ITipoMaquinario | null | undefined)[]
  ): ITipoMaquinario[] {
    const tipoMaquinarios: ITipoMaquinario[] = tipoMaquinariosToCheck.filter(isPresent);
    if (tipoMaquinarios.length > 0) {
      const tipoMaquinarioCollectionIdentifiers = tipoMaquinarioCollection.map(
        tipoMaquinarioItem => getTipoMaquinarioIdentifier(tipoMaquinarioItem)!
      );
      const tipoMaquinariosToAdd = tipoMaquinarios.filter(tipoMaquinarioItem => {
        const tipoMaquinarioIdentifier = getTipoMaquinarioIdentifier(tipoMaquinarioItem);
        if (tipoMaquinarioIdentifier == null || tipoMaquinarioCollectionIdentifiers.includes(tipoMaquinarioIdentifier)) {
          return false;
        }
        tipoMaquinarioCollectionIdentifiers.push(tipoMaquinarioIdentifier);
        return true;
      });
      return [...tipoMaquinariosToAdd, ...tipoMaquinarioCollection];
    }
    return tipoMaquinarioCollection;
  }
}
