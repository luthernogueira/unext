import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMaquinario, getMaquinarioIdentifier } from '../maquinario.model';

export type EntityResponseType = HttpResponse<IMaquinario>;
export type EntityArrayResponseType = HttpResponse<IMaquinario[]>;

@Injectable({ providedIn: 'root' })
export class MaquinarioService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/maquinarios');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(maquinario: IMaquinario): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(maquinario);
    return this.http
      .post<IMaquinario>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(maquinario: IMaquinario): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(maquinario);
    return this.http
      .put<IMaquinario>(`${this.resourceUrl}/${getMaquinarioIdentifier(maquinario) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(maquinario: IMaquinario): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(maquinario);
    return this.http
      .patch<IMaquinario>(`${this.resourceUrl}/${getMaquinarioIdentifier(maquinario) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMaquinario>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMaquinario[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMaquinarioToCollectionIfMissing(
    maquinarioCollection: IMaquinario[],
    ...maquinariosToCheck: (IMaquinario | null | undefined)[]
  ): IMaquinario[] {
    const maquinarios: IMaquinario[] = maquinariosToCheck.filter(isPresent);
    if (maquinarios.length > 0) {
      const maquinarioCollectionIdentifiers = maquinarioCollection.map(maquinarioItem => getMaquinarioIdentifier(maquinarioItem)!);
      const maquinariosToAdd = maquinarios.filter(maquinarioItem => {
        const maquinarioIdentifier = getMaquinarioIdentifier(maquinarioItem);
        if (maquinarioIdentifier == null || maquinarioCollectionIdentifiers.includes(maquinarioIdentifier)) {
          return false;
        }
        maquinarioCollectionIdentifiers.push(maquinarioIdentifier);
        return true;
      });
      return [...maquinariosToAdd, ...maquinarioCollection];
    }
    return maquinarioCollection;
  }

  protected convertDateFromClient(maquinario: IMaquinario): IMaquinario {
    return Object.assign({}, maquinario, {
      anoFabricacao: maquinario.anoFabricacao?.isValid() ? maquinario.anoFabricacao.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.anoFabricacao = res.body.anoFabricacao ? dayjs(res.body.anoFabricacao) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((maquinario: IMaquinario) => {
        maquinario.anoFabricacao = maquinario.anoFabricacao ? dayjs(maquinario.anoFabricacao) : undefined;
      });
    }
    return res;
  }
}
