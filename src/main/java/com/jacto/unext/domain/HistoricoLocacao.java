package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A HistoricoLocacao.
 */
@Entity
@Table(name = "historico_locacao")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class HistoricoLocacao implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "data", nullable = false)
    private LocalDate data;

    @NotNull
    @Column(name = "tempo_locacao", nullable = false)
    private Float tempoLocacao;

    @NotNull
    @Column(name = "valor_total", nullable = false)
    private Float valorTotal;

    @NotNull
    @Column(name = "fornecedor", nullable = false)
    private String fornecedor;

    @OneToMany(mappedBy = "historicoLocacao")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "historicoLocacao", "tipoMaquinario" }, allowSetters = true)
    private Set<Maquinario> maquinarios = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "historicoLocacaos", "carteiras" }, allowSetters = true)
    private Cliente cliente;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "historicoLocacaos", "carteiras" }, allowSetters = true)
    private Parceiro parceiro;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public HistoricoLocacao id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData() {
        return this.data;
    }

    public HistoricoLocacao data(LocalDate data) {
        this.setData(data);
        return this;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public Float getTempoLocacao() {
        return this.tempoLocacao;
    }

    public HistoricoLocacao tempoLocacao(Float tempoLocacao) {
        this.setTempoLocacao(tempoLocacao);
        return this;
    }

    public void setTempoLocacao(Float tempoLocacao) {
        this.tempoLocacao = tempoLocacao;
    }

    public Float getValorTotal() {
        return this.valorTotal;
    }

    public HistoricoLocacao valorTotal(Float valorTotal) {
        this.setValorTotal(valorTotal);
        return this;
    }

    public void setValorTotal(Float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getFornecedor() {
        return this.fornecedor;
    }

    public HistoricoLocacao fornecedor(String fornecedor) {
        this.setFornecedor(fornecedor);
        return this;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Set<Maquinario> getMaquinarios() {
        return this.maquinarios;
    }

    public void setMaquinarios(Set<Maquinario> maquinarios) {
        if (this.maquinarios != null) {
            this.maquinarios.forEach(i -> i.setHistoricoLocacao(null));
        }
        if (maquinarios != null) {
            maquinarios.forEach(i -> i.setHistoricoLocacao(this));
        }
        this.maquinarios = maquinarios;
    }

    public HistoricoLocacao maquinarios(Set<Maquinario> maquinarios) {
        this.setMaquinarios(maquinarios);
        return this;
    }

    public HistoricoLocacao addMaquinario(Maquinario maquinario) {
        this.maquinarios.add(maquinario);
        maquinario.setHistoricoLocacao(this);
        return this;
    }

    public HistoricoLocacao removeMaquinario(Maquinario maquinario) {
        this.maquinarios.remove(maquinario);
        maquinario.setHistoricoLocacao(null);
        return this;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public HistoricoLocacao cliente(Cliente cliente) {
        this.setCliente(cliente);
        return this;
    }

    public Parceiro getParceiro() {
        return this.parceiro;
    }

    public void setParceiro(Parceiro parceiro) {
        this.parceiro = parceiro;
    }

    public HistoricoLocacao parceiro(Parceiro parceiro) {
        this.setParceiro(parceiro);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HistoricoLocacao)) {
            return false;
        }
        return id != null && id.equals(((HistoricoLocacao) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HistoricoLocacao{" +
            "id=" + getId() +
            ", data='" + getData() + "'" +
            ", tempoLocacao=" + getTempoLocacao() +
            ", valorTotal=" + getValorTotal() +
            ", fornecedor='" + getFornecedor() + "'" +
            "}";
    }
}
