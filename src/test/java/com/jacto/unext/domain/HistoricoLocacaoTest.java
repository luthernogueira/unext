package com.jacto.unext.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HistoricoLocacaoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(HistoricoLocacao.class);
        HistoricoLocacao historicoLocacao1 = new HistoricoLocacao();
        historicoLocacao1.setId(1L);
        HistoricoLocacao historicoLocacao2 = new HistoricoLocacao();
        historicoLocacao2.setId(historicoLocacao1.getId());
        assertThat(historicoLocacao1).isEqualTo(historicoLocacao2);
        historicoLocacao2.setId(2L);
        assertThat(historicoLocacao1).isNotEqualTo(historicoLocacao2);
        historicoLocacao1.setId(null);
        assertThat(historicoLocacao1).isNotEqualTo(historicoLocacao2);
    }
}
