import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMaquinario, Maquinario } from '../maquinario.model';

import { MaquinarioService } from './maquinario.service';

describe('Maquinario Service', () => {
  let service: MaquinarioService;
  let httpMock: HttpTestingController;
  let elemDefault: IMaquinario;
  let expectedResult: IMaquinario | IMaquinario[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MaquinarioService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nome: 'AAAAAAA',
      modelo: 'AAAAAAA',
      anoFabricacao: currentDate,
      capacidadeLitro: 0,
      volumeMinuto: 0,
      descricao: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          anoFabricacao: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Maquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          anoFabricacao: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          anoFabricacao: currentDate,
        },
        returnedFromService
      );

      service.create(new Maquinario()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Maquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          modelo: 'BBBBBB',
          anoFabricacao: currentDate.format(DATE_FORMAT),
          capacidadeLitro: 1,
          volumeMinuto: 1,
          descricao: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          anoFabricacao: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Maquinario', () => {
      const patchObject = Object.assign(
        {
          capacidadeLitro: 1,
          volumeMinuto: 1,
          descricao: 1,
        },
        new Maquinario()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          anoFabricacao: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Maquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nome: 'BBBBBB',
          modelo: 'BBBBBB',
          anoFabricacao: currentDate.format(DATE_FORMAT),
          capacidadeLitro: 1,
          volumeMinuto: 1,
          descricao: 1,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          anoFabricacao: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Maquinario', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMaquinarioToCollectionIfMissing', () => {
      it('should add a Maquinario to an empty array', () => {
        const maquinario: IMaquinario = { id: 123 };
        expectedResult = service.addMaquinarioToCollectionIfMissing([], maquinario);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(maquinario);
      });

      it('should not add a Maquinario to an array that contains it', () => {
        const maquinario: IMaquinario = { id: 123 };
        const maquinarioCollection: IMaquinario[] = [
          {
            ...maquinario,
          },
          { id: 456 },
        ];
        expectedResult = service.addMaquinarioToCollectionIfMissing(maquinarioCollection, maquinario);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Maquinario to an array that doesn't contain it", () => {
        const maquinario: IMaquinario = { id: 123 };
        const maquinarioCollection: IMaquinario[] = [{ id: 456 }];
        expectedResult = service.addMaquinarioToCollectionIfMissing(maquinarioCollection, maquinario);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(maquinario);
      });

      it('should add only unique Maquinario to an array', () => {
        const maquinarioArray: IMaquinario[] = [{ id: 123 }, { id: 456 }, { id: 7230 }];
        const maquinarioCollection: IMaquinario[] = [{ id: 123 }];
        expectedResult = service.addMaquinarioToCollectionIfMissing(maquinarioCollection, ...maquinarioArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const maquinario: IMaquinario = { id: 123 };
        const maquinario2: IMaquinario = { id: 456 };
        expectedResult = service.addMaquinarioToCollectionIfMissing([], maquinario, maquinario2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(maquinario);
        expect(expectedResult).toContain(maquinario2);
      });

      it('should accept null and undefined values', () => {
        const maquinario: IMaquinario = { id: 123 };
        expectedResult = service.addMaquinarioToCollectionIfMissing([], null, maquinario, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(maquinario);
      });

      it('should return initial array if no Maquinario is added', () => {
        const maquinarioCollection: IMaquinario[] = [{ id: 123 }];
        expectedResult = service.addMaquinarioToCollectionIfMissing(maquinarioCollection, undefined, null);
        expect(expectedResult).toEqual(maquinarioCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
