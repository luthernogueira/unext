import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITipoMaquinario } from '../tipo-maquinario.model';
import { TipoMaquinarioService } from '../service/tipo-maquinario.service';

@Component({
  templateUrl: './tipo-maquinario-delete-dialog.component.html',
})
export class TipoMaquinarioDeleteDialogComponent {
  tipoMaquinario?: ITipoMaquinario;

  constructor(protected tipoMaquinarioService: TipoMaquinarioService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tipoMaquinarioService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
