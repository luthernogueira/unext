package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.Parceiro;
import com.jacto.unext.service.dto.ParceiroDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Parceiro} and its DTO {@link ParceiroDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface ParceiroMapper extends EntityMapper<ParceiroDTO, Parceiro> {
    @Mapping(target = "user", source = "user", qualifiedByName = "id")
    ParceiroDTO toDto(Parceiro s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ParceiroDTO toDtoId(Parceiro parceiro);
}
