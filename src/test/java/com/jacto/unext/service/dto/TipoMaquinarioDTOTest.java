package com.jacto.unext.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TipoMaquinarioDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoMaquinarioDTO.class);
        TipoMaquinarioDTO tipoMaquinarioDTO1 = new TipoMaquinarioDTO();
        tipoMaquinarioDTO1.setId(1L);
        TipoMaquinarioDTO tipoMaquinarioDTO2 = new TipoMaquinarioDTO();
        assertThat(tipoMaquinarioDTO1).isNotEqualTo(tipoMaquinarioDTO2);
        tipoMaquinarioDTO2.setId(tipoMaquinarioDTO1.getId());
        assertThat(tipoMaquinarioDTO1).isEqualTo(tipoMaquinarioDTO2);
        tipoMaquinarioDTO2.setId(2L);
        assertThat(tipoMaquinarioDTO1).isNotEqualTo(tipoMaquinarioDTO2);
        tipoMaquinarioDTO1.setId(null);
        assertThat(tipoMaquinarioDTO1).isNotEqualTo(tipoMaquinarioDTO2);
    }
}
