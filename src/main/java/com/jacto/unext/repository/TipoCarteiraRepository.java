package com.jacto.unext.repository;

import com.jacto.unext.domain.TipoCarteira;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TipoCarteira entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoCarteiraRepository extends JpaRepository<TipoCarteira, Long> {}
