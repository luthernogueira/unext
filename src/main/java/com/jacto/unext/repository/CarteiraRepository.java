package com.jacto.unext.repository;

import com.jacto.unext.domain.Carteira;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Carteira entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarteiraRepository extends JpaRepository<Carteira, Long> {}
