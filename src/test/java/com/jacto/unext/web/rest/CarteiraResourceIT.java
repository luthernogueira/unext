package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.Carteira;
import com.jacto.unext.repository.CarteiraRepository;
import com.jacto.unext.service.dto.CarteiraDTO;
import com.jacto.unext.service.mapper.CarteiraMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CarteiraResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CarteiraResourceIT {

    private static final String DEFAULT_TITULAR = "AAAAAAAAAA";
    private static final String UPDATED_TITULAR = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO_CONTA = 1;
    private static final Integer UPDATED_NUMERO_CONTA = 2;

    private static final String ENTITY_API_URL = "/api/carteiras";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CarteiraRepository carteiraRepository;

    @Autowired
    private CarteiraMapper carteiraMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCarteiraMockMvc;

    private Carteira carteira;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Carteira createEntity(EntityManager em) {
        Carteira carteira = new Carteira().titular(DEFAULT_TITULAR).numeroConta(DEFAULT_NUMERO_CONTA);
        return carteira;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Carteira createUpdatedEntity(EntityManager em) {
        Carteira carteira = new Carteira().titular(UPDATED_TITULAR).numeroConta(UPDATED_NUMERO_CONTA);
        return carteira;
    }

    @BeforeEach
    public void initTest() {
        carteira = createEntity(em);
    }

    @Test
    @Transactional
    void createCarteira() throws Exception {
        int databaseSizeBeforeCreate = carteiraRepository.findAll().size();
        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);
        restCarteiraMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carteiraDTO)))
            .andExpect(status().isCreated());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeCreate + 1);
        Carteira testCarteira = carteiraList.get(carteiraList.size() - 1);
        assertThat(testCarteira.getTitular()).isEqualTo(DEFAULT_TITULAR);
        assertThat(testCarteira.getNumeroConta()).isEqualTo(DEFAULT_NUMERO_CONTA);
    }

    @Test
    @Transactional
    void createCarteiraWithExistingId() throws Exception {
        // Create the Carteira with an existing ID
        carteira.setId(1L);
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        int databaseSizeBeforeCreate = carteiraRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarteiraMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carteiraDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitularIsRequired() throws Exception {
        int databaseSizeBeforeTest = carteiraRepository.findAll().size();
        // set the field null
        carteira.setTitular(null);

        // Create the Carteira, which fails.
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        restCarteiraMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carteiraDTO)))
            .andExpect(status().isBadRequest());

        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNumeroContaIsRequired() throws Exception {
        int databaseSizeBeforeTest = carteiraRepository.findAll().size();
        // set the field null
        carteira.setNumeroConta(null);

        // Create the Carteira, which fails.
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        restCarteiraMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carteiraDTO)))
            .andExpect(status().isBadRequest());

        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCarteiras() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        // Get all the carteiraList
        restCarteiraMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carteira.getId().intValue())))
            .andExpect(jsonPath("$.[*].titular").value(hasItem(DEFAULT_TITULAR)))
            .andExpect(jsonPath("$.[*].numeroConta").value(hasItem(DEFAULT_NUMERO_CONTA)));
    }

    @Test
    @Transactional
    void getCarteira() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        // Get the carteira
        restCarteiraMockMvc
            .perform(get(ENTITY_API_URL_ID, carteira.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(carteira.getId().intValue()))
            .andExpect(jsonPath("$.titular").value(DEFAULT_TITULAR))
            .andExpect(jsonPath("$.numeroConta").value(DEFAULT_NUMERO_CONTA));
    }

    @Test
    @Transactional
    void getNonExistingCarteira() throws Exception {
        // Get the carteira
        restCarteiraMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCarteira() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();

        // Update the carteira
        Carteira updatedCarteira = carteiraRepository.findById(carteira.getId()).get();
        // Disconnect from session so that the updates on updatedCarteira are not directly saved in db
        em.detach(updatedCarteira);
        updatedCarteira.titular(UPDATED_TITULAR).numeroConta(UPDATED_NUMERO_CONTA);
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(updatedCarteira);

        restCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, carteiraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isOk());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
        Carteira testCarteira = carteiraList.get(carteiraList.size() - 1);
        assertThat(testCarteira.getTitular()).isEqualTo(UPDATED_TITULAR);
        assertThat(testCarteira.getNumeroConta()).isEqualTo(UPDATED_NUMERO_CONTA);
    }

    @Test
    @Transactional
    void putNonExistingCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, carteiraDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carteiraDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCarteiraWithPatch() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();

        // Update the carteira using partial update
        Carteira partialUpdatedCarteira = new Carteira();
        partialUpdatedCarteira.setId(carteira.getId());

        restCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCarteira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCarteira))
            )
            .andExpect(status().isOk());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
        Carteira testCarteira = carteiraList.get(carteiraList.size() - 1);
        assertThat(testCarteira.getTitular()).isEqualTo(DEFAULT_TITULAR);
        assertThat(testCarteira.getNumeroConta()).isEqualTo(DEFAULT_NUMERO_CONTA);
    }

    @Test
    @Transactional
    void fullUpdateCarteiraWithPatch() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();

        // Update the carteira using partial update
        Carteira partialUpdatedCarteira = new Carteira();
        partialUpdatedCarteira.setId(carteira.getId());

        partialUpdatedCarteira.titular(UPDATED_TITULAR).numeroConta(UPDATED_NUMERO_CONTA);

        restCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCarteira.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCarteira))
            )
            .andExpect(status().isOk());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
        Carteira testCarteira = carteiraList.get(carteiraList.size() - 1);
        assertThat(testCarteira.getTitular()).isEqualTo(UPDATED_TITULAR);
        assertThat(testCarteira.getNumeroConta()).isEqualTo(UPDATED_NUMERO_CONTA);
    }

    @Test
    @Transactional
    void patchNonExistingCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, carteiraDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCarteira() throws Exception {
        int databaseSizeBeforeUpdate = carteiraRepository.findAll().size();
        carteira.setId(count.incrementAndGet());

        // Create the Carteira
        CarteiraDTO carteiraDTO = carteiraMapper.toDto(carteira);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCarteiraMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(carteiraDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Carteira in the database
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCarteira() throws Exception {
        // Initialize the database
        carteiraRepository.saveAndFlush(carteira);

        int databaseSizeBeforeDelete = carteiraRepository.findAll().size();

        // Delete the carteira
        restCarteiraMockMvc
            .perform(delete(ENTITY_API_URL_ID, carteira.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Carteira> carteiraList = carteiraRepository.findAll();
        assertThat(carteiraList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
