package com.jacto.unext.service;

import com.jacto.unext.service.dto.HistoricoLocacaoDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.HistoricoLocacao}.
 */
public interface HistoricoLocacaoService {
    /**
     * Save a historicoLocacao.
     *
     * @param historicoLocacaoDTO the entity to save.
     * @return the persisted entity.
     */
    HistoricoLocacaoDTO save(HistoricoLocacaoDTO historicoLocacaoDTO);

    /**
     * Partially updates a historicoLocacao.
     *
     * @param historicoLocacaoDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<HistoricoLocacaoDTO> partialUpdate(HistoricoLocacaoDTO historicoLocacaoDTO);

    /**
     * Get all the historicoLocacaos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HistoricoLocacaoDTO> findAll(Pageable pageable);

    /**
     * Get the "id" historicoLocacao.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HistoricoLocacaoDTO> findOne(Long id);

    /**
     * Delete the "id" historicoLocacao.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
