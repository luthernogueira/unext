import { ITipoCarteira } from 'app/entities/tipo-carteira/tipo-carteira.model';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';

export interface ICarteira {
  id?: number;
  titular?: string;
  numeroConta?: number;
  tipoCarteira?: ITipoCarteira | null;
  cliente?: ICliente | null;
  parceiro?: IParceiro | null;
}

export class Carteira implements ICarteira {
  constructor(
    public id?: number,
    public titular?: string,
    public numeroConta?: number,
    public tipoCarteira?: ITipoCarteira | null,
    public cliente?: ICliente | null,
    public parceiro?: IParceiro | null
  ) {}
}

export function getCarteiraIdentifier(carteira: ICarteira): number | undefined {
  return carteira.id;
}
