import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMaquinario } from '../maquinario.model';
import { MaquinarioService } from '../service/maquinario.service';

@Component({
  templateUrl: './maquinario-delete-dialog.component.html',
})
export class MaquinarioDeleteDialogComponent {
  maquinario?: IMaquinario;

  constructor(protected maquinarioService: MaquinarioService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.maquinarioService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
