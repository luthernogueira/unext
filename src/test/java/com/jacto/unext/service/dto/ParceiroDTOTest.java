package com.jacto.unext.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ParceiroDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParceiroDTO.class);
        ParceiroDTO parceiroDTO1 = new ParceiroDTO();
        parceiroDTO1.setId(1L);
        ParceiroDTO parceiroDTO2 = new ParceiroDTO();
        assertThat(parceiroDTO1).isNotEqualTo(parceiroDTO2);
        parceiroDTO2.setId(parceiroDTO1.getId());
        assertThat(parceiroDTO1).isEqualTo(parceiroDTO2);
        parceiroDTO2.setId(2L);
        assertThat(parceiroDTO1).isNotEqualTo(parceiroDTO2);
        parceiroDTO1.setId(null);
        assertThat(parceiroDTO1).isNotEqualTo(parceiroDTO2);
    }
}
