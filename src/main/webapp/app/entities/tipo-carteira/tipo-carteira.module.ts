import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TipoCarteiraComponent } from './list/tipo-carteira.component';
import { TipoCarteiraDetailComponent } from './detail/tipo-carteira-detail.component';
import { TipoCarteiraUpdateComponent } from './update/tipo-carteira-update.component';
import { TipoCarteiraDeleteDialogComponent } from './delete/tipo-carteira-delete-dialog.component';
import { TipoCarteiraRoutingModule } from './route/tipo-carteira-routing.module';

@NgModule({
  imports: [SharedModule, TipoCarteiraRoutingModule],
  declarations: [TipoCarteiraComponent, TipoCarteiraDetailComponent, TipoCarteiraUpdateComponent, TipoCarteiraDeleteDialogComponent],
  entryComponents: [TipoCarteiraDeleteDialogComponent],
})
export class TipoCarteiraModule {}
