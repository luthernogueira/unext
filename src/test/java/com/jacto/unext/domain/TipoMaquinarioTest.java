package com.jacto.unext.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TipoMaquinarioTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoMaquinario.class);
        TipoMaquinario tipoMaquinario1 = new TipoMaquinario();
        tipoMaquinario1.setId(1L);
        TipoMaquinario tipoMaquinario2 = new TipoMaquinario();
        tipoMaquinario2.setId(tipoMaquinario1.getId());
        assertThat(tipoMaquinario1).isEqualTo(tipoMaquinario2);
        tipoMaquinario2.setId(2L);
        assertThat(tipoMaquinario1).isNotEqualTo(tipoMaquinario2);
        tipoMaquinario1.setId(null);
        assertThat(tipoMaquinario1).isNotEqualTo(tipoMaquinario2);
    }
}
