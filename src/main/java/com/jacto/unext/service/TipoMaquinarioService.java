package com.jacto.unext.service;

import com.jacto.unext.service.dto.TipoMaquinarioDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.TipoMaquinario}.
 */
public interface TipoMaquinarioService {
    /**
     * Save a tipoMaquinario.
     *
     * @param tipoMaquinarioDTO the entity to save.
     * @return the persisted entity.
     */
    TipoMaquinarioDTO save(TipoMaquinarioDTO tipoMaquinarioDTO);

    /**
     * Partially updates a tipoMaquinario.
     *
     * @param tipoMaquinarioDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TipoMaquinarioDTO> partialUpdate(TipoMaquinarioDTO tipoMaquinarioDTO);

    /**
     * Get all the tipoMaquinarios.
     *
     * @return the list of entities.
     */
    List<TipoMaquinarioDTO> findAll();

    /**
     * Get the "id" tipoMaquinario.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TipoMaquinarioDTO> findOne(Long id);

    /**
     * Delete the "id" tipoMaquinario.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
