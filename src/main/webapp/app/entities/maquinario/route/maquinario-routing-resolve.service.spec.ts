jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMaquinario, Maquinario } from '../maquinario.model';
import { MaquinarioService } from '../service/maquinario.service';

import { MaquinarioRoutingResolveService } from './maquinario-routing-resolve.service';

describe('Maquinario routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MaquinarioRoutingResolveService;
  let service: MaquinarioService;
  let resultMaquinario: IMaquinario | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(MaquinarioRoutingResolveService);
    service = TestBed.inject(MaquinarioService);
    resultMaquinario = undefined;
  });

  describe('resolve', () => {
    it('should return IMaquinario returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMaquinario = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMaquinario).toEqual({ id: 123 });
    });

    it('should return new IMaquinario if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMaquinario = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMaquinario).toEqual(new Maquinario());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Maquinario })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMaquinario = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMaquinario).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
