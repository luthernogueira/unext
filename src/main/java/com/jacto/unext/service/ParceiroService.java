package com.jacto.unext.service;

import com.jacto.unext.service.dto.ParceiroDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.Parceiro}.
 */
public interface ParceiroService {
    /**
     * Save a parceiro.
     *
     * @param parceiroDTO the entity to save.
     * @return the persisted entity.
     */
    ParceiroDTO save(ParceiroDTO parceiroDTO);

    /**
     * Partially updates a parceiro.
     *
     * @param parceiroDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ParceiroDTO> partialUpdate(ParceiroDTO parceiroDTO);

    /**
     * Get all the parceiros.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ParceiroDTO> findAll(Pageable pageable);

    /**
     * Get the "id" parceiro.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ParceiroDTO> findOne(Long id);

    /**
     * Delete the "id" parceiro.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
