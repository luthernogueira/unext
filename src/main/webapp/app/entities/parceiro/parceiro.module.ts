import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ParceiroComponent } from './list/parceiro.component';
import { ParceiroDetailComponent } from './detail/parceiro-detail.component';
import { ParceiroUpdateComponent } from './update/parceiro-update.component';
import { ParceiroDeleteDialogComponent } from './delete/parceiro-delete-dialog.component';
import { ParceiroRoutingModule } from './route/parceiro-routing.module';

@NgModule({
  imports: [SharedModule, ParceiroRoutingModule],
  declarations: [ParceiroComponent, ParceiroDetailComponent, ParceiroUpdateComponent, ParceiroDeleteDialogComponent],
  entryComponents: [ParceiroDeleteDialogComponent],
})
export class ParceiroModule {}
