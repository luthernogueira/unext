import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CarteiraComponent } from '../list/carteira.component';
import { CarteiraDetailComponent } from '../detail/carteira-detail.component';
import { CarteiraUpdateComponent } from '../update/carteira-update.component';
import { CarteiraRoutingResolveService } from './carteira-routing-resolve.service';

const carteiraRoute: Routes = [
  {
    path: '',
    component: CarteiraComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CarteiraDetailComponent,
    resolve: {
      carteira: CarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CarteiraUpdateComponent,
    resolve: {
      carteira: CarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CarteiraUpdateComponent,
    resolve: {
      carteira: CarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(carteiraRoute)],
  exports: [RouterModule],
})
export class CarteiraRoutingModule {}
