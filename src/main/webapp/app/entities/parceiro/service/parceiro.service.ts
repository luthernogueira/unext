import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IParceiro, getParceiroIdentifier } from '../parceiro.model';

export type EntityResponseType = HttpResponse<IParceiro>;
export type EntityArrayResponseType = HttpResponse<IParceiro[]>;

@Injectable({ providedIn: 'root' })
export class ParceiroService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/parceiros');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(parceiro: IParceiro): Observable<EntityResponseType> {
    return this.http.post<IParceiro>(this.resourceUrl, parceiro, { observe: 'response' });
  }

  update(parceiro: IParceiro): Observable<EntityResponseType> {
    return this.http.put<IParceiro>(`${this.resourceUrl}/${getParceiroIdentifier(parceiro) as number}`, parceiro, { observe: 'response' });
  }

  partialUpdate(parceiro: IParceiro): Observable<EntityResponseType> {
    return this.http.patch<IParceiro>(`${this.resourceUrl}/${getParceiroIdentifier(parceiro) as number}`, parceiro, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IParceiro>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IParceiro[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addParceiroToCollectionIfMissing(parceiroCollection: IParceiro[], ...parceirosToCheck: (IParceiro | null | undefined)[]): IParceiro[] {
    const parceiros: IParceiro[] = parceirosToCheck.filter(isPresent);
    if (parceiros.length > 0) {
      const parceiroCollectionIdentifiers = parceiroCollection.map(parceiroItem => getParceiroIdentifier(parceiroItem)!);
      const parceirosToAdd = parceiros.filter(parceiroItem => {
        const parceiroIdentifier = getParceiroIdentifier(parceiroItem);
        if (parceiroIdentifier == null || parceiroCollectionIdentifiers.includes(parceiroIdentifier)) {
          return false;
        }
        parceiroCollectionIdentifiers.push(parceiroIdentifier);
        return true;
      });
      return [...parceirosToAdd, ...parceiroCollection];
    }
    return parceiroCollection;
  }
}
