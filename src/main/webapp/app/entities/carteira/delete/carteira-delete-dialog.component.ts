import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICarteira } from '../carteira.model';
import { CarteiraService } from '../service/carteira.service';

@Component({
  templateUrl: './carteira-delete-dialog.component.html',
})
export class CarteiraDeleteDialogComponent {
  carteira?: ICarteira;

  constructor(protected carteiraService: CarteiraService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.carteiraService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
