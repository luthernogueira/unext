package com.jacto.unext.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jacto.unext.domain.Maquinario} entity.
 */
public class MaquinarioDTO implements Serializable {

    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String modelo;

    @NotNull
    private LocalDate anoFabricacao;

    @NotNull
    private Float capacidadeLitro;

    @NotNull
    private Float volumeMinuto;

    private Long descricao;

    private HistoricoLocacaoDTO historicoLocacao;

    private TipoMaquinarioDTO tipoMaquinario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public LocalDate getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(LocalDate anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Float getCapacidadeLitro() {
        return capacidadeLitro;
    }

    public void setCapacidadeLitro(Float capacidadeLitro) {
        this.capacidadeLitro = capacidadeLitro;
    }

    public Float getVolumeMinuto() {
        return volumeMinuto;
    }

    public void setVolumeMinuto(Float volumeMinuto) {
        this.volumeMinuto = volumeMinuto;
    }

    public Long getDescricao() {
        return descricao;
    }

    public void setDescricao(Long descricao) {
        this.descricao = descricao;
    }

    public HistoricoLocacaoDTO getHistoricoLocacao() {
        return historicoLocacao;
    }

    public void setHistoricoLocacao(HistoricoLocacaoDTO historicoLocacao) {
        this.historicoLocacao = historicoLocacao;
    }

    public TipoMaquinarioDTO getTipoMaquinario() {
        return tipoMaquinario;
    }

    public void setTipoMaquinario(TipoMaquinarioDTO tipoMaquinario) {
        this.tipoMaquinario = tipoMaquinario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaquinarioDTO)) {
            return false;
        }

        MaquinarioDTO maquinarioDTO = (MaquinarioDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, maquinarioDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MaquinarioDTO{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", modelo='" + getModelo() + "'" +
            ", anoFabricacao='" + getAnoFabricacao() + "'" +
            ", capacidadeLitro=" + getCapacidadeLitro() +
            ", volumeMinuto=" + getVolumeMinuto() +
            ", descricao=" + getDescricao() +
            ", historicoLocacao=" + getHistoricoLocacao() +
            ", tipoMaquinario=" + getTipoMaquinario() +
            "}";
    }
}
