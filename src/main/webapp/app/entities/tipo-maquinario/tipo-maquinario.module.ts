import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TipoMaquinarioComponent } from './list/tipo-maquinario.component';
import { TipoMaquinarioDetailComponent } from './detail/tipo-maquinario-detail.component';
import { TipoMaquinarioUpdateComponent } from './update/tipo-maquinario-update.component';
import { TipoMaquinarioDeleteDialogComponent } from './delete/tipo-maquinario-delete-dialog.component';
import { TipoMaquinarioRoutingModule } from './route/tipo-maquinario-routing.module';

@NgModule({
  imports: [SharedModule, TipoMaquinarioRoutingModule],
  declarations: [
    TipoMaquinarioComponent,
    TipoMaquinarioDetailComponent,
    TipoMaquinarioUpdateComponent,
    TipoMaquinarioDeleteDialogComponent,
  ],
  entryComponents: [TipoMaquinarioDeleteDialogComponent],
})
export class TipoMaquinarioModule {}
