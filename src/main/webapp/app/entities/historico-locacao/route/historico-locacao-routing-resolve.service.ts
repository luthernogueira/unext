import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IHistoricoLocacao, HistoricoLocacao } from '../historico-locacao.model';
import { HistoricoLocacaoService } from '../service/historico-locacao.service';

@Injectable({ providedIn: 'root' })
export class HistoricoLocacaoRoutingResolveService implements Resolve<IHistoricoLocacao> {
  constructor(protected service: HistoricoLocacaoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHistoricoLocacao> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((historicoLocacao: HttpResponse<HistoricoLocacao>) => {
          if (historicoLocacao.body) {
            return of(historicoLocacao.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new HistoricoLocacao());
  }
}
