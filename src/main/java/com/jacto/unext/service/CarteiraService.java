package com.jacto.unext.service;

import com.jacto.unext.service.dto.CarteiraDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.Carteira}.
 */
public interface CarteiraService {
    /**
     * Save a carteira.
     *
     * @param carteiraDTO the entity to save.
     * @return the persisted entity.
     */
    CarteiraDTO save(CarteiraDTO carteiraDTO);

    /**
     * Partially updates a carteira.
     *
     * @param carteiraDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CarteiraDTO> partialUpdate(CarteiraDTO carteiraDTO);

    /**
     * Get all the carteiras.
     *
     * @return the list of entities.
     */
    List<CarteiraDTO> findAll();

    /**
     * Get the "id" carteira.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CarteiraDTO> findOne(Long id);

    /**
     * Delete the "id" carteira.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
