package com.jacto.unext.service.impl;

import com.jacto.unext.domain.TipoCarteira;
import com.jacto.unext.repository.TipoCarteiraRepository;
import com.jacto.unext.service.TipoCarteiraService;
import com.jacto.unext.service.dto.TipoCarteiraDTO;
import com.jacto.unext.service.mapper.TipoCarteiraMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TipoCarteira}.
 */
@Service
@Transactional
public class TipoCarteiraServiceImpl implements TipoCarteiraService {

    private final Logger log = LoggerFactory.getLogger(TipoCarteiraServiceImpl.class);

    private final TipoCarteiraRepository tipoCarteiraRepository;

    private final TipoCarteiraMapper tipoCarteiraMapper;

    public TipoCarteiraServiceImpl(TipoCarteiraRepository tipoCarteiraRepository, TipoCarteiraMapper tipoCarteiraMapper) {
        this.tipoCarteiraRepository = tipoCarteiraRepository;
        this.tipoCarteiraMapper = tipoCarteiraMapper;
    }

    @Override
    public TipoCarteiraDTO save(TipoCarteiraDTO tipoCarteiraDTO) {
        log.debug("Request to save TipoCarteira : {}", tipoCarteiraDTO);
        TipoCarteira tipoCarteira = tipoCarteiraMapper.toEntity(tipoCarteiraDTO);
        tipoCarteira = tipoCarteiraRepository.save(tipoCarteira);
        return tipoCarteiraMapper.toDto(tipoCarteira);
    }

    @Override
    public Optional<TipoCarteiraDTO> partialUpdate(TipoCarteiraDTO tipoCarteiraDTO) {
        log.debug("Request to partially update TipoCarteira : {}", tipoCarteiraDTO);

        return tipoCarteiraRepository
            .findById(tipoCarteiraDTO.getId())
            .map(existingTipoCarteira -> {
                tipoCarteiraMapper.partialUpdate(existingTipoCarteira, tipoCarteiraDTO);

                return existingTipoCarteira;
            })
            .map(tipoCarteiraRepository::save)
            .map(tipoCarteiraMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoCarteiraDTO> findAll() {
        log.debug("Request to get all TipoCarteiras");
        return tipoCarteiraRepository.findAll().stream().map(tipoCarteiraMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TipoCarteiraDTO> findOne(Long id) {
        log.debug("Request to get TipoCarteira : {}", id);
        return tipoCarteiraRepository.findById(id).map(tipoCarteiraMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoCarteira : {}", id);
        tipoCarteiraRepository.deleteById(id);
    }
}
