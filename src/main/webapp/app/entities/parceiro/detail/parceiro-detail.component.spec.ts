import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ParceiroDetailComponent } from './parceiro-detail.component';

describe('Parceiro Management Detail Component', () => {
  let comp: ParceiroDetailComponent;
  let fixture: ComponentFixture<ParceiroDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParceiroDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ parceiro: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ParceiroDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ParceiroDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load parceiro on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.parceiro).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
