import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMaquinario, Maquinario } from '../maquinario.model';
import { MaquinarioService } from '../service/maquinario.service';
import { IHistoricoLocacao } from 'app/entities/historico-locacao/historico-locacao.model';
import { HistoricoLocacaoService } from 'app/entities/historico-locacao/service/historico-locacao.service';
import { ITipoMaquinario } from 'app/entities/tipo-maquinario/tipo-maquinario.model';
import { TipoMaquinarioService } from 'app/entities/tipo-maquinario/service/tipo-maquinario.service';

@Component({
  selector: 'jhi-maquinario-update',
  templateUrl: './maquinario-update.component.html',
})
export class MaquinarioUpdateComponent implements OnInit {
  isSaving = false;

  historicoLocacaosSharedCollection: IHistoricoLocacao[] = [];
  tipoMaquinariosSharedCollection: ITipoMaquinario[] = [];

  editForm = this.fb.group({
    id: [],
    nome: [null, [Validators.required]],
    modelo: [null, [Validators.required]],
    anoFabricacao: [null, [Validators.required]],
    capacidadeLitro: [null, [Validators.required]],
    volumeMinuto: [null, [Validators.required]],
    descricao: [],
    historicoLocacao: [],
    tipoMaquinario: [],
  });

  constructor(
    protected maquinarioService: MaquinarioService,
    protected historicoLocacaoService: HistoricoLocacaoService,
    protected tipoMaquinarioService: TipoMaquinarioService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maquinario }) => {
      this.updateForm(maquinario);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const maquinario = this.createFromForm();
    if (maquinario.id !== undefined) {
      this.subscribeToSaveResponse(this.maquinarioService.update(maquinario));
    } else {
      this.subscribeToSaveResponse(this.maquinarioService.create(maquinario));
    }
  }

  trackHistoricoLocacaoById(index: number, item: IHistoricoLocacao): number {
    return item.id!;
  }

  trackTipoMaquinarioById(index: number, item: ITipoMaquinario): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMaquinario>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(maquinario: IMaquinario): void {
    this.editForm.patchValue({
      id: maquinario.id,
      nome: maquinario.nome,
      modelo: maquinario.modelo,
      anoFabricacao: maquinario.anoFabricacao,
      capacidadeLitro: maquinario.capacidadeLitro,
      volumeMinuto: maquinario.volumeMinuto,
      descricao: maquinario.descricao,
      historicoLocacao: maquinario.historicoLocacao,
      tipoMaquinario: maquinario.tipoMaquinario,
    });

    this.historicoLocacaosSharedCollection = this.historicoLocacaoService.addHistoricoLocacaoToCollectionIfMissing(
      this.historicoLocacaosSharedCollection,
      maquinario.historicoLocacao
    );
    this.tipoMaquinariosSharedCollection = this.tipoMaquinarioService.addTipoMaquinarioToCollectionIfMissing(
      this.tipoMaquinariosSharedCollection,
      maquinario.tipoMaquinario
    );
  }

  protected loadRelationshipsOptions(): void {
    this.historicoLocacaoService
      .query()
      .pipe(map((res: HttpResponse<IHistoricoLocacao[]>) => res.body ?? []))
      .pipe(
        map((historicoLocacaos: IHistoricoLocacao[]) =>
          this.historicoLocacaoService.addHistoricoLocacaoToCollectionIfMissing(
            historicoLocacaos,
            this.editForm.get('historicoLocacao')!.value
          )
        )
      )
      .subscribe((historicoLocacaos: IHistoricoLocacao[]) => (this.historicoLocacaosSharedCollection = historicoLocacaos));

    this.tipoMaquinarioService
      .query()
      .pipe(map((res: HttpResponse<ITipoMaquinario[]>) => res.body ?? []))
      .pipe(
        map((tipoMaquinarios: ITipoMaquinario[]) =>
          this.tipoMaquinarioService.addTipoMaquinarioToCollectionIfMissing(tipoMaquinarios, this.editForm.get('tipoMaquinario')!.value)
        )
      )
      .subscribe((tipoMaquinarios: ITipoMaquinario[]) => (this.tipoMaquinariosSharedCollection = tipoMaquinarios));
  }

  protected createFromForm(): IMaquinario {
    return {
      ...new Maquinario(),
      id: this.editForm.get(['id'])!.value,
      nome: this.editForm.get(['nome'])!.value,
      modelo: this.editForm.get(['modelo'])!.value,
      anoFabricacao: this.editForm.get(['anoFabricacao'])!.value,
      capacidadeLitro: this.editForm.get(['capacidadeLitro'])!.value,
      volumeMinuto: this.editForm.get(['volumeMinuto'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      historicoLocacao: this.editForm.get(['historicoLocacao.data'])!.value,
      tipoMaquinario: this.editForm.get(['tipoMaquinario.descricao'])!.value,
    };
  }
}
