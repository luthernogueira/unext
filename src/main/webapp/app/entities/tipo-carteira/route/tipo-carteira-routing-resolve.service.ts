import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITipoCarteira, TipoCarteira } from '../tipo-carteira.model';
import { TipoCarteiraService } from '../service/tipo-carteira.service';

@Injectable({ providedIn: 'root' })
export class TipoCarteiraRoutingResolveService implements Resolve<ITipoCarteira> {
  constructor(protected service: TipoCarteiraService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITipoCarteira> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((tipoCarteira: HttpResponse<TipoCarteira>) => {
          if (tipoCarteira.body) {
            return of(tipoCarteira.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TipoCarteira());
  }
}
