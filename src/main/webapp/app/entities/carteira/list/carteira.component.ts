import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICarteira } from '../carteira.model';
import { CarteiraService } from '../service/carteira.service';
import { CarteiraDeleteDialogComponent } from '../delete/carteira-delete-dialog.component';

@Component({
  selector: 'jhi-carteira',
  templateUrl: './carteira.component.html',
})
export class CarteiraComponent implements OnInit {
  carteiras?: ICarteira[];
  isLoading = false;

  constructor(protected carteiraService: CarteiraService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.carteiraService.query().subscribe(
      (res: HttpResponse<ICarteira[]>) => {
        this.isLoading = false;
        this.carteiras = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICarteira): number {
    return item.id!;
  }

  delete(carteira: ICarteira): void {
    const modalRef = this.modalService.open(CarteiraDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.carteira = carteira;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
