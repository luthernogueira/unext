jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ITipoMaquinario, TipoMaquinario } from '../tipo-maquinario.model';
import { TipoMaquinarioService } from '../service/tipo-maquinario.service';

import { TipoMaquinarioRoutingResolveService } from './tipo-maquinario-routing-resolve.service';

describe('TipoMaquinario routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: TipoMaquinarioRoutingResolveService;
  let service: TipoMaquinarioService;
  let resultTipoMaquinario: ITipoMaquinario | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(TipoMaquinarioRoutingResolveService);
    service = TestBed.inject(TipoMaquinarioService);
    resultTipoMaquinario = undefined;
  });

  describe('resolve', () => {
    it('should return ITipoMaquinario returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTipoMaquinario = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultTipoMaquinario).toEqual({ id: 123 });
    });

    it('should return new ITipoMaquinario if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTipoMaquinario = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultTipoMaquinario).toEqual(new TipoMaquinario());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as TipoMaquinario })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTipoMaquinario = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultTipoMaquinario).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
