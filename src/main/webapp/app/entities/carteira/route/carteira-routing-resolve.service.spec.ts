jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICarteira, Carteira } from '../carteira.model';
import { CarteiraService } from '../service/carteira.service';

import { CarteiraRoutingResolveService } from './carteira-routing-resolve.service';

describe('Carteira routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CarteiraRoutingResolveService;
  let service: CarteiraService;
  let resultCarteira: ICarteira | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(CarteiraRoutingResolveService);
    service = TestBed.inject(CarteiraService);
    resultCarteira = undefined;
  });

  describe('resolve', () => {
    it('should return ICarteira returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCarteira = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCarteira).toEqual({ id: 123 });
    });

    it('should return new ICarteira if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCarteira = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCarteira).toEqual(new Carteira());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Carteira })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCarteira = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCarteira).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
