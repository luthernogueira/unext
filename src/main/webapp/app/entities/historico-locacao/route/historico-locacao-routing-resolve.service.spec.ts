jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IHistoricoLocacao, HistoricoLocacao } from '../historico-locacao.model';
import { HistoricoLocacaoService } from '../service/historico-locacao.service';

import { HistoricoLocacaoRoutingResolveService } from './historico-locacao-routing-resolve.service';

describe('HistoricoLocacao routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: HistoricoLocacaoRoutingResolveService;
  let service: HistoricoLocacaoService;
  let resultHistoricoLocacao: IHistoricoLocacao | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(HistoricoLocacaoRoutingResolveService);
    service = TestBed.inject(HistoricoLocacaoService);
    resultHistoricoLocacao = undefined;
  });

  describe('resolve', () => {
    it('should return IHistoricoLocacao returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultHistoricoLocacao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultHistoricoLocacao).toEqual({ id: 123 });
    });

    it('should return new IHistoricoLocacao if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultHistoricoLocacao = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultHistoricoLocacao).toEqual(new HistoricoLocacao());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as HistoricoLocacao })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultHistoricoLocacao = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultHistoricoLocacao).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
