import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITipoMaquinario } from '../tipo-maquinario.model';

@Component({
  selector: 'jhi-tipo-maquinario-detail',
  templateUrl: './tipo-maquinario-detail.component.html',
})
export class TipoMaquinarioDetailComponent implements OnInit {
  tipoMaquinario: ITipoMaquinario | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tipoMaquinario }) => {
      this.tipoMaquinario = tipoMaquinario;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
