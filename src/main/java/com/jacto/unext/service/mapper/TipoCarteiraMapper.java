package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.TipoCarteira;
import com.jacto.unext.service.dto.TipoCarteiraDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TipoCarteira} and its DTO {@link TipoCarteiraDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TipoCarteiraMapper extends EntityMapper<TipoCarteiraDTO, TipoCarteira> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TipoCarteiraDTO toDtoId(TipoCarteira tipoCarteira);
}
