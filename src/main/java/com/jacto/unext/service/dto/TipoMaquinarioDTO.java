package com.jacto.unext.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jacto.unext.domain.TipoMaquinario} entity.
 */
public class TipoMaquinarioDTO implements Serializable {

    private Long id;

    @NotNull
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TipoMaquinarioDTO)) {
            return false;
        }

        TipoMaquinarioDTO tipoMaquinarioDTO = (TipoMaquinarioDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, tipoMaquinarioDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TipoMaquinarioDTO{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            "}";
    }
}
