import * as dayjs from 'dayjs';
import { IHistoricoLocacao } from 'app/entities/historico-locacao/historico-locacao.model';
import { ITipoMaquinario } from 'app/entities/tipo-maquinario/tipo-maquinario.model';

export interface IMaquinario {
  id?: number;
  nome?: string;
  modelo?: string;
  anoFabricacao?: dayjs.Dayjs;
  capacidadeLitro?: number;
  volumeMinuto?: number;
  descricao?: number | null;
  historicoLocacao?: IHistoricoLocacao | null;
  tipoMaquinario?: ITipoMaquinario | null;
}

export class Maquinario implements IMaquinario {
  constructor(
    public id?: number,
    public nome?: string,
    public modelo?: string,
    public anoFabricacao?: dayjs.Dayjs,
    public capacidadeLitro?: number,
    public volumeMinuto?: number,
    public descricao?: number | null,
    public historicoLocacao?: IHistoricoLocacao | null,
    public tipoMaquinario?: ITipoMaquinario | null
  ) {}
}

export function getMaquinarioIdentifier(maquinario: IMaquinario): number | undefined {
  return maquinario.id;
}
