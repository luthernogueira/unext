jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { TipoCarteiraService } from '../service/tipo-carteira.service';
import { ITipoCarteira, TipoCarteira } from '../tipo-carteira.model';

import { TipoCarteiraUpdateComponent } from './tipo-carteira-update.component';

describe('TipoCarteira Management Update Component', () => {
  let comp: TipoCarteiraUpdateComponent;
  let fixture: ComponentFixture<TipoCarteiraUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let tipoCarteiraService: TipoCarteiraService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TipoCarteiraUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(TipoCarteiraUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TipoCarteiraUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    tipoCarteiraService = TestBed.inject(TipoCarteiraService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const tipoCarteira: ITipoCarteira = { id: 456 };

      activatedRoute.data = of({ tipoCarteira });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(tipoCarteira));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoCarteira>>();
      const tipoCarteira = { id: 123 };
      jest.spyOn(tipoCarteiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoCarteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tipoCarteira }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(tipoCarteiraService.update).toHaveBeenCalledWith(tipoCarteira);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoCarteira>>();
      const tipoCarteira = new TipoCarteira();
      jest.spyOn(tipoCarteiraService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoCarteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tipoCarteira }));
      saveSubject.complete();

      // THEN
      expect(tipoCarteiraService.create).toHaveBeenCalledWith(tipoCarteira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoCarteira>>();
      const tipoCarteira = { id: 123 };
      jest.spyOn(tipoCarteiraService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoCarteira });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(tipoCarteiraService.update).toHaveBeenCalledWith(tipoCarteira);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
