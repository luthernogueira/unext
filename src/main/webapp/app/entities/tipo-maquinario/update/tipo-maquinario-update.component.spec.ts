jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { TipoMaquinarioService } from '../service/tipo-maquinario.service';
import { ITipoMaquinario, TipoMaquinario } from '../tipo-maquinario.model';

import { TipoMaquinarioUpdateComponent } from './tipo-maquinario-update.component';

describe('TipoMaquinario Management Update Component', () => {
  let comp: TipoMaquinarioUpdateComponent;
  let fixture: ComponentFixture<TipoMaquinarioUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let tipoMaquinarioService: TipoMaquinarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TipoMaquinarioUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(TipoMaquinarioUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TipoMaquinarioUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    tipoMaquinarioService = TestBed.inject(TipoMaquinarioService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const tipoMaquinario: ITipoMaquinario = { id: 456 };

      activatedRoute.data = of({ tipoMaquinario });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(tipoMaquinario));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoMaquinario>>();
      const tipoMaquinario = { id: 123 };
      jest.spyOn(tipoMaquinarioService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoMaquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tipoMaquinario }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(tipoMaquinarioService.update).toHaveBeenCalledWith(tipoMaquinario);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoMaquinario>>();
      const tipoMaquinario = new TipoMaquinario();
      jest.spyOn(tipoMaquinarioService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoMaquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tipoMaquinario }));
      saveSubject.complete();

      // THEN
      expect(tipoMaquinarioService.create).toHaveBeenCalledWith(tipoMaquinario);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TipoMaquinario>>();
      const tipoMaquinario = { id: 123 };
      jest.spyOn(tipoMaquinarioService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tipoMaquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(tipoMaquinarioService.update).toHaveBeenCalledWith(tipoMaquinario);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
