import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MaquinarioComponent } from './list/maquinario.component';
import { MaquinarioDetailComponent } from './detail/maquinario-detail.component';
import { MaquinarioUpdateComponent } from './update/maquinario-update.component';
import { MaquinarioDeleteDialogComponent } from './delete/maquinario-delete-dialog.component';
import { MaquinarioRoutingModule } from './route/maquinario-routing.module';

@NgModule({
  imports: [SharedModule, MaquinarioRoutingModule],
  declarations: [MaquinarioComponent, MaquinarioDetailComponent, MaquinarioUpdateComponent, MaquinarioDeleteDialogComponent],
  entryComponents: [MaquinarioDeleteDialogComponent],
})
export class MaquinarioModule {}
