package com.jacto.unext.web.rest;

import com.jacto.unext.repository.CarteiraRepository;
import com.jacto.unext.service.CarteiraService;
import com.jacto.unext.service.dto.CarteiraDTO;
import com.jacto.unext.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jacto.unext.domain.Carteira}.
 */
@RestController
@RequestMapping("/api")
public class CarteiraResource {

    private final Logger log = LoggerFactory.getLogger(CarteiraResource.class);

    private static final String ENTITY_NAME = "carteira";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CarteiraService carteiraService;

    private final CarteiraRepository carteiraRepository;

    public CarteiraResource(CarteiraService carteiraService, CarteiraRepository carteiraRepository) {
        this.carteiraService = carteiraService;
        this.carteiraRepository = carteiraRepository;
    }

    /**
     * {@code POST  /carteiras} : Create a new carteira.
     *
     * @param carteiraDTO the carteiraDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new carteiraDTO, or with status {@code 400 (Bad Request)} if the carteira has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/carteiras")
    public ResponseEntity<CarteiraDTO> createCarteira(@Valid @RequestBody CarteiraDTO carteiraDTO) throws URISyntaxException {
        log.debug("REST request to save Carteira : {}", carteiraDTO);
        if (carteiraDTO.getId() != null) {
            throw new BadRequestAlertException("A new carteira cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CarteiraDTO result = carteiraService.save(carteiraDTO);
        return ResponseEntity
            .created(new URI("/api/carteiras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /carteiras/:id} : Updates an existing carteira.
     *
     * @param id the id of the carteiraDTO to save.
     * @param carteiraDTO the carteiraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated carteiraDTO,
     * or with status {@code 400 (Bad Request)} if the carteiraDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the carteiraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/carteiras/{id}")
    public ResponseEntity<CarteiraDTO> updateCarteira(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CarteiraDTO carteiraDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Carteira : {}, {}", id, carteiraDTO);
        if (carteiraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, carteiraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!carteiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CarteiraDTO result = carteiraService.save(carteiraDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, carteiraDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /carteiras/:id} : Partial updates given fields of an existing carteira, field will ignore if it is null
     *
     * @param id the id of the carteiraDTO to save.
     * @param carteiraDTO the carteiraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated carteiraDTO,
     * or with status {@code 400 (Bad Request)} if the carteiraDTO is not valid,
     * or with status {@code 404 (Not Found)} if the carteiraDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the carteiraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/carteiras/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CarteiraDTO> partialUpdateCarteira(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CarteiraDTO carteiraDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Carteira partially : {}, {}", id, carteiraDTO);
        if (carteiraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, carteiraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!carteiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CarteiraDTO> result = carteiraService.partialUpdate(carteiraDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, carteiraDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /carteiras} : get all the carteiras.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of carteiras in body.
     */
    @GetMapping("/carteiras")
    public List<CarteiraDTO> getAllCarteiras() {
        log.debug("REST request to get all Carteiras");
        return carteiraService.findAll();
    }

    /**
     * {@code GET  /carteiras/:id} : get the "id" carteira.
     *
     * @param id the id of the carteiraDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the carteiraDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/carteiras/{id}")
    public ResponseEntity<CarteiraDTO> getCarteira(@PathVariable Long id) {
        log.debug("REST request to get Carteira : {}", id);
        Optional<CarteiraDTO> carteiraDTO = carteiraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(carteiraDTO);
    }

    /**
     * {@code DELETE  /carteiras/:id} : delete the "id" carteira.
     *
     * @param id the id of the carteiraDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/carteiras/{id}")
    public ResponseEntity<Void> deleteCarteira(@PathVariable Long id) {
        log.debug("REST request to delete Carteira : {}", id);
        carteiraService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
