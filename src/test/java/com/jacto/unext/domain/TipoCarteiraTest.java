package com.jacto.unext.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TipoCarteiraTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoCarteira.class);
        TipoCarteira tipoCarteira1 = new TipoCarteira();
        tipoCarteira1.setId(1L);
        TipoCarteira tipoCarteira2 = new TipoCarteira();
        tipoCarteira2.setId(tipoCarteira1.getId());
        assertThat(tipoCarteira1).isEqualTo(tipoCarteira2);
        tipoCarteira2.setId(2L);
        assertThat(tipoCarteira1).isNotEqualTo(tipoCarteira2);
        tipoCarteira1.setId(null);
        assertThat(tipoCarteira1).isNotEqualTo(tipoCarteira2);
    }
}
