import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHistoricoLocacao } from '../historico-locacao.model';

@Component({
  selector: 'jhi-historico-locacao-detail',
  templateUrl: './historico-locacao-detail.component.html',
})
export class HistoricoLocacaoDetailComponent implements OnInit {
  historicoLocacao: IHistoricoLocacao | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ historicoLocacao }) => {
      this.historicoLocacao = historicoLocacao;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
