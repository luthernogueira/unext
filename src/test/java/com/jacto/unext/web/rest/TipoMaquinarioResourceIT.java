package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.TipoMaquinario;
import com.jacto.unext.repository.TipoMaquinarioRepository;
import com.jacto.unext.service.dto.TipoMaquinarioDTO;
import com.jacto.unext.service.mapper.TipoMaquinarioMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link TipoMaquinarioResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TipoMaquinarioResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/tipo-maquinarios";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TipoMaquinarioRepository tipoMaquinarioRepository;

    @Autowired
    private TipoMaquinarioMapper tipoMaquinarioMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTipoMaquinarioMockMvc;

    private TipoMaquinario tipoMaquinario;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoMaquinario createEntity(EntityManager em) {
        TipoMaquinario tipoMaquinario = new TipoMaquinario().descricao(DEFAULT_DESCRICAO);
        return tipoMaquinario;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoMaquinario createUpdatedEntity(EntityManager em) {
        TipoMaquinario tipoMaquinario = new TipoMaquinario().descricao(UPDATED_DESCRICAO);
        return tipoMaquinario;
    }

    @BeforeEach
    public void initTest() {
        tipoMaquinario = createEntity(em);
    }

    @Test
    @Transactional
    void createTipoMaquinario() throws Exception {
        int databaseSizeBeforeCreate = tipoMaquinarioRepository.findAll().size();
        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);
        restTipoMaquinarioMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isCreated());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeCreate + 1);
        TipoMaquinario testTipoMaquinario = tipoMaquinarioList.get(tipoMaquinarioList.size() - 1);
        assertThat(testTipoMaquinario.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createTipoMaquinarioWithExistingId() throws Exception {
        // Create the TipoMaquinario with an existing ID
        tipoMaquinario.setId(1L);
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        int databaseSizeBeforeCreate = tipoMaquinarioRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoMaquinarioMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDescricaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoMaquinarioRepository.findAll().size();
        // set the field null
        tipoMaquinario.setDescricao(null);

        // Create the TipoMaquinario, which fails.
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        restTipoMaquinarioMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTipoMaquinarios() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        // Get all the tipoMaquinarioList
        restTipoMaquinarioMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoMaquinario.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)));
    }

    @Test
    @Transactional
    void getTipoMaquinario() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        // Get the tipoMaquinario
        restTipoMaquinarioMockMvc
            .perform(get(ENTITY_API_URL_ID, tipoMaquinario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(tipoMaquinario.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO));
    }

    @Test
    @Transactional
    void getNonExistingTipoMaquinario() throws Exception {
        // Get the tipoMaquinario
        restTipoMaquinarioMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewTipoMaquinario() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();

        // Update the tipoMaquinario
        TipoMaquinario updatedTipoMaquinario = tipoMaquinarioRepository.findById(tipoMaquinario.getId()).get();
        // Disconnect from session so that the updates on updatedTipoMaquinario are not directly saved in db
        em.detach(updatedTipoMaquinario);
        updatedTipoMaquinario.descricao(UPDATED_DESCRICAO);
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(updatedTipoMaquinario);

        restTipoMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tipoMaquinarioDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isOk());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
        TipoMaquinario testTipoMaquinario = tipoMaquinarioList.get(tipoMaquinarioList.size() - 1);
        assertThat(testTipoMaquinario.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, tipoMaquinarioDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTipoMaquinarioWithPatch() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();

        // Update the tipoMaquinario using partial update
        TipoMaquinario partialUpdatedTipoMaquinario = new TipoMaquinario();
        partialUpdatedTipoMaquinario.setId(tipoMaquinario.getId());

        restTipoMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTipoMaquinario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTipoMaquinario))
            )
            .andExpect(status().isOk());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
        TipoMaquinario testTipoMaquinario = tipoMaquinarioList.get(tipoMaquinarioList.size() - 1);
        assertThat(testTipoMaquinario.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateTipoMaquinarioWithPatch() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();

        // Update the tipoMaquinario using partial update
        TipoMaquinario partialUpdatedTipoMaquinario = new TipoMaquinario();
        partialUpdatedTipoMaquinario.setId(tipoMaquinario.getId());

        partialUpdatedTipoMaquinario.descricao(UPDATED_DESCRICAO);

        restTipoMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTipoMaquinario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTipoMaquinario))
            )
            .andExpect(status().isOk());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
        TipoMaquinario testTipoMaquinario = tipoMaquinarioList.get(tipoMaquinarioList.size() - 1);
        assertThat(testTipoMaquinario.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, tipoMaquinarioDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTipoMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = tipoMaquinarioRepository.findAll().size();
        tipoMaquinario.setId(count.incrementAndGet());

        // Create the TipoMaquinario
        TipoMaquinarioDTO tipoMaquinarioDTO = tipoMaquinarioMapper.toDto(tipoMaquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTipoMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(tipoMaquinarioDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TipoMaquinario in the database
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTipoMaquinario() throws Exception {
        // Initialize the database
        tipoMaquinarioRepository.saveAndFlush(tipoMaquinario);

        int databaseSizeBeforeDelete = tipoMaquinarioRepository.findAll().size();

        // Delete the tipoMaquinario
        restTipoMaquinarioMockMvc
            .perform(delete(ENTITY_API_URL_ID, tipoMaquinario.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TipoMaquinario> tipoMaquinarioList = tipoMaquinarioRepository.findAll();
        assertThat(tipoMaquinarioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
