import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITipoMaquinario, TipoMaquinario } from '../tipo-maquinario.model';

import { TipoMaquinarioService } from './tipo-maquinario.service';

describe('TipoMaquinario Service', () => {
  let service: TipoMaquinarioService;
  let httpMock: HttpTestingController;
  let elemDefault: ITipoMaquinario;
  let expectedResult: ITipoMaquinario | ITipoMaquinario[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TipoMaquinarioService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      descricao: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a TipoMaquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new TipoMaquinario()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TipoMaquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TipoMaquinario', () => {
      const patchObject = Object.assign({}, new TipoMaquinario());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TipoMaquinario', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a TipoMaquinario', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addTipoMaquinarioToCollectionIfMissing', () => {
      it('should add a TipoMaquinario to an empty array', () => {
        const tipoMaquinario: ITipoMaquinario = { id: 123 };
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing([], tipoMaquinario);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(tipoMaquinario);
      });

      it('should not add a TipoMaquinario to an array that contains it', () => {
        const tipoMaquinario: ITipoMaquinario = { id: 123 };
        const tipoMaquinarioCollection: ITipoMaquinario[] = [
          {
            ...tipoMaquinario,
          },
          { id: 456 },
        ];
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing(tipoMaquinarioCollection, tipoMaquinario);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TipoMaquinario to an array that doesn't contain it", () => {
        const tipoMaquinario: ITipoMaquinario = { id: 123 };
        const tipoMaquinarioCollection: ITipoMaquinario[] = [{ id: 456 }];
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing(tipoMaquinarioCollection, tipoMaquinario);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(tipoMaquinario);
      });

      it('should add only unique TipoMaquinario to an array', () => {
        const tipoMaquinarioArray: ITipoMaquinario[] = [{ id: 123 }, { id: 456 }, { id: 60587 }];
        const tipoMaquinarioCollection: ITipoMaquinario[] = [{ id: 123 }];
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing(tipoMaquinarioCollection, ...tipoMaquinarioArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const tipoMaquinario: ITipoMaquinario = { id: 123 };
        const tipoMaquinario2: ITipoMaquinario = { id: 456 };
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing([], tipoMaquinario, tipoMaquinario2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(tipoMaquinario);
        expect(expectedResult).toContain(tipoMaquinario2);
      });

      it('should accept null and undefined values', () => {
        const tipoMaquinario: ITipoMaquinario = { id: 123 };
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing([], null, tipoMaquinario, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(tipoMaquinario);
      });

      it('should return initial array if no TipoMaquinario is added', () => {
        const tipoMaquinarioCollection: ITipoMaquinario[] = [{ id: 123 }];
        expectedResult = service.addTipoMaquinarioToCollectionIfMissing(tipoMaquinarioCollection, undefined, null);
        expect(expectedResult).toEqual(tipoMaquinarioCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
