import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CarteiraService } from '../service/carteira.service';

import { CarteiraComponent } from './carteira.component';

describe('Carteira Management Component', () => {
  let comp: CarteiraComponent;
  let fixture: ComponentFixture<CarteiraComponent>;
  let service: CarteiraService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [CarteiraComponent],
    })
      .overrideTemplate(CarteiraComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CarteiraComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(CarteiraService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.carteiras?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
