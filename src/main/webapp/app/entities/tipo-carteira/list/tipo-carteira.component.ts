import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITipoCarteira } from '../tipo-carteira.model';
import { TipoCarteiraService } from '../service/tipo-carteira.service';
import { TipoCarteiraDeleteDialogComponent } from '../delete/tipo-carteira-delete-dialog.component';

@Component({
  selector: 'jhi-tipo-carteira',
  templateUrl: './tipo-carteira.component.html',
})
export class TipoCarteiraComponent implements OnInit {
  tipoCarteiras?: ITipoCarteira[];
  isLoading = false;

  constructor(protected tipoCarteiraService: TipoCarteiraService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.tipoCarteiraService.query().subscribe(
      (res: HttpResponse<ITipoCarteira[]>) => {
        this.isLoading = false;
        this.tipoCarteiras = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ITipoCarteira): number {
    return item.id!;
  }

  delete(tipoCarteira: ITipoCarteira): void {
    const modalRef = this.modalService.open(TipoCarteiraDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tipoCarteira = tipoCarteira;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
