import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IHistoricoLocacao } from '../historico-locacao.model';
import { HistoricoLocacaoService } from '../service/historico-locacao.service';

@Component({
  templateUrl: './historico-locacao-delete-dialog.component.html',
})
export class HistoricoLocacaoDeleteDialogComponent {
  historicoLocacao?: IHistoricoLocacao;

  constructor(protected historicoLocacaoService: HistoricoLocacaoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.historicoLocacaoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
