import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITipoCarteira, TipoCarteira } from '../tipo-carteira.model';

import { TipoCarteiraService } from './tipo-carteira.service';

describe('TipoCarteira Service', () => {
  let service: TipoCarteiraService;
  let httpMock: HttpTestingController;
  let elemDefault: ITipoCarteira;
  let expectedResult: ITipoCarteira | ITipoCarteira[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TipoCarteiraService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      descricao: 'AAAAAAA',
      bandeira: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a TipoCarteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new TipoCarteira()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TipoCarteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
          bandeira: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TipoCarteira', () => {
      const patchObject = Object.assign(
        {
          descricao: 'BBBBBB',
        },
        new TipoCarteira()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TipoCarteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          descricao: 'BBBBBB',
          bandeira: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a TipoCarteira', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addTipoCarteiraToCollectionIfMissing', () => {
      it('should add a TipoCarteira to an empty array', () => {
        const tipoCarteira: ITipoCarteira = { id: 123 };
        expectedResult = service.addTipoCarteiraToCollectionIfMissing([], tipoCarteira);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(tipoCarteira);
      });

      it('should not add a TipoCarteira to an array that contains it', () => {
        const tipoCarteira: ITipoCarteira = { id: 123 };
        const tipoCarteiraCollection: ITipoCarteira[] = [
          {
            ...tipoCarteira,
          },
          { id: 456 },
        ];
        expectedResult = service.addTipoCarteiraToCollectionIfMissing(tipoCarteiraCollection, tipoCarteira);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TipoCarteira to an array that doesn't contain it", () => {
        const tipoCarteira: ITipoCarteira = { id: 123 };
        const tipoCarteiraCollection: ITipoCarteira[] = [{ id: 456 }];
        expectedResult = service.addTipoCarteiraToCollectionIfMissing(tipoCarteiraCollection, tipoCarteira);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(tipoCarteira);
      });

      it('should add only unique TipoCarteira to an array', () => {
        const tipoCarteiraArray: ITipoCarteira[] = [{ id: 123 }, { id: 456 }, { id: 78936 }];
        const tipoCarteiraCollection: ITipoCarteira[] = [{ id: 123 }];
        expectedResult = service.addTipoCarteiraToCollectionIfMissing(tipoCarteiraCollection, ...tipoCarteiraArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const tipoCarteira: ITipoCarteira = { id: 123 };
        const tipoCarteira2: ITipoCarteira = { id: 456 };
        expectedResult = service.addTipoCarteiraToCollectionIfMissing([], tipoCarteira, tipoCarteira2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(tipoCarteira);
        expect(expectedResult).toContain(tipoCarteira2);
      });

      it('should accept null and undefined values', () => {
        const tipoCarteira: ITipoCarteira = { id: 123 };
        expectedResult = service.addTipoCarteiraToCollectionIfMissing([], null, tipoCarteira, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(tipoCarteira);
      });

      it('should return initial array if no TipoCarteira is added', () => {
        const tipoCarteiraCollection: ITipoCarteira[] = [{ id: 123 }];
        expectedResult = service.addTipoCarteiraToCollectionIfMissing(tipoCarteiraCollection, undefined, null);
        expect(expectedResult).toEqual(tipoCarteiraCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
