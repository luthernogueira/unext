package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.Carteira;
import com.jacto.unext.service.dto.CarteiraDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Carteira} and its DTO {@link CarteiraDTO}.
 */
@Mapper(componentModel = "spring", uses = { TipoCarteiraMapper.class, ClienteMapper.class, ParceiroMapper.class })
public interface CarteiraMapper extends EntityMapper<CarteiraDTO, Carteira> {
    @Mapping(target = "tipoCarteira", source = "tipoCarteira", qualifiedByName = "id")
    @Mapping(target = "cliente", source = "cliente", qualifiedByName = "id")
    @Mapping(target = "parceiro", source = "parceiro", qualifiedByName = "id")
    CarteiraDTO toDto(Carteira s);
}
