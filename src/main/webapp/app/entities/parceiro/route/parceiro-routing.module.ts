import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ParceiroComponent } from '../list/parceiro.component';
import { ParceiroDetailComponent } from '../detail/parceiro-detail.component';
import { ParceiroUpdateComponent } from '../update/parceiro-update.component';
import { ParceiroRoutingResolveService } from './parceiro-routing-resolve.service';

const parceiroRoute: Routes = [
  {
    path: '',
    component: ParceiroComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ParceiroDetailComponent,
    resolve: {
      parceiro: ParceiroRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ParceiroUpdateComponent,
    resolve: {
      parceiro: ParceiroRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ParceiroUpdateComponent,
    resolve: {
      parceiro: ParceiroRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(parceiroRoute)],
  exports: [RouterModule],
})
export class ParceiroRoutingModule {}
