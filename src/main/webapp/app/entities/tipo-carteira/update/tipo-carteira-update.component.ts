import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ITipoCarteira, TipoCarteira } from '../tipo-carteira.model';
import { TipoCarteiraService } from '../service/tipo-carteira.service';

@Component({
  selector: 'jhi-tipo-carteira-update',
  templateUrl: './tipo-carteira-update.component.html',
})
export class TipoCarteiraUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    descricao: [null, [Validators.required]],
    bandeira: [null, [Validators.required]],
  });

  constructor(protected tipoCarteiraService: TipoCarteiraService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tipoCarteira }) => {
      this.updateForm(tipoCarteira);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tipoCarteira = this.createFromForm();
    if (tipoCarteira.id !== undefined) {
      this.subscribeToSaveResponse(this.tipoCarteiraService.update(tipoCarteira));
    } else {
      this.subscribeToSaveResponse(this.tipoCarteiraService.create(tipoCarteira));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoCarteira>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(tipoCarteira: ITipoCarteira): void {
    this.editForm.patchValue({
      id: tipoCarteira.id,
      descricao: tipoCarteira.descricao,
      bandeira: tipoCarteira.bandeira,
    });
  }

  protected createFromForm(): ITipoCarteira {
    return {
      ...new TipoCarteira(),
      id: this.editForm.get(['id'])!.value,
      descricao: this.editForm.get(['descricao'])!.value,
      bandeira: this.editForm.get(['bandeira'])!.value,
    };
  }
}
