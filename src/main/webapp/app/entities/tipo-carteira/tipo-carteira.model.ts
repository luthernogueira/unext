import { ICarteira } from 'app/entities/carteira/carteira.model';

export interface ITipoCarteira {
  id?: number;
  descricao?: string;
  bandeira?: string;
  carteiras?: ICarteira[] | null;
}

export class TipoCarteira implements ITipoCarteira {
  constructor(public id?: number, public descricao?: string, public bandeira?: string, public carteiras?: ICarteira[] | null) {}
}

export function getTipoCarteiraIdentifier(tipoCarteira: ITipoCarteira): number | undefined {
  return tipoCarteira.id;
}
