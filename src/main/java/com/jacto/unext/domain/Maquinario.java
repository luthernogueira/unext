package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Maquinario.
 */
@Entity
@Table(name = "maquinario")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Maquinario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "modelo", nullable = false)
    private String modelo;

    @NotNull
    @Column(name = "ano_fabricacao", nullable = false)
    private LocalDate anoFabricacao;

    @NotNull
    @Column(name = "capacidade_litro", nullable = false)
    private Float capacidadeLitro;

    @NotNull
    @Column(name = "volume_minuto", nullable = false)
    private Float volumeMinuto;

    @Column(name = "descricao")
    private Long descricao;

    @ManyToOne
    @JsonIgnoreProperties(value = { "maquinarios", "cliente", "parceiro" }, allowSetters = true)
    private HistoricoLocacao historicoLocacao;

    @ManyToOne
    @JsonIgnoreProperties(value = { "maquinarios" }, allowSetters = true)
    private TipoMaquinario tipoMaquinario;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Maquinario id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Maquinario nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getModelo() {
        return this.modelo;
    }

    public Maquinario modelo(String modelo) {
        this.setModelo(modelo);
        return this;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public LocalDate getAnoFabricacao() {
        return this.anoFabricacao;
    }

    public Maquinario anoFabricacao(LocalDate anoFabricacao) {
        this.setAnoFabricacao(anoFabricacao);
        return this;
    }

    public void setAnoFabricacao(LocalDate anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Float getCapacidadeLitro() {
        return this.capacidadeLitro;
    }

    public Maquinario capacidadeLitro(Float capacidadeLitro) {
        this.setCapacidadeLitro(capacidadeLitro);
        return this;
    }

    public void setCapacidadeLitro(Float capacidadeLitro) {
        this.capacidadeLitro = capacidadeLitro;
    }

    public Float getVolumeMinuto() {
        return this.volumeMinuto;
    }

    public Maquinario volumeMinuto(Float volumeMinuto) {
        this.setVolumeMinuto(volumeMinuto);
        return this;
    }

    public void setVolumeMinuto(Float volumeMinuto) {
        this.volumeMinuto = volumeMinuto;
    }

    public Long getDescricao() {
        return this.descricao;
    }

    public Maquinario descricao(Long descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(Long descricao) {
        this.descricao = descricao;
    }

    public HistoricoLocacao getHistoricoLocacao() {
        return this.historicoLocacao;
    }

    public void setHistoricoLocacao(HistoricoLocacao historicoLocacao) {
        this.historicoLocacao = historicoLocacao;
    }

    public Maquinario historicoLocacao(HistoricoLocacao historicoLocacao) {
        this.setHistoricoLocacao(historicoLocacao);
        return this;
    }

    public TipoMaquinario getTipoMaquinario() {
        return this.tipoMaquinario;
    }

    public void setTipoMaquinario(TipoMaquinario tipoMaquinario) {
        this.tipoMaquinario = tipoMaquinario;
    }

    public Maquinario tipoMaquinario(TipoMaquinario tipoMaquinario) {
        this.setTipoMaquinario(tipoMaquinario);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Maquinario)) {
            return false;
        }
        return id != null && id.equals(((Maquinario) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Maquinario{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", modelo='" + getModelo() + "'" +
            ", anoFabricacao='" + getAnoFabricacao() + "'" +
            ", capacidadeLitro=" + getCapacidadeLitro() +
            ", volumeMinuto=" + getVolumeMinuto() +
            ", descricao=" + getDescricao() +
            "}";
    }
}
