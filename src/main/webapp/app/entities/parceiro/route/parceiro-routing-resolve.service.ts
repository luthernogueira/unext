import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IParceiro, Parceiro } from '../parceiro.model';
import { ParceiroService } from '../service/parceiro.service';

@Injectable({ providedIn: 'root' })
export class ParceiroRoutingResolveService implements Resolve<IParceiro> {
  constructor(protected service: ParceiroService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IParceiro> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((parceiro: HttpResponse<Parceiro>) => {
          if (parceiro.body) {
            return of(parceiro.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Parceiro());
  }
}
