import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IHistoricoLocacao, HistoricoLocacao } from '../historico-locacao.model';

import { HistoricoLocacaoService } from './historico-locacao.service';

describe('HistoricoLocacao Service', () => {
  let service: HistoricoLocacaoService;
  let httpMock: HttpTestingController;
  let elemDefault: IHistoricoLocacao;
  let expectedResult: IHistoricoLocacao | IHistoricoLocacao[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(HistoricoLocacaoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      data: currentDate,
      tempoLocacao: 0,
      valorTotal: 0,
      fornecedor: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          data: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a HistoricoLocacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          data: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          data: currentDate,
        },
        returnedFromService
      );

      service.create(new HistoricoLocacao()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a HistoricoLocacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          data: currentDate.format(DATE_FORMAT),
          tempoLocacao: 1,
          valorTotal: 1,
          fornecedor: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          data: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a HistoricoLocacao', () => {
      const patchObject = Object.assign(
        {
          tempoLocacao: 1,
          valorTotal: 1,
          fornecedor: 'BBBBBB',
        },
        new HistoricoLocacao()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          data: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of HistoricoLocacao', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          data: currentDate.format(DATE_FORMAT),
          tempoLocacao: 1,
          valorTotal: 1,
          fornecedor: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          data: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a HistoricoLocacao', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addHistoricoLocacaoToCollectionIfMissing', () => {
      it('should add a HistoricoLocacao to an empty array', () => {
        const historicoLocacao: IHistoricoLocacao = { id: 123 };
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing([], historicoLocacao);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(historicoLocacao);
      });

      it('should not add a HistoricoLocacao to an array that contains it', () => {
        const historicoLocacao: IHistoricoLocacao = { id: 123 };
        const historicoLocacaoCollection: IHistoricoLocacao[] = [
          {
            ...historicoLocacao,
          },
          { id: 456 },
        ];
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing(historicoLocacaoCollection, historicoLocacao);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a HistoricoLocacao to an array that doesn't contain it", () => {
        const historicoLocacao: IHistoricoLocacao = { id: 123 };
        const historicoLocacaoCollection: IHistoricoLocacao[] = [{ id: 456 }];
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing(historicoLocacaoCollection, historicoLocacao);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(historicoLocacao);
      });

      it('should add only unique HistoricoLocacao to an array', () => {
        const historicoLocacaoArray: IHistoricoLocacao[] = [{ id: 123 }, { id: 456 }, { id: 47782 }];
        const historicoLocacaoCollection: IHistoricoLocacao[] = [{ id: 123 }];
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing(historicoLocacaoCollection, ...historicoLocacaoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const historicoLocacao: IHistoricoLocacao = { id: 123 };
        const historicoLocacao2: IHistoricoLocacao = { id: 456 };
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing([], historicoLocacao, historicoLocacao2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(historicoLocacao);
        expect(expectedResult).toContain(historicoLocacao2);
      });

      it('should accept null and undefined values', () => {
        const historicoLocacao: IHistoricoLocacao = { id: 123 };
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing([], null, historicoLocacao, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(historicoLocacao);
      });

      it('should return initial array if no HistoricoLocacao is added', () => {
        const historicoLocacaoCollection: IHistoricoLocacao[] = [{ id: 123 }];
        expectedResult = service.addHistoricoLocacaoToCollectionIfMissing(historicoLocacaoCollection, undefined, null);
        expect(expectedResult).toEqual(historicoLocacaoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
