import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { TipoCarteiraService } from '../service/tipo-carteira.service';

import { TipoCarteiraComponent } from './tipo-carteira.component';

describe('TipoCarteira Management Component', () => {
  let comp: TipoCarteiraComponent;
  let fixture: ComponentFixture<TipoCarteiraComponent>;
  let service: TipoCarteiraService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [TipoCarteiraComponent],
    })
      .overrideTemplate(TipoCarteiraComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TipoCarteiraComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(TipoCarteiraService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.tipoCarteiras?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
