import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMaquinario, Maquinario } from '../maquinario.model';
import { MaquinarioService } from '../service/maquinario.service';

@Injectable({ providedIn: 'root' })
export class MaquinarioRoutingResolveService implements Resolve<IMaquinario> {
  constructor(protected service: MaquinarioService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMaquinario> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((maquinario: HttpResponse<Maquinario>) => {
          if (maquinario.body) {
            return of(maquinario.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Maquinario());
  }
}
