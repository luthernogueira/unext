import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IParceiro, Parceiro } from '../parceiro.model';

import { ParceiroService } from './parceiro.service';

describe('Parceiro Service', () => {
  let service: ParceiroService;
  let httpMock: HttpTestingController;
  let elemDefault: IParceiro;
  let expectedResult: IParceiro | IParceiro[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ParceiroService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nomeFantasia: 'AAAAAAA',
      razaoSocial: 'AAAAAAA',
      cnpj: 'AAAAAAA',
      tefelone: 0,
      email: 'AAAAAAA',
      rua: 'AAAAAAA',
      numero: 0,
      bairro: 'AAAAAAA',
      cep: 'AAAAAAA',
      cidade: 'AAAAAAA',
      estado: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Parceiro', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Parceiro()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Parceiro', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nomeFantasia: 'BBBBBB',
          razaoSocial: 'BBBBBB',
          cnpj: 'BBBBBB',
          tefelone: 1,
          email: 'BBBBBB',
          rua: 'BBBBBB',
          numero: 1,
          bairro: 'BBBBBB',
          cep: 'BBBBBB',
          cidade: 'BBBBBB',
          estado: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Parceiro', () => {
      const patchObject = Object.assign(
        {
          nomeFantasia: 'BBBBBB',
          razaoSocial: 'BBBBBB',
          rua: 'BBBBBB',
          cep: 'BBBBBB',
        },
        new Parceiro()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Parceiro', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nomeFantasia: 'BBBBBB',
          razaoSocial: 'BBBBBB',
          cnpj: 'BBBBBB',
          tefelone: 1,
          email: 'BBBBBB',
          rua: 'BBBBBB',
          numero: 1,
          bairro: 'BBBBBB',
          cep: 'BBBBBB',
          cidade: 'BBBBBB',
          estado: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Parceiro', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addParceiroToCollectionIfMissing', () => {
      it('should add a Parceiro to an empty array', () => {
        const parceiro: IParceiro = { id: 123 };
        expectedResult = service.addParceiroToCollectionIfMissing([], parceiro);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(parceiro);
      });

      it('should not add a Parceiro to an array that contains it', () => {
        const parceiro: IParceiro = { id: 123 };
        const parceiroCollection: IParceiro[] = [
          {
            ...parceiro,
          },
          { id: 456 },
        ];
        expectedResult = service.addParceiroToCollectionIfMissing(parceiroCollection, parceiro);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Parceiro to an array that doesn't contain it", () => {
        const parceiro: IParceiro = { id: 123 };
        const parceiroCollection: IParceiro[] = [{ id: 456 }];
        expectedResult = service.addParceiroToCollectionIfMissing(parceiroCollection, parceiro);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(parceiro);
      });

      it('should add only unique Parceiro to an array', () => {
        const parceiroArray: IParceiro[] = [{ id: 123 }, { id: 456 }, { id: 86374 }];
        const parceiroCollection: IParceiro[] = [{ id: 123 }];
        expectedResult = service.addParceiroToCollectionIfMissing(parceiroCollection, ...parceiroArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const parceiro: IParceiro = { id: 123 };
        const parceiro2: IParceiro = { id: 456 };
        expectedResult = service.addParceiroToCollectionIfMissing([], parceiro, parceiro2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(parceiro);
        expect(expectedResult).toContain(parceiro2);
      });

      it('should accept null and undefined values', () => {
        const parceiro: IParceiro = { id: 123 };
        expectedResult = service.addParceiroToCollectionIfMissing([], null, parceiro, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(parceiro);
      });

      it('should return initial array if no Parceiro is added', () => {
        const parceiroCollection: IParceiro[] = [{ id: 123 }];
        expectedResult = service.addParceiroToCollectionIfMissing(parceiroCollection, undefined, null);
        expect(expectedResult).toEqual(parceiroCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
