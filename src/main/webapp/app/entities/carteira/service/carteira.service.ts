import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICarteira, getCarteiraIdentifier } from '../carteira.model';

export type EntityResponseType = HttpResponse<ICarteira>;
export type EntityArrayResponseType = HttpResponse<ICarteira[]>;

@Injectable({ providedIn: 'root' })
export class CarteiraService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/carteiras');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(carteira: ICarteira): Observable<EntityResponseType> {
    return this.http.post<ICarteira>(this.resourceUrl, carteira, { observe: 'response' });
  }

  update(carteira: ICarteira): Observable<EntityResponseType> {
    return this.http.put<ICarteira>(`${this.resourceUrl}/${getCarteiraIdentifier(carteira) as number}`, carteira, { observe: 'response' });
  }

  partialUpdate(carteira: ICarteira): Observable<EntityResponseType> {
    return this.http.patch<ICarteira>(`${this.resourceUrl}/${getCarteiraIdentifier(carteira) as number}`, carteira, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICarteira>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICarteira[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCarteiraToCollectionIfMissing(carteiraCollection: ICarteira[], ...carteirasToCheck: (ICarteira | null | undefined)[]): ICarteira[] {
    const carteiras: ICarteira[] = carteirasToCheck.filter(isPresent);
    if (carteiras.length > 0) {
      const carteiraCollectionIdentifiers = carteiraCollection.map(carteiraItem => getCarteiraIdentifier(carteiraItem)!);
      const carteirasToAdd = carteiras.filter(carteiraItem => {
        const carteiraIdentifier = getCarteiraIdentifier(carteiraItem);
        if (carteiraIdentifier == null || carteiraCollectionIdentifiers.includes(carteiraIdentifier)) {
          return false;
        }
        carteiraCollectionIdentifiers.push(carteiraIdentifier);
        return true;
      });
      return [...carteirasToAdd, ...carteiraCollection];
    }
    return carteiraCollection;
  }
}
