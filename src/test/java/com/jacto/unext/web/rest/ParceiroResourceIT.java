package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.Parceiro;
import com.jacto.unext.repository.ParceiroRepository;
import com.jacto.unext.service.dto.ParceiroDTO;
import com.jacto.unext.service.mapper.ParceiroMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ParceiroResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ParceiroResourceIT {

    private static final String DEFAULT_NOME_FANTASIA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_FANTASIA = "BBBBBBBBBB";

    private static final String DEFAULT_RAZAO_SOCIAL = "AAAAAAAAAA";
    private static final String UPDATED_RAZAO_SOCIAL = "BBBBBBBBBB";

    private static final String DEFAULT_CNPJ = "AAAAAAAAAA";
    private static final String UPDATED_CNPJ = "BBBBBBBBBB";

    private static final Integer DEFAULT_TEFELONE = 1;
    private static final Integer UPDATED_TEFELONE = 2;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_RUA = "AAAAAAAAAA";
    private static final String UPDATED_RUA = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO = 1;
    private static final Integer UPDATED_NUMERO = 2;

    private static final String DEFAULT_BAIRRO = "AAAAAAAAAA";
    private static final String UPDATED_BAIRRO = "BBBBBBBBBB";

    private static final String DEFAULT_CEP = "AAAAAAAAAA";
    private static final String UPDATED_CEP = "BBBBBBBBBB";

    private static final String DEFAULT_CIDADE = "AAAAAAAAAA";
    private static final String UPDATED_CIDADE = "BBBBBBBBBB";

    private static final String DEFAULT_ESTADO = "AAAAAAAAAA";
    private static final String UPDATED_ESTADO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/parceiros";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ParceiroRepository parceiroRepository;

    @Autowired
    private ParceiroMapper parceiroMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restParceiroMockMvc;

    private Parceiro parceiro;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parceiro createEntity(EntityManager em) {
        Parceiro parceiro = new Parceiro()
            .nomeFantasia(DEFAULT_NOME_FANTASIA)
            .razaoSocial(DEFAULT_RAZAO_SOCIAL)
            .cnpj(DEFAULT_CNPJ)
            .tefelone(DEFAULT_TEFELONE)
            .email(DEFAULT_EMAIL)
            .rua(DEFAULT_RUA)
            .numero(DEFAULT_NUMERO)
            .bairro(DEFAULT_BAIRRO)
            .cep(DEFAULT_CEP)
            .cidade(DEFAULT_CIDADE)
            .estado(DEFAULT_ESTADO);
        return parceiro;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Parceiro createUpdatedEntity(EntityManager em) {
        Parceiro parceiro = new Parceiro()
            .nomeFantasia(UPDATED_NOME_FANTASIA)
            .razaoSocial(UPDATED_RAZAO_SOCIAL)
            .cnpj(UPDATED_CNPJ)
            .tefelone(UPDATED_TEFELONE)
            .email(UPDATED_EMAIL)
            .rua(UPDATED_RUA)
            .numero(UPDATED_NUMERO)
            .bairro(UPDATED_BAIRRO)
            .cep(UPDATED_CEP)
            .cidade(UPDATED_CIDADE)
            .estado(UPDATED_ESTADO);
        return parceiro;
    }

    @BeforeEach
    public void initTest() {
        parceiro = createEntity(em);
    }

    @Test
    @Transactional
    void createParceiro() throws Exception {
        int databaseSizeBeforeCreate = parceiroRepository.findAll().size();
        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);
        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isCreated());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeCreate + 1);
        Parceiro testParceiro = parceiroList.get(parceiroList.size() - 1);
        assertThat(testParceiro.getNomeFantasia()).isEqualTo(DEFAULT_NOME_FANTASIA);
        assertThat(testParceiro.getRazaoSocial()).isEqualTo(DEFAULT_RAZAO_SOCIAL);
        assertThat(testParceiro.getCnpj()).isEqualTo(DEFAULT_CNPJ);
        assertThat(testParceiro.getTefelone()).isEqualTo(DEFAULT_TEFELONE);
        assertThat(testParceiro.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testParceiro.getRua()).isEqualTo(DEFAULT_RUA);
        assertThat(testParceiro.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testParceiro.getBairro()).isEqualTo(DEFAULT_BAIRRO);
        assertThat(testParceiro.getCep()).isEqualTo(DEFAULT_CEP);
        assertThat(testParceiro.getCidade()).isEqualTo(DEFAULT_CIDADE);
        assertThat(testParceiro.getEstado()).isEqualTo(DEFAULT_ESTADO);
    }

    @Test
    @Transactional
    void createParceiroWithExistingId() throws Exception {
        // Create the Parceiro with an existing ID
        parceiro.setId(1L);
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        int databaseSizeBeforeCreate = parceiroRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeFantasiaIsRequired() throws Exception {
        int databaseSizeBeforeTest = parceiroRepository.findAll().size();
        // set the field null
        parceiro.setNomeFantasia(null);

        // Create the Parceiro, which fails.
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isBadRequest());

        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRazaoSocialIsRequired() throws Exception {
        int databaseSizeBeforeTest = parceiroRepository.findAll().size();
        // set the field null
        parceiro.setRazaoSocial(null);

        // Create the Parceiro, which fails.
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isBadRequest());

        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCnpjIsRequired() throws Exception {
        int databaseSizeBeforeTest = parceiroRepository.findAll().size();
        // set the field null
        parceiro.setCnpj(null);

        // Create the Parceiro, which fails.
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isBadRequest());

        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTefeloneIsRequired() throws Exception {
        int databaseSizeBeforeTest = parceiroRepository.findAll().size();
        // set the field null
        parceiro.setTefelone(null);

        // Create the Parceiro, which fails.
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        restParceiroMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isBadRequest());

        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllParceiros() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        // Get all the parceiroList
        restParceiroMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parceiro.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeFantasia").value(hasItem(DEFAULT_NOME_FANTASIA)))
            .andExpect(jsonPath("$.[*].razaoSocial").value(hasItem(DEFAULT_RAZAO_SOCIAL)))
            .andExpect(jsonPath("$.[*].cnpj").value(hasItem(DEFAULT_CNPJ)))
            .andExpect(jsonPath("$.[*].tefelone").value(hasItem(DEFAULT_TEFELONE)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].rua").value(hasItem(DEFAULT_RUA)))
            .andExpect(jsonPath("$.[*].numero").value(hasItem(DEFAULT_NUMERO)))
            .andExpect(jsonPath("$.[*].bairro").value(hasItem(DEFAULT_BAIRRO)))
            .andExpect(jsonPath("$.[*].cep").value(hasItem(DEFAULT_CEP)))
            .andExpect(jsonPath("$.[*].cidade").value(hasItem(DEFAULT_CIDADE)))
            .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO)));
    }

    @Test
    @Transactional
    void getParceiro() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        // Get the parceiro
        restParceiroMockMvc
            .perform(get(ENTITY_API_URL_ID, parceiro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(parceiro.getId().intValue()))
            .andExpect(jsonPath("$.nomeFantasia").value(DEFAULT_NOME_FANTASIA))
            .andExpect(jsonPath("$.razaoSocial").value(DEFAULT_RAZAO_SOCIAL))
            .andExpect(jsonPath("$.cnpj").value(DEFAULT_CNPJ))
            .andExpect(jsonPath("$.tefelone").value(DEFAULT_TEFELONE))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.rua").value(DEFAULT_RUA))
            .andExpect(jsonPath("$.numero").value(DEFAULT_NUMERO))
            .andExpect(jsonPath("$.bairro").value(DEFAULT_BAIRRO))
            .andExpect(jsonPath("$.cep").value(DEFAULT_CEP))
            .andExpect(jsonPath("$.cidade").value(DEFAULT_CIDADE))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO));
    }

    @Test
    @Transactional
    void getNonExistingParceiro() throws Exception {
        // Get the parceiro
        restParceiroMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewParceiro() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();

        // Update the parceiro
        Parceiro updatedParceiro = parceiroRepository.findById(parceiro.getId()).get();
        // Disconnect from session so that the updates on updatedParceiro are not directly saved in db
        em.detach(updatedParceiro);
        updatedParceiro
            .nomeFantasia(UPDATED_NOME_FANTASIA)
            .razaoSocial(UPDATED_RAZAO_SOCIAL)
            .cnpj(UPDATED_CNPJ)
            .tefelone(UPDATED_TEFELONE)
            .email(UPDATED_EMAIL)
            .rua(UPDATED_RUA)
            .numero(UPDATED_NUMERO)
            .bairro(UPDATED_BAIRRO)
            .cep(UPDATED_CEP)
            .cidade(UPDATED_CIDADE)
            .estado(UPDATED_ESTADO);
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(updatedParceiro);

        restParceiroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, parceiroDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isOk());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
        Parceiro testParceiro = parceiroList.get(parceiroList.size() - 1);
        assertThat(testParceiro.getNomeFantasia()).isEqualTo(UPDATED_NOME_FANTASIA);
        assertThat(testParceiro.getRazaoSocial()).isEqualTo(UPDATED_RAZAO_SOCIAL);
        assertThat(testParceiro.getCnpj()).isEqualTo(UPDATED_CNPJ);
        assertThat(testParceiro.getTefelone()).isEqualTo(UPDATED_TEFELONE);
        assertThat(testParceiro.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testParceiro.getRua()).isEqualTo(UPDATED_RUA);
        assertThat(testParceiro.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testParceiro.getBairro()).isEqualTo(UPDATED_BAIRRO);
        assertThat(testParceiro.getCep()).isEqualTo(UPDATED_CEP);
        assertThat(testParceiro.getCidade()).isEqualTo(UPDATED_CIDADE);
        assertThat(testParceiro.getEstado()).isEqualTo(UPDATED_ESTADO);
    }

    @Test
    @Transactional
    void putNonExistingParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, parceiroDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parceiroDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateParceiroWithPatch() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();

        // Update the parceiro using partial update
        Parceiro partialUpdatedParceiro = new Parceiro();
        partialUpdatedParceiro.setId(parceiro.getId());

        partialUpdatedParceiro.tefelone(UPDATED_TEFELONE).email(UPDATED_EMAIL).rua(UPDATED_RUA).cep(UPDATED_CEP).cidade(UPDATED_CIDADE);

        restParceiroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParceiro.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParceiro))
            )
            .andExpect(status().isOk());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
        Parceiro testParceiro = parceiroList.get(parceiroList.size() - 1);
        assertThat(testParceiro.getNomeFantasia()).isEqualTo(DEFAULT_NOME_FANTASIA);
        assertThat(testParceiro.getRazaoSocial()).isEqualTo(DEFAULT_RAZAO_SOCIAL);
        assertThat(testParceiro.getCnpj()).isEqualTo(DEFAULT_CNPJ);
        assertThat(testParceiro.getTefelone()).isEqualTo(UPDATED_TEFELONE);
        assertThat(testParceiro.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testParceiro.getRua()).isEqualTo(UPDATED_RUA);
        assertThat(testParceiro.getNumero()).isEqualTo(DEFAULT_NUMERO);
        assertThat(testParceiro.getBairro()).isEqualTo(DEFAULT_BAIRRO);
        assertThat(testParceiro.getCep()).isEqualTo(UPDATED_CEP);
        assertThat(testParceiro.getCidade()).isEqualTo(UPDATED_CIDADE);
        assertThat(testParceiro.getEstado()).isEqualTo(DEFAULT_ESTADO);
    }

    @Test
    @Transactional
    void fullUpdateParceiroWithPatch() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();

        // Update the parceiro using partial update
        Parceiro partialUpdatedParceiro = new Parceiro();
        partialUpdatedParceiro.setId(parceiro.getId());

        partialUpdatedParceiro
            .nomeFantasia(UPDATED_NOME_FANTASIA)
            .razaoSocial(UPDATED_RAZAO_SOCIAL)
            .cnpj(UPDATED_CNPJ)
            .tefelone(UPDATED_TEFELONE)
            .email(UPDATED_EMAIL)
            .rua(UPDATED_RUA)
            .numero(UPDATED_NUMERO)
            .bairro(UPDATED_BAIRRO)
            .cep(UPDATED_CEP)
            .cidade(UPDATED_CIDADE)
            .estado(UPDATED_ESTADO);

        restParceiroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParceiro.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParceiro))
            )
            .andExpect(status().isOk());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
        Parceiro testParceiro = parceiroList.get(parceiroList.size() - 1);
        assertThat(testParceiro.getNomeFantasia()).isEqualTo(UPDATED_NOME_FANTASIA);
        assertThat(testParceiro.getRazaoSocial()).isEqualTo(UPDATED_RAZAO_SOCIAL);
        assertThat(testParceiro.getCnpj()).isEqualTo(UPDATED_CNPJ);
        assertThat(testParceiro.getTefelone()).isEqualTo(UPDATED_TEFELONE);
        assertThat(testParceiro.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testParceiro.getRua()).isEqualTo(UPDATED_RUA);
        assertThat(testParceiro.getNumero()).isEqualTo(UPDATED_NUMERO);
        assertThat(testParceiro.getBairro()).isEqualTo(UPDATED_BAIRRO);
        assertThat(testParceiro.getCep()).isEqualTo(UPDATED_CEP);
        assertThat(testParceiro.getCidade()).isEqualTo(UPDATED_CIDADE);
        assertThat(testParceiro.getEstado()).isEqualTo(UPDATED_ESTADO);
    }

    @Test
    @Transactional
    void patchNonExistingParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, parceiroDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamParceiro() throws Exception {
        int databaseSizeBeforeUpdate = parceiroRepository.findAll().size();
        parceiro.setId(count.incrementAndGet());

        // Create the Parceiro
        ParceiroDTO parceiroDTO = parceiroMapper.toDto(parceiro);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParceiroMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(parceiroDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Parceiro in the database
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteParceiro() throws Exception {
        // Initialize the database
        parceiroRepository.saveAndFlush(parceiro);

        int databaseSizeBeforeDelete = parceiroRepository.findAll().size();

        // Delete the parceiro
        restParceiroMockMvc
            .perform(delete(ENTITY_API_URL_ID, parceiro.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Parceiro> parceiroList = parceiroRepository.findAll();
        assertThat(parceiroList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
