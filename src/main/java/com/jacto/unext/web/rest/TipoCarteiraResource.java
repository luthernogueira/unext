package com.jacto.unext.web.rest;

import com.jacto.unext.repository.TipoCarteiraRepository;
import com.jacto.unext.service.TipoCarteiraService;
import com.jacto.unext.service.dto.TipoCarteiraDTO;
import com.jacto.unext.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jacto.unext.domain.TipoCarteira}.
 */
@RestController
@RequestMapping("/api")
public class TipoCarteiraResource {

    private final Logger log = LoggerFactory.getLogger(TipoCarteiraResource.class);

    private static final String ENTITY_NAME = "tipoCarteira";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoCarteiraService tipoCarteiraService;

    private final TipoCarteiraRepository tipoCarteiraRepository;

    public TipoCarteiraResource(TipoCarteiraService tipoCarteiraService, TipoCarteiraRepository tipoCarteiraRepository) {
        this.tipoCarteiraService = tipoCarteiraService;
        this.tipoCarteiraRepository = tipoCarteiraRepository;
    }

    /**
     * {@code POST  /tipo-carteiras} : Create a new tipoCarteira.
     *
     * @param tipoCarteiraDTO the tipoCarteiraDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipoCarteiraDTO, or with status {@code 400 (Bad Request)} if the tipoCarteira has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipo-carteiras")
    public ResponseEntity<TipoCarteiraDTO> createTipoCarteira(@Valid @RequestBody TipoCarteiraDTO tipoCarteiraDTO)
        throws URISyntaxException {
        log.debug("REST request to save TipoCarteira : {}", tipoCarteiraDTO);
        if (tipoCarteiraDTO.getId() != null) {
            throw new BadRequestAlertException("A new tipoCarteira cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoCarteiraDTO result = tipoCarteiraService.save(tipoCarteiraDTO);
        return ResponseEntity
            .created(new URI("/api/tipo-carteiras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipo-carteiras/:id} : Updates an existing tipoCarteira.
     *
     * @param id the id of the tipoCarteiraDTO to save.
     * @param tipoCarteiraDTO the tipoCarteiraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoCarteiraDTO,
     * or with status {@code 400 (Bad Request)} if the tipoCarteiraDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipoCarteiraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipo-carteiras/{id}")
    public ResponseEntity<TipoCarteiraDTO> updateTipoCarteira(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TipoCarteiraDTO tipoCarteiraDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TipoCarteira : {}, {}", id, tipoCarteiraDTO);
        if (tipoCarteiraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tipoCarteiraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tipoCarteiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TipoCarteiraDTO result = tipoCarteiraService.save(tipoCarteiraDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tipoCarteiraDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /tipo-carteiras/:id} : Partial updates given fields of an existing tipoCarteira, field will ignore if it is null
     *
     * @param id the id of the tipoCarteiraDTO to save.
     * @param tipoCarteiraDTO the tipoCarteiraDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipoCarteiraDTO,
     * or with status {@code 400 (Bad Request)} if the tipoCarteiraDTO is not valid,
     * or with status {@code 404 (Not Found)} if the tipoCarteiraDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the tipoCarteiraDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/tipo-carteiras/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TipoCarteiraDTO> partialUpdateTipoCarteira(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TipoCarteiraDTO tipoCarteiraDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TipoCarteira partially : {}, {}", id, tipoCarteiraDTO);
        if (tipoCarteiraDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, tipoCarteiraDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!tipoCarteiraRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TipoCarteiraDTO> result = tipoCarteiraService.partialUpdate(tipoCarteiraDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tipoCarteiraDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /tipo-carteiras} : get all the tipoCarteiras.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipoCarteiras in body.
     */
    @GetMapping("/tipo-carteiras")
    public List<TipoCarteiraDTO> getAllTipoCarteiras() {
        log.debug("REST request to get all TipoCarteiras");
        return tipoCarteiraService.findAll();
    }

    /**
     * {@code GET  /tipo-carteiras/:id} : get the "id" tipoCarteira.
     *
     * @param id the id of the tipoCarteiraDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipoCarteiraDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipo-carteiras/{id}")
    public ResponseEntity<TipoCarteiraDTO> getTipoCarteira(@PathVariable Long id) {
        log.debug("REST request to get TipoCarteira : {}", id);
        Optional<TipoCarteiraDTO> tipoCarteiraDTO = tipoCarteiraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipoCarteiraDTO);
    }

    /**
     * {@code DELETE  /tipo-carteiras/:id} : delete the "id" tipoCarteira.
     *
     * @param id the id of the tipoCarteiraDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipo-carteiras/{id}")
    public ResponseEntity<Void> deleteTipoCarteira(@PathVariable Long id) {
        log.debug("REST request to delete TipoCarteira : {}", id);
        tipoCarteiraService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
