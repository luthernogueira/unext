import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMaquinario } from '../maquinario.model';

@Component({
  selector: 'jhi-maquinario-detail',
  templateUrl: './maquinario-detail.component.html',
})
export class MaquinarioDetailComponent implements OnInit {
  maquinario: IMaquinario | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ maquinario }) => {
      this.maquinario = maquinario;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
