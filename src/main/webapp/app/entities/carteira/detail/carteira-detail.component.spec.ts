import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CarteiraDetailComponent } from './carteira-detail.component';

describe('Carteira Management Detail Component', () => {
  let comp: CarteiraDetailComponent;
  let fixture: ComponentFixture<CarteiraDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarteiraDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ carteira: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CarteiraDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CarteiraDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load carteira on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.carteira).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
