package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Carteira.
 */
@Entity
@Table(name = "carteira")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Carteira implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "titular", nullable = false)
    private String titular;

    @NotNull
    @Column(name = "numero_conta", nullable = false)
    private Integer numeroConta;

    @ManyToOne
    @JsonIgnoreProperties(value = { "carteiras" }, allowSetters = true)
    private TipoCarteira tipoCarteira;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "historicoLocacaos", "carteiras" }, allowSetters = true)
    private Cliente cliente;

    @ManyToOne
    @JsonIgnoreProperties(value = { "user", "historicoLocacaos", "carteiras" }, allowSetters = true)
    private Parceiro parceiro;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Carteira id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitular() {
        return this.titular;
    }

    public Carteira titular(String titular) {
        this.setTitular(titular);
        return this;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Integer getNumeroConta() {
        return this.numeroConta;
    }

    public Carteira numeroConta(Integer numeroConta) {
        this.setNumeroConta(numeroConta);
        return this;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public TipoCarteira getTipoCarteira() {
        return this.tipoCarteira;
    }

    public void setTipoCarteira(TipoCarteira tipoCarteira) {
        this.tipoCarteira = tipoCarteira;
    }

    public Carteira tipoCarteira(TipoCarteira tipoCarteira) {
        this.setTipoCarteira(tipoCarteira);
        return this;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Carteira cliente(Cliente cliente) {
        this.setCliente(cliente);
        return this;
    }

    public Parceiro getParceiro() {
        return this.parceiro;
    }

    public void setParceiro(Parceiro parceiro) {
        this.parceiro = parceiro;
    }

    public Carteira parceiro(Parceiro parceiro) {
        this.setParceiro(parceiro);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Carteira)) {
            return false;
        }
        return id != null && id.equals(((Carteira) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Carteira{" +
            "id=" + getId() +
            ", titular='" + getTitular() + "'" +
            ", numeroConta=" + getNumeroConta() +
            "}";
    }
}
