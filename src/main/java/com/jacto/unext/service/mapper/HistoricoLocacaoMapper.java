package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.HistoricoLocacao;
import com.jacto.unext.service.dto.HistoricoLocacaoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link HistoricoLocacao} and its DTO {@link HistoricoLocacaoDTO}.
 */
@Mapper(componentModel = "spring", uses = { ClienteMapper.class, ParceiroMapper.class })
public interface HistoricoLocacaoMapper extends EntityMapper<HistoricoLocacaoDTO, HistoricoLocacao> {
    @Mapping(target = "cliente", source = "cliente", qualifiedByName = "id")
    @Mapping(target = "parceiro", source = "parceiro", qualifiedByName = "id")
    HistoricoLocacaoDTO toDto(HistoricoLocacao s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    HistoricoLocacaoDTO toDtoId(HistoricoLocacao historicoLocacao);
}
