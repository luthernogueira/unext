import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITipoCarteira, getTipoCarteiraIdentifier } from '../tipo-carteira.model';

export type EntityResponseType = HttpResponse<ITipoCarteira>;
export type EntityArrayResponseType = HttpResponse<ITipoCarteira[]>;

@Injectable({ providedIn: 'root' })
export class TipoCarteiraService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/tipo-carteiras');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(tipoCarteira: ITipoCarteira): Observable<EntityResponseType> {
    return this.http.post<ITipoCarteira>(this.resourceUrl, tipoCarteira, { observe: 'response' });
  }

  update(tipoCarteira: ITipoCarteira): Observable<EntityResponseType> {
    return this.http.put<ITipoCarteira>(`${this.resourceUrl}/${getTipoCarteiraIdentifier(tipoCarteira) as number}`, tipoCarteira, {
      observe: 'response',
    });
  }

  partialUpdate(tipoCarteira: ITipoCarteira): Observable<EntityResponseType> {
    return this.http.patch<ITipoCarteira>(`${this.resourceUrl}/${getTipoCarteiraIdentifier(tipoCarteira) as number}`, tipoCarteira, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITipoCarteira>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITipoCarteira[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTipoCarteiraToCollectionIfMissing(
    tipoCarteiraCollection: ITipoCarteira[],
    ...tipoCarteirasToCheck: (ITipoCarteira | null | undefined)[]
  ): ITipoCarteira[] {
    const tipoCarteiras: ITipoCarteira[] = tipoCarteirasToCheck.filter(isPresent);
    if (tipoCarteiras.length > 0) {
      const tipoCarteiraCollectionIdentifiers = tipoCarteiraCollection.map(
        tipoCarteiraItem => getTipoCarteiraIdentifier(tipoCarteiraItem)!
      );
      const tipoCarteirasToAdd = tipoCarteiras.filter(tipoCarteiraItem => {
        const tipoCarteiraIdentifier = getTipoCarteiraIdentifier(tipoCarteiraItem);
        if (tipoCarteiraIdentifier == null || tipoCarteiraCollectionIdentifiers.includes(tipoCarteiraIdentifier)) {
          return false;
        }
        tipoCarteiraCollectionIdentifiers.push(tipoCarteiraIdentifier);
        return true;
      });
      return [...tipoCarteirasToAdd, ...tipoCarteiraCollection];
    }
    return tipoCarteiraCollection;
  }
}
