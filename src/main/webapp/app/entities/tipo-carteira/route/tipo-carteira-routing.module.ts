import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TipoCarteiraComponent } from '../list/tipo-carteira.component';
import { TipoCarteiraDetailComponent } from '../detail/tipo-carteira-detail.component';
import { TipoCarteiraUpdateComponent } from '../update/tipo-carteira-update.component';
import { TipoCarteiraRoutingResolveService } from './tipo-carteira-routing-resolve.service';

const tipoCarteiraRoute: Routes = [
  {
    path: '',
    component: TipoCarteiraComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TipoCarteiraDetailComponent,
    resolve: {
      tipoCarteira: TipoCarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TipoCarteiraUpdateComponent,
    resolve: {
      tipoCarteira: TipoCarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TipoCarteiraUpdateComponent,
    resolve: {
      tipoCarteira: TipoCarteiraRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(tipoCarteiraRoute)],
  exports: [RouterModule],
})
export class TipoCarteiraRoutingModule {}
