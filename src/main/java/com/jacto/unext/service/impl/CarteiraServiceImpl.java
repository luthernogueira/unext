package com.jacto.unext.service.impl;

import com.jacto.unext.domain.Carteira;
import com.jacto.unext.repository.CarteiraRepository;
import com.jacto.unext.service.CarteiraService;
import com.jacto.unext.service.dto.CarteiraDTO;
import com.jacto.unext.service.mapper.CarteiraMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Carteira}.
 */
@Service
@Transactional
public class CarteiraServiceImpl implements CarteiraService {

    private final Logger log = LoggerFactory.getLogger(CarteiraServiceImpl.class);

    private final CarteiraRepository carteiraRepository;

    private final CarteiraMapper carteiraMapper;

    public CarteiraServiceImpl(CarteiraRepository carteiraRepository, CarteiraMapper carteiraMapper) {
        this.carteiraRepository = carteiraRepository;
        this.carteiraMapper = carteiraMapper;
    }

    @Override
    public CarteiraDTO save(CarteiraDTO carteiraDTO) {
        log.debug("Request to save Carteira : {}", carteiraDTO);
        Carteira carteira = carteiraMapper.toEntity(carteiraDTO);
        carteira = carteiraRepository.save(carteira);
        return carteiraMapper.toDto(carteira);
    }

    @Override
    public Optional<CarteiraDTO> partialUpdate(CarteiraDTO carteiraDTO) {
        log.debug("Request to partially update Carteira : {}", carteiraDTO);

        return carteiraRepository
            .findById(carteiraDTO.getId())
            .map(existingCarteira -> {
                carteiraMapper.partialUpdate(existingCarteira, carteiraDTO);

                return existingCarteira;
            })
            .map(carteiraRepository::save)
            .map(carteiraMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CarteiraDTO> findAll() {
        log.debug("Request to get all Carteiras");
        return carteiraRepository.findAll().stream().map(carteiraMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CarteiraDTO> findOne(Long id) {
        log.debug("Request to get Carteira : {}", id);
        return carteiraRepository.findById(id).map(carteiraMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Carteira : {}", id);
        carteiraRepository.deleteById(id);
    }
}
