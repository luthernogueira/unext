package com.jacto.unext.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jacto.unext.domain.Carteira} entity.
 */
public class CarteiraDTO implements Serializable {

    private Long id;

    @NotNull
    private String titular;

    @NotNull
    private Integer numeroConta;

    private TipoCarteiraDTO tipoCarteira;

    private ClienteDTO cliente;

    private ParceiroDTO parceiro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public TipoCarteiraDTO getTipoCarteira() {
        return tipoCarteira;
    }

    public void setTipoCarteira(TipoCarteiraDTO tipoCarteira) {
        this.tipoCarteira = tipoCarteira;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public ParceiroDTO getParceiro() {
        return parceiro;
    }

    public void setParceiro(ParceiroDTO parceiro) {
        this.parceiro = parceiro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CarteiraDTO)) {
            return false;
        }

        CarteiraDTO carteiraDTO = (CarteiraDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, carteiraDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CarteiraDTO{" +
            "id=" + getId() +
            ", titular='" + getTitular() + "'" +
            ", numeroConta=" + getNumeroConta() +
            ", tipoCarteira=" + getTipoCarteira() +
            ", cliente=" + getCliente() +
            ", parceiro=" + getParceiro() +
            "}";
    }
}
