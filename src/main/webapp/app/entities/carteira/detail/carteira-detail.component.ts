import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICarteira } from '../carteira.model';

@Component({
  selector: 'jhi-carteira-detail',
  templateUrl: './carteira-detail.component.html',
})
export class CarteiraDetailComponent implements OnInit {
  carteira: ICarteira | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ carteira }) => {
      this.carteira = carteira;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
