package com.jacto.unext.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MaquinarioDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaquinarioDTO.class);
        MaquinarioDTO maquinarioDTO1 = new MaquinarioDTO();
        maquinarioDTO1.setId(1L);
        MaquinarioDTO maquinarioDTO2 = new MaquinarioDTO();
        assertThat(maquinarioDTO1).isNotEqualTo(maquinarioDTO2);
        maquinarioDTO2.setId(maquinarioDTO1.getId());
        assertThat(maquinarioDTO1).isEqualTo(maquinarioDTO2);
        maquinarioDTO2.setId(2L);
        assertThat(maquinarioDTO1).isNotEqualTo(maquinarioDTO2);
        maquinarioDTO1.setId(null);
        assertThat(maquinarioDTO1).isNotEqualTo(maquinarioDTO2);
    }
}
