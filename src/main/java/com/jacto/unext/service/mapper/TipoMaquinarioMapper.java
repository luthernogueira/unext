package com.jacto.unext.service.mapper;

import com.jacto.unext.domain.TipoMaquinario;
import com.jacto.unext.service.dto.TipoMaquinarioDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TipoMaquinario} and its DTO {@link TipoMaquinarioDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TipoMaquinarioMapper extends EntityMapper<TipoMaquinarioDTO, TipoMaquinario> {
    @Named("descricao")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "descricao", source = "descricao")
    TipoMaquinarioDTO toDtoDescricao(TipoMaquinario tipoMaquinario);
}
