import { Component } from '@angular/core';

@Component({
  selector: 'jhi-map-root',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent {
  texto = 'Jacto - Hackathon Grupo4';
  lat = -22.1080528;
  lng = -50.19947429170646;
  zoom = 15;
}
