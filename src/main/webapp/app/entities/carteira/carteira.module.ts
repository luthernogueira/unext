import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CarteiraComponent } from './list/carteira.component';
import { CarteiraDetailComponent } from './detail/carteira-detail.component';
import { CarteiraUpdateComponent } from './update/carteira-update.component';
import { CarteiraDeleteDialogComponent } from './delete/carteira-delete-dialog.component';
import { CarteiraRoutingModule } from './route/carteira-routing.module';

@NgModule({
  imports: [SharedModule, CarteiraRoutingModule],
  declarations: [CarteiraComponent, CarteiraDetailComponent, CarteiraUpdateComponent, CarteiraDeleteDialogComponent],
  entryComponents: [CarteiraDeleteDialogComponent],
})
export class CarteiraModule {}
