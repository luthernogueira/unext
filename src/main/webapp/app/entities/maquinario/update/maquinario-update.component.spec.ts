jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MaquinarioService } from '../service/maquinario.service';
import { IMaquinario, Maquinario } from '../maquinario.model';
import { IHistoricoLocacao } from 'app/entities/historico-locacao/historico-locacao.model';
import { HistoricoLocacaoService } from 'app/entities/historico-locacao/service/historico-locacao.service';
import { ITipoMaquinario } from 'app/entities/tipo-maquinario/tipo-maquinario.model';
import { TipoMaquinarioService } from 'app/entities/tipo-maquinario/service/tipo-maquinario.service';

import { MaquinarioUpdateComponent } from './maquinario-update.component';

describe('Maquinario Management Update Component', () => {
  let comp: MaquinarioUpdateComponent;
  let fixture: ComponentFixture<MaquinarioUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let maquinarioService: MaquinarioService;
  let historicoLocacaoService: HistoricoLocacaoService;
  let tipoMaquinarioService: TipoMaquinarioService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MaquinarioUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(MaquinarioUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MaquinarioUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    maquinarioService = TestBed.inject(MaquinarioService);
    historicoLocacaoService = TestBed.inject(HistoricoLocacaoService);
    tipoMaquinarioService = TestBed.inject(TipoMaquinarioService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call HistoricoLocacao query and add missing value', () => {
      const maquinario: IMaquinario = { id: 456 };
      const historicoLocacao: IHistoricoLocacao = { id: 72102 };
      maquinario.historicoLocacao = historicoLocacao;

      const historicoLocacaoCollection: IHistoricoLocacao[] = [{ id: 88594 }];
      jest.spyOn(historicoLocacaoService, 'query').mockReturnValue(of(new HttpResponse({ body: historicoLocacaoCollection })));
      const additionalHistoricoLocacaos = [historicoLocacao];
      const expectedCollection: IHistoricoLocacao[] = [...additionalHistoricoLocacaos, ...historicoLocacaoCollection];
      jest.spyOn(historicoLocacaoService, 'addHistoricoLocacaoToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      expect(historicoLocacaoService.query).toHaveBeenCalled();
      expect(historicoLocacaoService.addHistoricoLocacaoToCollectionIfMissing).toHaveBeenCalledWith(
        historicoLocacaoCollection,
        ...additionalHistoricoLocacaos
      );
      expect(comp.historicoLocacaosSharedCollection).toEqual(expectedCollection);
    });

    it('Should call TipoMaquinario query and add missing value', () => {
      const maquinario: IMaquinario = { id: 456 };
      const tipoMaquinario: ITipoMaquinario = { id: 78520 };
      maquinario.tipoMaquinario = tipoMaquinario;

      const tipoMaquinarioCollection: ITipoMaquinario[] = [{ id: 60079 }];
      jest.spyOn(tipoMaquinarioService, 'query').mockReturnValue(of(new HttpResponse({ body: tipoMaquinarioCollection })));
      const additionalTipoMaquinarios = [tipoMaquinario];
      const expectedCollection: ITipoMaquinario[] = [...additionalTipoMaquinarios, ...tipoMaquinarioCollection];
      jest.spyOn(tipoMaquinarioService, 'addTipoMaquinarioToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      expect(tipoMaquinarioService.query).toHaveBeenCalled();
      expect(tipoMaquinarioService.addTipoMaquinarioToCollectionIfMissing).toHaveBeenCalledWith(
        tipoMaquinarioCollection,
        ...additionalTipoMaquinarios
      );
      expect(comp.tipoMaquinariosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const maquinario: IMaquinario = { id: 456 };
      const historicoLocacao: IHistoricoLocacao = { id: 66350 };
      maquinario.historicoLocacao = historicoLocacao;
      const tipoMaquinario: ITipoMaquinario = { id: 34247 };
      maquinario.tipoMaquinario = tipoMaquinario;

      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(maquinario));
      expect(comp.historicoLocacaosSharedCollection).toContain(historicoLocacao);
      expect(comp.tipoMaquinariosSharedCollection).toContain(tipoMaquinario);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Maquinario>>();
      const maquinario = { id: 123 };
      jest.spyOn(maquinarioService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: maquinario }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(maquinarioService.update).toHaveBeenCalledWith(maquinario);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Maquinario>>();
      const maquinario = new Maquinario();
      jest.spyOn(maquinarioService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: maquinario }));
      saveSubject.complete();

      // THEN
      expect(maquinarioService.create).toHaveBeenCalledWith(maquinario);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Maquinario>>();
      const maquinario = { id: 123 };
      jest.spyOn(maquinarioService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ maquinario });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(maquinarioService.update).toHaveBeenCalledWith(maquinario);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackHistoricoLocacaoById', () => {
      it('Should return tracked HistoricoLocacao primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackHistoricoLocacaoById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackTipoMaquinarioById', () => {
      it('Should return tracked TipoMaquinario primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTipoMaquinarioById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
