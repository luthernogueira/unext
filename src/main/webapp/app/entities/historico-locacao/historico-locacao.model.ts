import * as dayjs from 'dayjs';
import { IMaquinario } from 'app/entities/maquinario/maquinario.model';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';

export interface IHistoricoLocacao {
  id?: number;
  data?: dayjs.Dayjs;
  tempoLocacao?: number;
  valorTotal?: number;
  fornecedor?: string;
  maquinarios?: IMaquinario[] | null;
  cliente?: ICliente | null;
  parceiro?: IParceiro | null;
}

export class HistoricoLocacao implements IHistoricoLocacao {
  constructor(
    public id?: number,
    public data?: dayjs.Dayjs,
    public tempoLocacao?: number,
    public valorTotal?: number,
    public fornecedor?: string,
    public maquinarios?: IMaquinario[] | null,
    public cliente?: ICliente | null,
    public parceiro?: IParceiro | null
  ) {}
}

export function getHistoricoLocacaoIdentifier(historicoLocacao: IHistoricoLocacao): number | undefined {
  return historicoLocacao.id;
}
