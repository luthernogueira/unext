import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITipoMaquinario, TipoMaquinario } from '../tipo-maquinario.model';
import { TipoMaquinarioService } from '../service/tipo-maquinario.service';

@Injectable({ providedIn: 'root' })
export class TipoMaquinarioRoutingResolveService implements Resolve<ITipoMaquinario> {
  constructor(protected service: TipoMaquinarioService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITipoMaquinario> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((tipoMaquinario: HttpResponse<TipoMaquinario>) => {
          if (tipoMaquinario.body) {
            return of(tipoMaquinario.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TipoMaquinario());
  }
}
