package com.jacto.unext.web.rest;

import com.jacto.unext.repository.ParceiroRepository;
import com.jacto.unext.service.ParceiroService;
import com.jacto.unext.service.dto.ParceiroDTO;
import com.jacto.unext.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jacto.unext.domain.Parceiro}.
 */
@RestController
@RequestMapping("/api")
public class ParceiroResource {

    private final Logger log = LoggerFactory.getLogger(ParceiroResource.class);

    private static final String ENTITY_NAME = "parceiro";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParceiroService parceiroService;

    private final ParceiroRepository parceiroRepository;

    public ParceiroResource(ParceiroService parceiroService, ParceiroRepository parceiroRepository) {
        this.parceiroService = parceiroService;
        this.parceiroRepository = parceiroRepository;
    }

    /**
     * {@code POST  /parceiros} : Create a new parceiro.
     *
     * @param parceiroDTO the parceiroDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new parceiroDTO, or with status {@code 400 (Bad Request)} if the parceiro has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/parceiros")
    public ResponseEntity<ParceiroDTO> createParceiro(@Valid @RequestBody ParceiroDTO parceiroDTO) throws URISyntaxException {
        log.debug("REST request to save Parceiro : {}", parceiroDTO);
        if (parceiroDTO.getId() != null) {
            throw new BadRequestAlertException("A new parceiro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParceiroDTO result = parceiroService.save(parceiroDTO);
        return ResponseEntity
            .created(new URI("/api/parceiros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /parceiros/:id} : Updates an existing parceiro.
     *
     * @param id the id of the parceiroDTO to save.
     * @param parceiroDTO the parceiroDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parceiroDTO,
     * or with status {@code 400 (Bad Request)} if the parceiroDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the parceiroDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/parceiros/{id}")
    public ResponseEntity<ParceiroDTO> updateParceiro(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ParceiroDTO parceiroDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Parceiro : {}, {}", id, parceiroDTO);
        if (parceiroDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parceiroDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parceiroRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ParceiroDTO result = parceiroService.save(parceiroDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parceiroDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /parceiros/:id} : Partial updates given fields of an existing parceiro, field will ignore if it is null
     *
     * @param id the id of the parceiroDTO to save.
     * @param parceiroDTO the parceiroDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parceiroDTO,
     * or with status {@code 400 (Bad Request)} if the parceiroDTO is not valid,
     * or with status {@code 404 (Not Found)} if the parceiroDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the parceiroDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/parceiros/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ParceiroDTO> partialUpdateParceiro(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ParceiroDTO parceiroDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Parceiro partially : {}, {}", id, parceiroDTO);
        if (parceiroDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parceiroDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parceiroRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ParceiroDTO> result = parceiroService.partialUpdate(parceiroDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parceiroDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /parceiros} : get all the parceiros.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of parceiros in body.
     */
    @GetMapping("/parceiros")
    public ResponseEntity<List<ParceiroDTO>> getAllParceiros(Pageable pageable) {
        log.debug("REST request to get a page of Parceiros");
        Page<ParceiroDTO> page = parceiroService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /parceiros/:id} : get the "id" parceiro.
     *
     * @param id the id of the parceiroDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the parceiroDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/parceiros/{id}")
    public ResponseEntity<ParceiroDTO> getParceiro(@PathVariable Long id) {
        log.debug("REST request to get Parceiro : {}", id);
        Optional<ParceiroDTO> parceiroDTO = parceiroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(parceiroDTO);
    }

    /**
     * {@code DELETE  /parceiros/:id} : delete the "id" parceiro.
     *
     * @param id the id of the parceiroDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/parceiros/{id}")
    public ResponseEntity<Void> deleteParceiro(@PathVariable Long id) {
        log.debug("REST request to delete Parceiro : {}", id);
        parceiroService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
