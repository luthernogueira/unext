import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICarteira, Carteira } from '../carteira.model';

import { CarteiraService } from './carteira.service';

describe('Carteira Service', () => {
  let service: CarteiraService;
  let httpMock: HttpTestingController;
  let elemDefault: ICarteira;
  let expectedResult: ICarteira | ICarteira[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CarteiraService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      titular: 'AAAAAAA',
      numeroConta: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Carteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Carteira()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Carteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          titular: 'BBBBBB',
          numeroConta: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Carteira', () => {
      const patchObject = Object.assign(
        {
          titular: 'BBBBBB',
          numeroConta: 1,
        },
        new Carteira()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Carteira', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          titular: 'BBBBBB',
          numeroConta: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Carteira', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCarteiraToCollectionIfMissing', () => {
      it('should add a Carteira to an empty array', () => {
        const carteira: ICarteira = { id: 123 };
        expectedResult = service.addCarteiraToCollectionIfMissing([], carteira);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(carteira);
      });

      it('should not add a Carteira to an array that contains it', () => {
        const carteira: ICarteira = { id: 123 };
        const carteiraCollection: ICarteira[] = [
          {
            ...carteira,
          },
          { id: 456 },
        ];
        expectedResult = service.addCarteiraToCollectionIfMissing(carteiraCollection, carteira);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Carteira to an array that doesn't contain it", () => {
        const carteira: ICarteira = { id: 123 };
        const carteiraCollection: ICarteira[] = [{ id: 456 }];
        expectedResult = service.addCarteiraToCollectionIfMissing(carteiraCollection, carteira);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(carteira);
      });

      it('should add only unique Carteira to an array', () => {
        const carteiraArray: ICarteira[] = [{ id: 123 }, { id: 456 }, { id: 51910 }];
        const carteiraCollection: ICarteira[] = [{ id: 123 }];
        expectedResult = service.addCarteiraToCollectionIfMissing(carteiraCollection, ...carteiraArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const carteira: ICarteira = { id: 123 };
        const carteira2: ICarteira = { id: 456 };
        expectedResult = service.addCarteiraToCollectionIfMissing([], carteira, carteira2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(carteira);
        expect(expectedResult).toContain(carteira2);
      });

      it('should accept null and undefined values', () => {
        const carteira: ICarteira = { id: 123 };
        expectedResult = service.addCarteiraToCollectionIfMissing([], null, carteira, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(carteira);
      });

      it('should return initial array if no Carteira is added', () => {
        const carteiraCollection: ICarteira[] = [{ id: 123 }];
        expectedResult = service.addCarteiraToCollectionIfMissing(carteiraCollection, undefined, null);
        expect(expectedResult).toEqual(carteiraCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
