package com.jacto.unext.service.impl;

import com.jacto.unext.domain.TipoMaquinario;
import com.jacto.unext.repository.TipoMaquinarioRepository;
import com.jacto.unext.service.TipoMaquinarioService;
import com.jacto.unext.service.dto.TipoMaquinarioDTO;
import com.jacto.unext.service.mapper.TipoMaquinarioMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TipoMaquinario}.
 */
@Service
@Transactional
public class TipoMaquinarioServiceImpl implements TipoMaquinarioService {

    private final Logger log = LoggerFactory.getLogger(TipoMaquinarioServiceImpl.class);

    private final TipoMaquinarioRepository tipoMaquinarioRepository;

    private final TipoMaquinarioMapper tipoMaquinarioMapper;

    public TipoMaquinarioServiceImpl(TipoMaquinarioRepository tipoMaquinarioRepository, TipoMaquinarioMapper tipoMaquinarioMapper) {
        this.tipoMaquinarioRepository = tipoMaquinarioRepository;
        this.tipoMaquinarioMapper = tipoMaquinarioMapper;
    }

    @Override
    public TipoMaquinarioDTO save(TipoMaquinarioDTO tipoMaquinarioDTO) {
        log.debug("Request to save TipoMaquinario : {}", tipoMaquinarioDTO);
        TipoMaquinario tipoMaquinario = tipoMaquinarioMapper.toEntity(tipoMaquinarioDTO);
        tipoMaquinario = tipoMaquinarioRepository.save(tipoMaquinario);
        return tipoMaquinarioMapper.toDto(tipoMaquinario);
    }

    @Override
    public Optional<TipoMaquinarioDTO> partialUpdate(TipoMaquinarioDTO tipoMaquinarioDTO) {
        log.debug("Request to partially update TipoMaquinario : {}", tipoMaquinarioDTO);

        return tipoMaquinarioRepository
            .findById(tipoMaquinarioDTO.getId())
            .map(existingTipoMaquinario -> {
                tipoMaquinarioMapper.partialUpdate(existingTipoMaquinario, tipoMaquinarioDTO);

                return existingTipoMaquinario;
            })
            .map(tipoMaquinarioRepository::save)
            .map(tipoMaquinarioMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoMaquinarioDTO> findAll() {
        log.debug("Request to get all TipoMaquinarios");
        return tipoMaquinarioRepository
            .findAll()
            .stream()
            .map(tipoMaquinarioMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TipoMaquinarioDTO> findOne(Long id) {
        log.debug("Request to get TipoMaquinario : {}", id);
        return tipoMaquinarioRepository.findById(id).map(tipoMaquinarioMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TipoMaquinario : {}", id);
        tipoMaquinarioRepository.deleteById(id);
    }
}
