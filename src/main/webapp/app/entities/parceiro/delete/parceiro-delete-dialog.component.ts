import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IParceiro } from '../parceiro.model';
import { ParceiroService } from '../service/parceiro.service';

@Component({
  templateUrl: './parceiro-delete-dialog.component.html',
})
export class ParceiroDeleteDialogComponent {
  parceiro?: IParceiro;

  constructor(protected parceiroService: ParceiroService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.parceiroService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
