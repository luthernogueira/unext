import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IHistoricoLocacao, getHistoricoLocacaoIdentifier } from '../historico-locacao.model';

export type EntityResponseType = HttpResponse<IHistoricoLocacao>;
export type EntityArrayResponseType = HttpResponse<IHistoricoLocacao[]>;

@Injectable({ providedIn: 'root' })
export class HistoricoLocacaoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/historico-locacaos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(historicoLocacao: IHistoricoLocacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historicoLocacao);
    return this.http
      .post<IHistoricoLocacao>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(historicoLocacao: IHistoricoLocacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historicoLocacao);
    return this.http
      .put<IHistoricoLocacao>(`${this.resourceUrl}/${getHistoricoLocacaoIdentifier(historicoLocacao) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(historicoLocacao: IHistoricoLocacao): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historicoLocacao);
    return this.http
      .patch<IHistoricoLocacao>(`${this.resourceUrl}/${getHistoricoLocacaoIdentifier(historicoLocacao) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHistoricoLocacao>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHistoricoLocacao[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addHistoricoLocacaoToCollectionIfMissing(
    historicoLocacaoCollection: IHistoricoLocacao[],
    ...historicoLocacaosToCheck: (IHistoricoLocacao | null | undefined)[]
  ): IHistoricoLocacao[] {
    const historicoLocacaos: IHistoricoLocacao[] = historicoLocacaosToCheck.filter(isPresent);
    if (historicoLocacaos.length > 0) {
      const historicoLocacaoCollectionIdentifiers = historicoLocacaoCollection.map(
        historicoLocacaoItem => getHistoricoLocacaoIdentifier(historicoLocacaoItem)!
      );
      const historicoLocacaosToAdd = historicoLocacaos.filter(historicoLocacaoItem => {
        const historicoLocacaoIdentifier = getHistoricoLocacaoIdentifier(historicoLocacaoItem);
        if (historicoLocacaoIdentifier == null || historicoLocacaoCollectionIdentifiers.includes(historicoLocacaoIdentifier)) {
          return false;
        }
        historicoLocacaoCollectionIdentifiers.push(historicoLocacaoIdentifier);
        return true;
      });
      return [...historicoLocacaosToAdd, ...historicoLocacaoCollection];
    }
    return historicoLocacaoCollection;
  }

  protected convertDateFromClient(historicoLocacao: IHistoricoLocacao): IHistoricoLocacao {
    return Object.assign({}, historicoLocacao, {
      data: historicoLocacao.data?.isValid() ? historicoLocacao.data.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.data = res.body.data ? dayjs(res.body.data) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((historicoLocacao: IHistoricoLocacao) => {
        historicoLocacao.data = historicoLocacao.data ? dayjs(historicoLocacao.data) : undefined;
      });
    }
    return res;
  }
}
