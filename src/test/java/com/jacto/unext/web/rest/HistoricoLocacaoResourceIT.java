package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.HistoricoLocacao;
import com.jacto.unext.repository.HistoricoLocacaoRepository;
import com.jacto.unext.service.dto.HistoricoLocacaoDTO;
import com.jacto.unext.service.mapper.HistoricoLocacaoMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HistoricoLocacaoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class HistoricoLocacaoResourceIT {

    private static final LocalDate DEFAULT_DATA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_TEMPO_LOCACAO = 1F;
    private static final Float UPDATED_TEMPO_LOCACAO = 2F;

    private static final Float DEFAULT_VALOR_TOTAL = 1F;
    private static final Float UPDATED_VALOR_TOTAL = 2F;

    private static final String DEFAULT_FORNECEDOR = "AAAAAAAAAA";
    private static final String UPDATED_FORNECEDOR = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/historico-locacaos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HistoricoLocacaoRepository historicoLocacaoRepository;

    @Autowired
    private HistoricoLocacaoMapper historicoLocacaoMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHistoricoLocacaoMockMvc;

    private HistoricoLocacao historicoLocacao;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistoricoLocacao createEntity(EntityManager em) {
        HistoricoLocacao historicoLocacao = new HistoricoLocacao()
            .data(DEFAULT_DATA)
            .tempoLocacao(DEFAULT_TEMPO_LOCACAO)
            .valorTotal(DEFAULT_VALOR_TOTAL)
            .fornecedor(DEFAULT_FORNECEDOR);
        return historicoLocacao;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static HistoricoLocacao createUpdatedEntity(EntityManager em) {
        HistoricoLocacao historicoLocacao = new HistoricoLocacao()
            .data(UPDATED_DATA)
            .tempoLocacao(UPDATED_TEMPO_LOCACAO)
            .valorTotal(UPDATED_VALOR_TOTAL)
            .fornecedor(UPDATED_FORNECEDOR);
        return historicoLocacao;
    }

    @BeforeEach
    public void initTest() {
        historicoLocacao = createEntity(em);
    }

    @Test
    @Transactional
    void createHistoricoLocacao() throws Exception {
        int databaseSizeBeforeCreate = historicoLocacaoRepository.findAll().size();
        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);
        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isCreated());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeCreate + 1);
        HistoricoLocacao testHistoricoLocacao = historicoLocacaoList.get(historicoLocacaoList.size() - 1);
        assertThat(testHistoricoLocacao.getData()).isEqualTo(DEFAULT_DATA);
        assertThat(testHistoricoLocacao.getTempoLocacao()).isEqualTo(DEFAULT_TEMPO_LOCACAO);
        assertThat(testHistoricoLocacao.getValorTotal()).isEqualTo(DEFAULT_VALOR_TOTAL);
        assertThat(testHistoricoLocacao.getFornecedor()).isEqualTo(DEFAULT_FORNECEDOR);
    }

    @Test
    @Transactional
    void createHistoricoLocacaoWithExistingId() throws Exception {
        // Create the HistoricoLocacao with an existing ID
        historicoLocacao.setId(1L);
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        int databaseSizeBeforeCreate = historicoLocacaoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoLocacaoRepository.findAll().size();
        // set the field null
        historicoLocacao.setData(null);

        // Create the HistoricoLocacao, which fails.
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTempoLocacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoLocacaoRepository.findAll().size();
        // set the field null
        historicoLocacao.setTempoLocacao(null);

        // Create the HistoricoLocacao, which fails.
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkValorTotalIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoLocacaoRepository.findAll().size();
        // set the field null
        historicoLocacao.setValorTotal(null);

        // Create the HistoricoLocacao, which fails.
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFornecedorIsRequired() throws Exception {
        int databaseSizeBeforeTest = historicoLocacaoRepository.findAll().size();
        // set the field null
        historicoLocacao.setFornecedor(null);

        // Create the HistoricoLocacao, which fails.
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        restHistoricoLocacaoMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllHistoricoLocacaos() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        // Get all the historicoLocacaoList
        restHistoricoLocacaoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(historicoLocacao.getId().intValue())))
            .andExpect(jsonPath("$.[*].data").value(hasItem(DEFAULT_DATA.toString())))
            .andExpect(jsonPath("$.[*].tempoLocacao").value(hasItem(DEFAULT_TEMPO_LOCACAO.doubleValue())))
            .andExpect(jsonPath("$.[*].valorTotal").value(hasItem(DEFAULT_VALOR_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].fornecedor").value(hasItem(DEFAULT_FORNECEDOR)));
    }

    @Test
    @Transactional
    void getHistoricoLocacao() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        // Get the historicoLocacao
        restHistoricoLocacaoMockMvc
            .perform(get(ENTITY_API_URL_ID, historicoLocacao.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(historicoLocacao.getId().intValue()))
            .andExpect(jsonPath("$.data").value(DEFAULT_DATA.toString()))
            .andExpect(jsonPath("$.tempoLocacao").value(DEFAULT_TEMPO_LOCACAO.doubleValue()))
            .andExpect(jsonPath("$.valorTotal").value(DEFAULT_VALOR_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.fornecedor").value(DEFAULT_FORNECEDOR));
    }

    @Test
    @Transactional
    void getNonExistingHistoricoLocacao() throws Exception {
        // Get the historicoLocacao
        restHistoricoLocacaoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHistoricoLocacao() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();

        // Update the historicoLocacao
        HistoricoLocacao updatedHistoricoLocacao = historicoLocacaoRepository.findById(historicoLocacao.getId()).get();
        // Disconnect from session so that the updates on updatedHistoricoLocacao are not directly saved in db
        em.detach(updatedHistoricoLocacao);
        updatedHistoricoLocacao
            .data(UPDATED_DATA)
            .tempoLocacao(UPDATED_TEMPO_LOCACAO)
            .valorTotal(UPDATED_VALOR_TOTAL)
            .fornecedor(UPDATED_FORNECEDOR);
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(updatedHistoricoLocacao);

        restHistoricoLocacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, historicoLocacaoDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isOk());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
        HistoricoLocacao testHistoricoLocacao = historicoLocacaoList.get(historicoLocacaoList.size() - 1);
        assertThat(testHistoricoLocacao.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testHistoricoLocacao.getTempoLocacao()).isEqualTo(UPDATED_TEMPO_LOCACAO);
        assertThat(testHistoricoLocacao.getValorTotal()).isEqualTo(UPDATED_VALOR_TOTAL);
        assertThat(testHistoricoLocacao.getFornecedor()).isEqualTo(UPDATED_FORNECEDOR);
    }

    @Test
    @Transactional
    void putNonExistingHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, historicoLocacaoDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHistoricoLocacaoWithPatch() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();

        // Update the historicoLocacao using partial update
        HistoricoLocacao partialUpdatedHistoricoLocacao = new HistoricoLocacao();
        partialUpdatedHistoricoLocacao.setId(historicoLocacao.getId());

        partialUpdatedHistoricoLocacao
            .data(UPDATED_DATA)
            .tempoLocacao(UPDATED_TEMPO_LOCACAO)
            .valorTotal(UPDATED_VALOR_TOTAL)
            .fornecedor(UPDATED_FORNECEDOR);

        restHistoricoLocacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHistoricoLocacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHistoricoLocacao))
            )
            .andExpect(status().isOk());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
        HistoricoLocacao testHistoricoLocacao = historicoLocacaoList.get(historicoLocacaoList.size() - 1);
        assertThat(testHistoricoLocacao.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testHistoricoLocacao.getTempoLocacao()).isEqualTo(UPDATED_TEMPO_LOCACAO);
        assertThat(testHistoricoLocacao.getValorTotal()).isEqualTo(UPDATED_VALOR_TOTAL);
        assertThat(testHistoricoLocacao.getFornecedor()).isEqualTo(UPDATED_FORNECEDOR);
    }

    @Test
    @Transactional
    void fullUpdateHistoricoLocacaoWithPatch() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();

        // Update the historicoLocacao using partial update
        HistoricoLocacao partialUpdatedHistoricoLocacao = new HistoricoLocacao();
        partialUpdatedHistoricoLocacao.setId(historicoLocacao.getId());

        partialUpdatedHistoricoLocacao
            .data(UPDATED_DATA)
            .tempoLocacao(UPDATED_TEMPO_LOCACAO)
            .valorTotal(UPDATED_VALOR_TOTAL)
            .fornecedor(UPDATED_FORNECEDOR);

        restHistoricoLocacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHistoricoLocacao.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHistoricoLocacao))
            )
            .andExpect(status().isOk());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
        HistoricoLocacao testHistoricoLocacao = historicoLocacaoList.get(historicoLocacaoList.size() - 1);
        assertThat(testHistoricoLocacao.getData()).isEqualTo(UPDATED_DATA);
        assertThat(testHistoricoLocacao.getTempoLocacao()).isEqualTo(UPDATED_TEMPO_LOCACAO);
        assertThat(testHistoricoLocacao.getValorTotal()).isEqualTo(UPDATED_VALOR_TOTAL);
        assertThat(testHistoricoLocacao.getFornecedor()).isEqualTo(UPDATED_FORNECEDOR);
    }

    @Test
    @Transactional
    void patchNonExistingHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, historicoLocacaoDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHistoricoLocacao() throws Exception {
        int databaseSizeBeforeUpdate = historicoLocacaoRepository.findAll().size();
        historicoLocacao.setId(count.incrementAndGet());

        // Create the HistoricoLocacao
        HistoricoLocacaoDTO historicoLocacaoDTO = historicoLocacaoMapper.toDto(historicoLocacao);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHistoricoLocacaoMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(historicoLocacaoDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the HistoricoLocacao in the database
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHistoricoLocacao() throws Exception {
        // Initialize the database
        historicoLocacaoRepository.saveAndFlush(historicoLocacao);

        int databaseSizeBeforeDelete = historicoLocacaoRepository.findAll().size();

        // Delete the historicoLocacao
        restHistoricoLocacaoMockMvc
            .perform(delete(ENTITY_API_URL_ID, historicoLocacao.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<HistoricoLocacao> historicoLocacaoList = historicoLocacaoRepository.findAll();
        assertThat(historicoLocacaoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
