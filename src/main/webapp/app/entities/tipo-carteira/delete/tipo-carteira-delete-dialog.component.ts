import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITipoCarteira } from '../tipo-carteira.model';
import { TipoCarteiraService } from '../service/tipo-carteira.service';

@Component({
  templateUrl: './tipo-carteira-delete-dialog.component.html',
})
export class TipoCarteiraDeleteDialogComponent {
  tipoCarteira?: ITipoCarteira;

  constructor(protected tipoCarteiraService: TipoCarteiraService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tipoCarteiraService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
