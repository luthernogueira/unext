package com.jacto.unext.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jacto.unext.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class TipoCarteiraDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoCarteiraDTO.class);
        TipoCarteiraDTO tipoCarteiraDTO1 = new TipoCarteiraDTO();
        tipoCarteiraDTO1.setId(1L);
        TipoCarteiraDTO tipoCarteiraDTO2 = new TipoCarteiraDTO();
        assertThat(tipoCarteiraDTO1).isNotEqualTo(tipoCarteiraDTO2);
        tipoCarteiraDTO2.setId(tipoCarteiraDTO1.getId());
        assertThat(tipoCarteiraDTO1).isEqualTo(tipoCarteiraDTO2);
        tipoCarteiraDTO2.setId(2L);
        assertThat(tipoCarteiraDTO1).isNotEqualTo(tipoCarteiraDTO2);
        tipoCarteiraDTO1.setId(null);
        assertThat(tipoCarteiraDTO1).isNotEqualTo(tipoCarteiraDTO2);
    }
}
