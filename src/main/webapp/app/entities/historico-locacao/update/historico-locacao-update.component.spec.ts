jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { HistoricoLocacaoService } from '../service/historico-locacao.service';
import { IHistoricoLocacao, HistoricoLocacao } from '../historico-locacao.model';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { IParceiro } from 'app/entities/parceiro/parceiro.model';
import { ParceiroService } from 'app/entities/parceiro/service/parceiro.service';

import { HistoricoLocacaoUpdateComponent } from './historico-locacao-update.component';

describe('HistoricoLocacao Management Update Component', () => {
  let comp: HistoricoLocacaoUpdateComponent;
  let fixture: ComponentFixture<HistoricoLocacaoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let historicoLocacaoService: HistoricoLocacaoService;
  let clienteService: ClienteService;
  let parceiroService: ParceiroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HistoricoLocacaoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(HistoricoLocacaoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HistoricoLocacaoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    historicoLocacaoService = TestBed.inject(HistoricoLocacaoService);
    clienteService = TestBed.inject(ClienteService);
    parceiroService = TestBed.inject(ParceiroService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Cliente query and add missing value', () => {
      const historicoLocacao: IHistoricoLocacao = { id: 456 };
      const cliente: ICliente = { id: 31942 };
      historicoLocacao.cliente = cliente;

      const clienteCollection: ICliente[] = [{ id: 63550 }];
      jest.spyOn(clienteService, 'query').mockReturnValue(of(new HttpResponse({ body: clienteCollection })));
      const additionalClientes = [cliente];
      const expectedCollection: ICliente[] = [...additionalClientes, ...clienteCollection];
      jest.spyOn(clienteService, 'addClienteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      expect(clienteService.query).toHaveBeenCalled();
      expect(clienteService.addClienteToCollectionIfMissing).toHaveBeenCalledWith(clienteCollection, ...additionalClientes);
      expect(comp.clientesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Parceiro query and add missing value', () => {
      const historicoLocacao: IHistoricoLocacao = { id: 456 };
      const parceiro: IParceiro = { id: 58767 };
      historicoLocacao.parceiro = parceiro;

      const parceiroCollection: IParceiro[] = [{ id: 29563 }];
      jest.spyOn(parceiroService, 'query').mockReturnValue(of(new HttpResponse({ body: parceiroCollection })));
      const additionalParceiros = [parceiro];
      const expectedCollection: IParceiro[] = [...additionalParceiros, ...parceiroCollection];
      jest.spyOn(parceiroService, 'addParceiroToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      expect(parceiroService.query).toHaveBeenCalled();
      expect(parceiroService.addParceiroToCollectionIfMissing).toHaveBeenCalledWith(parceiroCollection, ...additionalParceiros);
      expect(comp.parceirosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const historicoLocacao: IHistoricoLocacao = { id: 456 };
      const cliente: ICliente = { id: 36849 };
      historicoLocacao.cliente = cliente;
      const parceiro: IParceiro = { id: 19846 };
      historicoLocacao.parceiro = parceiro;

      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(historicoLocacao));
      expect(comp.clientesSharedCollection).toContain(cliente);
      expect(comp.parceirosSharedCollection).toContain(parceiro);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<HistoricoLocacao>>();
      const historicoLocacao = { id: 123 };
      jest.spyOn(historicoLocacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: historicoLocacao }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(historicoLocacaoService.update).toHaveBeenCalledWith(historicoLocacao);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<HistoricoLocacao>>();
      const historicoLocacao = new HistoricoLocacao();
      jest.spyOn(historicoLocacaoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: historicoLocacao }));
      saveSubject.complete();

      // THEN
      expect(historicoLocacaoService.create).toHaveBeenCalledWith(historicoLocacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<HistoricoLocacao>>();
      const historicoLocacao = { id: 123 };
      jest.spyOn(historicoLocacaoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ historicoLocacao });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(historicoLocacaoService.update).toHaveBeenCalledWith(historicoLocacao);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackClienteById', () => {
      it('Should return tracked Cliente primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackClienteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackParceiroById', () => {
      it('Should return tracked Parceiro primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackParceiroById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
