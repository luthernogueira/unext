import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { HistoricoLocacaoComponent } from './list/historico-locacao.component';
import { HistoricoLocacaoDetailComponent } from './detail/historico-locacao-detail.component';
import { HistoricoLocacaoUpdateComponent } from './update/historico-locacao-update.component';
import { HistoricoLocacaoDeleteDialogComponent } from './delete/historico-locacao-delete-dialog.component';
import { HistoricoLocacaoRoutingModule } from './route/historico-locacao-routing.module';

@NgModule({
  imports: [SharedModule, HistoricoLocacaoRoutingModule],
  declarations: [
    HistoricoLocacaoComponent,
    HistoricoLocacaoDetailComponent,
    HistoricoLocacaoUpdateComponent,
    HistoricoLocacaoDeleteDialogComponent,
  ],
  entryComponents: [HistoricoLocacaoDeleteDialogComponent],
})
export class HistoricoLocacaoModule {}
