import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HistoricoLocacaoDetailComponent } from './historico-locacao-detail.component';

describe('HistoricoLocacao Management Detail Component', () => {
  let comp: HistoricoLocacaoDetailComponent;
  let fixture: ComponentFixture<HistoricoLocacaoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HistoricoLocacaoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ historicoLocacao: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(HistoricoLocacaoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(HistoricoLocacaoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load historicoLocacao on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.historicoLocacao).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
