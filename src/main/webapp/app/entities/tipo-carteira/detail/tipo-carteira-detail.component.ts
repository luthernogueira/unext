import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITipoCarteira } from '../tipo-carteira.model';

@Component({
  selector: 'jhi-tipo-carteira-detail',
  templateUrl: './tipo-carteira-detail.component.html',
})
export class TipoCarteiraDetailComponent implements OnInit {
  tipoCarteira: ITipoCarteira | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tipoCarteira }) => {
      this.tipoCarteira = tipoCarteira;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
