package com.jacto.unext.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Cliente.
 */
@Entity
@Table(name = "cliente")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotNull
    @Column(name = "sobrenome", nullable = false)
    private String sobrenome;

    @NotNull
    @Column(name = "cpfcnpj", nullable = false, unique = true)
    private String cpfcnpj;

    @NotNull
    @Column(name = "tefelone", nullable = false)
    private Integer tefelone;

    @Column(name = "email")
    private String email;

    @Column(name = "rua")
    private String rua;

    @Column(name = "numero")
    private Integer numero;

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "cep")
    private String cep;

    @Column(name = "cidade")
    private String cidade;

    @Column(name = "estado")
    private String estado;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "cliente")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "maquinarios", "cliente", "parceiro" }, allowSetters = true)
    private Set<HistoricoLocacao> historicoLocacaos = new HashSet<>();

    @OneToMany(mappedBy = "cliente")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tipoCarteira", "cliente", "parceiro" }, allowSetters = true)
    private Set<Carteira> carteiras = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Cliente id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public Cliente nome(String nome) {
        this.setNome(nome);
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return this.sobrenome;
    }

    public Cliente sobrenome(String sobrenome) {
        this.setSobrenome(sobrenome);
        return this;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCpfcnpj() {
        return this.cpfcnpj;
    }

    public Cliente cpfcnpj(String cpfcnpj) {
        this.setCpfcnpj(cpfcnpj);
        return this;
    }

    public void setCpfcnpj(String cpfcnpj) {
        this.cpfcnpj = cpfcnpj;
    }

    public Integer getTefelone() {
        return this.tefelone;
    }

    public Cliente tefelone(Integer tefelone) {
        this.setTefelone(tefelone);
        return this;
    }

    public void setTefelone(Integer tefelone) {
        this.tefelone = tefelone;
    }

    public String getEmail() {
        return this.email;
    }

    public Cliente email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRua() {
        return this.rua;
    }

    public Cliente rua(String rua) {
        this.setRua(rua);
        return this;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Cliente numero(Integer numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return this.bairro;
    }

    public Cliente bairro(String bairro) {
        this.setBairro(bairro);
        return this;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return this.cep;
    }

    public Cliente cep(String cep) {
        this.setCep(cep);
        return this;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return this.cidade;
    }

    public Cliente cidade(String cidade) {
        this.setCidade(cidade);
        return this;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return this.estado;
    }

    public Cliente estado(String estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cliente user(User user) {
        this.setUser(user);
        return this;
    }

    public Set<HistoricoLocacao> getHistoricoLocacaos() {
        return this.historicoLocacaos;
    }

    public void setHistoricoLocacaos(Set<HistoricoLocacao> historicoLocacaos) {
        if (this.historicoLocacaos != null) {
            this.historicoLocacaos.forEach(i -> i.setCliente(null));
        }
        if (historicoLocacaos != null) {
            historicoLocacaos.forEach(i -> i.setCliente(this));
        }
        this.historicoLocacaos = historicoLocacaos;
    }

    public Cliente historicoLocacaos(Set<HistoricoLocacao> historicoLocacaos) {
        this.setHistoricoLocacaos(historicoLocacaos);
        return this;
    }

    public Cliente addHistoricoLocacao(HistoricoLocacao historicoLocacao) {
        this.historicoLocacaos.add(historicoLocacao);
        historicoLocacao.setCliente(this);
        return this;
    }

    public Cliente removeHistoricoLocacao(HistoricoLocacao historicoLocacao) {
        this.historicoLocacaos.remove(historicoLocacao);
        historicoLocacao.setCliente(null);
        return this;
    }

    public Set<Carteira> getCarteiras() {
        return this.carteiras;
    }

    public void setCarteiras(Set<Carteira> carteiras) {
        if (this.carteiras != null) {
            this.carteiras.forEach(i -> i.setCliente(null));
        }
        if (carteiras != null) {
            carteiras.forEach(i -> i.setCliente(this));
        }
        this.carteiras = carteiras;
    }

    public Cliente carteiras(Set<Carteira> carteiras) {
        this.setCarteiras(carteiras);
        return this;
    }

    public Cliente addCarteira(Carteira carteira) {
        this.carteiras.add(carteira);
        carteira.setCliente(this);
        return this;
    }

    public Cliente removeCarteira(Carteira carteira) {
        this.carteiras.remove(carteira);
        carteira.setCliente(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cliente)) {
            return false;
        }
        return id != null && id.equals(((Cliente) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cliente{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", sobrenome='" + getSobrenome() + "'" +
            ", cpfcnpj='" + getCpfcnpj() + "'" +
            ", tefelone=" + getTefelone() +
            ", email='" + getEmail() + "'" +
            ", rua='" + getRua() + "'" +
            ", numero=" + getNumero() +
            ", bairro='" + getBairro() + "'" +
            ", cep='" + getCep() + "'" +
            ", cidade='" + getCidade() + "'" +
            ", estado='" + getEstado() + "'" +
            "}";
    }
}
