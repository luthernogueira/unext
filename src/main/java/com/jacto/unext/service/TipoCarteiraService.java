package com.jacto.unext.service;

import com.jacto.unext.service.dto.TipoCarteiraDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.jacto.unext.domain.TipoCarteira}.
 */
public interface TipoCarteiraService {
    /**
     * Save a tipoCarteira.
     *
     * @param tipoCarteiraDTO the entity to save.
     * @return the persisted entity.
     */
    TipoCarteiraDTO save(TipoCarteiraDTO tipoCarteiraDTO);

    /**
     * Partially updates a tipoCarteira.
     *
     * @param tipoCarteiraDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TipoCarteiraDTO> partialUpdate(TipoCarteiraDTO tipoCarteiraDTO);

    /**
     * Get all the tipoCarteiras.
     *
     * @return the list of entities.
     */
    List<TipoCarteiraDTO> findAll();

    /**
     * Get the "id" tipoCarteira.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TipoCarteiraDTO> findOne(Long id);

    /**
     * Delete the "id" tipoCarteira.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
