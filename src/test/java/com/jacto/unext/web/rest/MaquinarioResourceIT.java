package com.jacto.unext.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jacto.unext.IntegrationTest;
import com.jacto.unext.domain.Maquinario;
import com.jacto.unext.repository.MaquinarioRepository;
import com.jacto.unext.service.dto.MaquinarioDTO;
import com.jacto.unext.service.mapper.MaquinarioMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MaquinarioResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MaquinarioResourceIT {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_MODELO = "AAAAAAAAAA";
    private static final String UPDATED_MODELO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ANO_FABRICACAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ANO_FABRICACAO = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_CAPACIDADE_LITRO = 1F;
    private static final Float UPDATED_CAPACIDADE_LITRO = 2F;

    private static final Float DEFAULT_VOLUME_MINUTO = 1F;
    private static final Float UPDATED_VOLUME_MINUTO = 2F;

    private static final Long DEFAULT_DESCRICAO = 1L;
    private static final Long UPDATED_DESCRICAO = 2L;

    private static final String ENTITY_API_URL = "/api/maquinarios";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MaquinarioRepository maquinarioRepository;

    @Autowired
    private MaquinarioMapper maquinarioMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMaquinarioMockMvc;

    private Maquinario maquinario;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Maquinario createEntity(EntityManager em) {
        Maquinario maquinario = new Maquinario()
            .nome(DEFAULT_NOME)
            .modelo(DEFAULT_MODELO)
            .anoFabricacao(DEFAULT_ANO_FABRICACAO)
            .capacidadeLitro(DEFAULT_CAPACIDADE_LITRO)
            .volumeMinuto(DEFAULT_VOLUME_MINUTO)
            .descricao(DEFAULT_DESCRICAO);
        return maquinario;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Maquinario createUpdatedEntity(EntityManager em) {
        Maquinario maquinario = new Maquinario()
            .nome(UPDATED_NOME)
            .modelo(UPDATED_MODELO)
            .anoFabricacao(UPDATED_ANO_FABRICACAO)
            .capacidadeLitro(UPDATED_CAPACIDADE_LITRO)
            .volumeMinuto(UPDATED_VOLUME_MINUTO)
            .descricao(UPDATED_DESCRICAO);
        return maquinario;
    }

    @BeforeEach
    public void initTest() {
        maquinario = createEntity(em);
    }

    @Test
    @Transactional
    void createMaquinario() throws Exception {
        int databaseSizeBeforeCreate = maquinarioRepository.findAll().size();
        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);
        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isCreated());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeCreate + 1);
        Maquinario testMaquinario = maquinarioList.get(maquinarioList.size() - 1);
        assertThat(testMaquinario.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testMaquinario.getModelo()).isEqualTo(DEFAULT_MODELO);
        assertThat(testMaquinario.getAnoFabricacao()).isEqualTo(DEFAULT_ANO_FABRICACAO);
        assertThat(testMaquinario.getCapacidadeLitro()).isEqualTo(DEFAULT_CAPACIDADE_LITRO);
        assertThat(testMaquinario.getVolumeMinuto()).isEqualTo(DEFAULT_VOLUME_MINUTO);
        assertThat(testMaquinario.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void createMaquinarioWithExistingId() throws Exception {
        // Create the Maquinario with an existing ID
        maquinario.setId(1L);
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        int databaseSizeBeforeCreate = maquinarioRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeIsRequired() throws Exception {
        int databaseSizeBeforeTest = maquinarioRepository.findAll().size();
        // set the field null
        maquinario.setNome(null);

        // Create the Maquinario, which fails.
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkModeloIsRequired() throws Exception {
        int databaseSizeBeforeTest = maquinarioRepository.findAll().size();
        // set the field null
        maquinario.setModelo(null);

        // Create the Maquinario, which fails.
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAnoFabricacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = maquinarioRepository.findAll().size();
        // set the field null
        maquinario.setAnoFabricacao(null);

        // Create the Maquinario, which fails.
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCapacidadeLitroIsRequired() throws Exception {
        int databaseSizeBeforeTest = maquinarioRepository.findAll().size();
        // set the field null
        maquinario.setCapacidadeLitro(null);

        // Create the Maquinario, which fails.
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkVolumeMinutoIsRequired() throws Exception {
        int databaseSizeBeforeTest = maquinarioRepository.findAll().size();
        // set the field null
        maquinario.setVolumeMinuto(null);

        // Create the Maquinario, which fails.
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        restMaquinarioMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isBadRequest());

        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMaquinarios() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        // Get all the maquinarioList
        restMaquinarioMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(maquinario.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME)))
            .andExpect(jsonPath("$.[*].modelo").value(hasItem(DEFAULT_MODELO)))
            .andExpect(jsonPath("$.[*].anoFabricacao").value(hasItem(DEFAULT_ANO_FABRICACAO.toString())))
            .andExpect(jsonPath("$.[*].capacidadeLitro").value(hasItem(DEFAULT_CAPACIDADE_LITRO.doubleValue())))
            .andExpect(jsonPath("$.[*].volumeMinuto").value(hasItem(DEFAULT_VOLUME_MINUTO.doubleValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.intValue())));
    }

    @Test
    @Transactional
    void getMaquinario() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        // Get the maquinario
        restMaquinarioMockMvc
            .perform(get(ENTITY_API_URL_ID, maquinario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(maquinario.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME))
            .andExpect(jsonPath("$.modelo").value(DEFAULT_MODELO))
            .andExpect(jsonPath("$.anoFabricacao").value(DEFAULT_ANO_FABRICACAO.toString()))
            .andExpect(jsonPath("$.capacidadeLitro").value(DEFAULT_CAPACIDADE_LITRO.doubleValue()))
            .andExpect(jsonPath("$.volumeMinuto").value(DEFAULT_VOLUME_MINUTO.doubleValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingMaquinario() throws Exception {
        // Get the maquinario
        restMaquinarioMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMaquinario() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();

        // Update the maquinario
        Maquinario updatedMaquinario = maquinarioRepository.findById(maquinario.getId()).get();
        // Disconnect from session so that the updates on updatedMaquinario are not directly saved in db
        em.detach(updatedMaquinario);
        updatedMaquinario
            .nome(UPDATED_NOME)
            .modelo(UPDATED_MODELO)
            .anoFabricacao(UPDATED_ANO_FABRICACAO)
            .capacidadeLitro(UPDATED_CAPACIDADE_LITRO)
            .volumeMinuto(UPDATED_VOLUME_MINUTO)
            .descricao(UPDATED_DESCRICAO);
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(updatedMaquinario);

        restMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, maquinarioDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isOk());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
        Maquinario testMaquinario = maquinarioList.get(maquinarioList.size() - 1);
        assertThat(testMaquinario.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testMaquinario.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testMaquinario.getAnoFabricacao()).isEqualTo(UPDATED_ANO_FABRICACAO);
        assertThat(testMaquinario.getCapacidadeLitro()).isEqualTo(UPDATED_CAPACIDADE_LITRO);
        assertThat(testMaquinario.getVolumeMinuto()).isEqualTo(UPDATED_VOLUME_MINUTO);
        assertThat(testMaquinario.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void putNonExistingMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, maquinarioDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(maquinarioDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMaquinarioWithPatch() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();

        // Update the maquinario using partial update
        Maquinario partialUpdatedMaquinario = new Maquinario();
        partialUpdatedMaquinario.setId(maquinario.getId());

        partialUpdatedMaquinario.anoFabricacao(UPDATED_ANO_FABRICACAO).capacidadeLitro(UPDATED_CAPACIDADE_LITRO);

        restMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMaquinario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMaquinario))
            )
            .andExpect(status().isOk());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
        Maquinario testMaquinario = maquinarioList.get(maquinarioList.size() - 1);
        assertThat(testMaquinario.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testMaquinario.getModelo()).isEqualTo(DEFAULT_MODELO);
        assertThat(testMaquinario.getAnoFabricacao()).isEqualTo(UPDATED_ANO_FABRICACAO);
        assertThat(testMaquinario.getCapacidadeLitro()).isEqualTo(UPDATED_CAPACIDADE_LITRO);
        assertThat(testMaquinario.getVolumeMinuto()).isEqualTo(DEFAULT_VOLUME_MINUTO);
        assertThat(testMaquinario.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    void fullUpdateMaquinarioWithPatch() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();

        // Update the maquinario using partial update
        Maquinario partialUpdatedMaquinario = new Maquinario();
        partialUpdatedMaquinario.setId(maquinario.getId());

        partialUpdatedMaquinario
            .nome(UPDATED_NOME)
            .modelo(UPDATED_MODELO)
            .anoFabricacao(UPDATED_ANO_FABRICACAO)
            .capacidadeLitro(UPDATED_CAPACIDADE_LITRO)
            .volumeMinuto(UPDATED_VOLUME_MINUTO)
            .descricao(UPDATED_DESCRICAO);

        restMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMaquinario.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMaquinario))
            )
            .andExpect(status().isOk());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
        Maquinario testMaquinario = maquinarioList.get(maquinarioList.size() - 1);
        assertThat(testMaquinario.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testMaquinario.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testMaquinario.getAnoFabricacao()).isEqualTo(UPDATED_ANO_FABRICACAO);
        assertThat(testMaquinario.getCapacidadeLitro()).isEqualTo(UPDATED_CAPACIDADE_LITRO);
        assertThat(testMaquinario.getVolumeMinuto()).isEqualTo(UPDATED_VOLUME_MINUTO);
        assertThat(testMaquinario.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void patchNonExistingMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, maquinarioDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMaquinario() throws Exception {
        int databaseSizeBeforeUpdate = maquinarioRepository.findAll().size();
        maquinario.setId(count.incrementAndGet());

        // Create the Maquinario
        MaquinarioDTO maquinarioDTO = maquinarioMapper.toDto(maquinario);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMaquinarioMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(maquinarioDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Maquinario in the database
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMaquinario() throws Exception {
        // Initialize the database
        maquinarioRepository.saveAndFlush(maquinario);

        int databaseSizeBeforeDelete = maquinarioRepository.findAll().size();

        // Delete the maquinario
        restMaquinarioMockMvc
            .perform(delete(ENTITY_API_URL_ID, maquinario.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Maquinario> maquinarioList = maquinarioRepository.findAll();
        assertThat(maquinarioList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
