package com.jacto.unext.repository;

import com.jacto.unext.domain.HistoricoLocacao;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the HistoricoLocacao entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistoricoLocacaoRepository extends JpaRepository<HistoricoLocacao, Long> {}
