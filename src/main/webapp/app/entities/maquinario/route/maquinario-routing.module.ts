import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MaquinarioComponent } from '../list/maquinario.component';
import { MaquinarioDetailComponent } from '../detail/maquinario-detail.component';
import { MaquinarioUpdateComponent } from '../update/maquinario-update.component';
import { MaquinarioRoutingResolveService } from './maquinario-routing-resolve.service';

const maquinarioRoute: Routes = [
  {
    path: '',
    component: MaquinarioComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MaquinarioDetailComponent,
    resolve: {
      maquinario: MaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MaquinarioUpdateComponent,
    resolve: {
      maquinario: MaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MaquinarioUpdateComponent,
    resolve: {
      maquinario: MaquinarioRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(maquinarioRoute)],
  exports: [RouterModule],
})
export class MaquinarioRoutingModule {}
