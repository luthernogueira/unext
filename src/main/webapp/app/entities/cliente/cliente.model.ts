import { IUser } from 'app/entities/user/user.model';
import { IHistoricoLocacao } from 'app/entities/historico-locacao/historico-locacao.model';
import { ICarteira } from 'app/entities/carteira/carteira.model';

export interface ICliente {
  id?: number;
  nome?: string;
  sobrenome?: string;
  cpfcnpj?: string;
  tefelone?: number;
  email?: string | null;
  rua?: string | null;
  numero?: number | null;
  bairro?: string | null;
  cep?: string | null;
  cidade?: string | null;
  estado?: string | null;
  user?: IUser | null;
  historicoLocacaos?: IHistoricoLocacao[] | null;
  carteiras?: ICarteira[] | null;
}

export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public nome?: string,
    public sobrenome?: string,
    public cpfcnpj?: string,
    public tefelone?: number,
    public email?: string | null,
    public rua?: string | null,
    public numero?: number | null,
    public bairro?: string | null,
    public cep?: string | null,
    public cidade?: string | null,
    public estado?: string | null,
    public user?: IUser | null,
    public historicoLocacaos?: IHistoricoLocacao[] | null,
    public carteiras?: ICarteira[] | null
  ) {}
}

export function getClienteIdentifier(cliente: ICliente): number | undefined {
  return cliente.id;
}
