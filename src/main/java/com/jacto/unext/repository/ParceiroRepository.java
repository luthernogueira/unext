package com.jacto.unext.repository;

import com.jacto.unext.domain.Parceiro;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Parceiro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParceiroRepository extends JpaRepository<Parceiro, Long> {}
